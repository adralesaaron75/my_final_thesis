require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const dbConfig = require("./config/databaseConnection");
const studentModel = require("./models/studentModel");
const errorMiddleware = require("./middleware/errorMiddleware");
const cors = require("cors");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const cookieParser = require("cookie-parser");
const booksModel = require("./models/bookModel");
const borrowbooksModel = require("./models/borrowbookModel");

const  author  = require("./models/author");
const books = require("./models/book");

const webscrapeModel = require("./models/databaseWebScraped")
const subrejected = require("./models/submissionrejected");
const subapproved = require("./models/submissionapproved");

const { exec } = require("child_process");
const bodyParser = require("body-parser");
const fs = require("fs");

const MONGO_URL = process.env.MONGO_URL;
const JWT_SECRET = process.env.JWT_SECRET;
const FRONTEND = process.env.FRONTEND;
const PORT = process.env.PORT || 3000;

var corsOptions = {
  origin: FRONTEND, //multiple access ['http://example.com', 'www.facebook.com']
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

const app = express();
app.use(cors(corsOptions));
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());



app.post("/saveFormToJson", (req, res) => {
  const formData = req.body; // Assuming the form data is sent in the request body
  const jsonData = JSON.stringify(formData, null, 2); // Convert the form data to JSON string
  fs.writeFileSync("form_data.json", jsonData); // Write JSON data to file
  res.status(200).send("Form data saved to form_data.json");
});




app.post("/runcode", (req, res) => {
  exec("node WebScraped.js", (err, stdout, stderr) => {
    if (err) {
      console.error(err);
      return res.status(500).send(err);
    }
    console.log(stdout);
    console.error(stderr);
    res.send("Script executed successfully");
  });
});



app.post("/spellingsaveFormToJson", (req, res) => {
  const formData = req.body; // Assuming the form data is sent in the request body
  const jsonData = JSON.stringify(formData, null, 2); // Convert the form data to JSON string
  fs.writeFileSync("spellingsearchterm.json", jsonData); // Write JSON data to file
  res.status(200).send("Form data saved to form_data.json");
});

app.post("/runcodeforspelling", (req, res) => {
  exec("node spelling_corrected.js", (err, stdout, stderr) => {
    if (err) {
      console.error(err);
      return res.status(500).send(err);
    }
    console.log(stdout);
    console.error(stderr);
    res.send("Script executed successfully");
  });
});




app.post("/api/register", async (req, res) => {
  try {
    const password = req.body.password;
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);
    req.body.password = hashedPassword;
    const newuser = new studentModel(req.body);
    const student = await newuser.save();
    res.status(200).json(student);
  } catch (error) {
    res.status(500);
    // throw new Error(error.message);
  }
});

app.get("/api/students", async (req, res) => {
  try {
    const students = await studentModel.find();
    res.status(201).json(students);
  } catch (error) {
    res.status(500).json({ error: "Unable to get students" });
    // throw new Error(error.message);
  }
});

app.post("/api/login", async (req, res) => {
  const { email, password } = req.body;

  const user = await studentModel.findOne({ email });
  if (!user) {
    return res.json({ error: "User Not found" });
  }
  if (await bcrypt.compare(password, user.password)) {
    const token = jwt.sign({ email: user.email }, JWT_SECRET, {
      expiresIn: "1d",
      // expiresIn: "1hr",
      //expiresIn: "15m",
    });

    if (res.status(201)) {
      return res.json({ status: "ok", data: token });
    } else {
      return res.json({ status: "notlogin" });
    }
  }
  res.json({ status: "error", error: "Invalid Password" });
});

app.post("/api/userData", async (req, res) => {
  const { token } = req.body;
  try {
    const user = jwt.verify(token, JWT_SECRET, (err, res) => {
      if (err) {
        return "token expired";
      }
      return res;
    });
    console.log(user);
    if (user == "token expired") {
      return res.send({ status: "error", data: "token expired" });
    }

    const useremail = user.email;
    studentModel
      .findOne({ email: useremail })
      .then((data) => {
        res.send({ status: "ok", data: data });
      })
      .catch((error) => {
        res.send({ status: "error", data: error });
      });
  } catch (error) {}
});




app.get("/api/getAllUser", async (req, res) => {
  try {
    const allUser = await studentModel.find({});
    res.send({ status: "ok", data: allUser });
  } catch (error) {
    console.log(error);
  }
});

app.get("/api/countAllUser", async (req, res) => {
  try {
    const totalCount = await studentModel.countDocuments({});
    res.send({ totalCount });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Unable to count all books" });
  }
});


app.get("/api/getAllBooks", async (req, res) => {
  try {
    const allBooks = await booksModel.find({});
    res.send({ status: "ok", data: allBooks });
  } catch (error) {
    console.log(error);
  }
});

app.post("/api/add-book", async (req, res) => {
  const {
    titlename,
    contactemail,
    authorjoin,
    abstractname,
    category,
    publisheddate,
    yearpublished,
    course,
    base64,
    referencejoin,
  } = req.body;
  try {
    await booksModel.create({
      titlename,
      contactemail,
      authorjoin,
      abstractname,
      category,
      publisheddate,
      yearpublished,
      course,
      referencejoin,
      image: base64,
    });
    res.send({ status: "ok" });
  } catch (error) {
    res.send({ status: "error" });
  }
});


app.delete("/api/delete-book/:id", async (req, res) => {
  try {
     const {id} = req.params;
     const book = await booksModel.findByIdAndDelete(id);
     if(!book){
      res.status(404)
      throw new Error(`cannot find any book with ID ${id}`) 
     } 
     res.status(200).json(book);
    } catch (error) {
      res.status(500)
      throw new Error(error.message)
  }
})

app.get("/api/get-singlebook/:id", async (req, res) => {
  try {
     const {id} = req.body;
     const book = await booksModel.findOne(id);
     if(!book){
      res.status(404)
      throw new Error(`cannot find any book with ID ${id}`) 
     } 
     res.status(200).json(book);
  } catch (error) {
      res.status(500)
      throw new Error(error.message)
  //    res.status(500).json({message: error.message}) 
  }
});


app.put("/api/updatebook/:id", async (req, res) => {
  try {
     const {id} = req.params;
     const books = await booksModel.findByIdAndUpdate(id, req.body);
     if(!books){
      res.status(404)
      throw new Error(`cannot find any product with ID ${id}`) 

     } 
     await booksModel.findById(id);
     res.send({ status: "ok" });
    } catch (error) {
      res.send({ status: "error" });
    }
})


app.get("/api/getScraped", async (req, res) => {
  try {
    const allScraped = await webscrapeModel.find({});
    res.send({ status: "ok", data: allScraped });
  } catch (error) {
    console.log(error);
  }
});



app.post("/api/add-webscraped", async (req, res) => {
  const {
    ScrapedTitle,
    ScrapedAuthor,
    ScrapedYear,
    ScrapedAbstract,
    ScrapedFullAbstract,
    ScrapedLink
  } = req.body;
  try {
    await webscrapeModel.create({
      ScrapedTitle,
      ScrapedAuthor,
      ScrapedYear,
      ScrapedAbstract,
      ScrapedFullAbstract,
      ScrapedLink,
    });
    res.send({ status: "ok" });
  } catch (error) {
    res.send({ status: "error" });
  }
});


app.get("/api/getSubmissionrejected", async (req, res) => {
  try {
    const allrejected = await subrejected.find({});
    res.send({ status: "ok", data: allrejected });
  } catch (error) {
    console.log(error);
  }
});




app.post("/api/add-submissionrejected", async (req, res) => {
  const {
    SubmitTitleName,
    SubmitAbstract,
    recommendation,
    yearpublished,
    publisheddate,
    authorjoin,
    contactemail,
    category,
    course,
  } = req.body;
  try {
    await subrejected.create({
      SubmitTitleName,
      SubmitAbstract,
      recommendation,
      yearpublished,
      publisheddate,
      authorjoin,
      contactemail,
      category,
      course,
    });
    res.send({ status: "ok" });
  } catch (error) {
    res.send({ status: "error" });
  }
});


app.get("/api/getSubmissionapproved", async (req, res) => {
  try {
    const allapproved = await subapproved.find({});
    res.send({ status: "ok", data: allapproved });
  } catch (error) {
    console.log(error);
  }
});

app.post("/api/add-submissionapproved", async (req, res) => {
 const {
   SubmitTitleName,
   SubmitAbstract,
   yearpublished,
   publisheddate,
   authorjoin,
   contactemail,
   category,
   course,
 } = req.body;

  try {
    await subapproved.create({
      SubmitTitleName,
      SubmitAbstract,
      yearpublished,
      publisheddate,
      authorjoin,
      contactemail,
      category,
      course,
    });
    res.send({ status: "ok" });
  } catch (error) {
    res.send({ status: "error" });
  }
});

app.put("/api/updatesubmissionapproved/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const aapproved = await subapproved.findByIdAndUpdate(id, req.body);
    if (!aapproved) {
      res.status(404);
      throw new Error(`cannot find any borrowed book with ID ${id}`);
    }
    await subapproved.findById(id);
    res.send({ status: "ok" });
  } catch (error) {
    res.send({ status: "error" });
  }
});








//borrow books

app.post("/api/add-borrowbook", async (req, res) => {
  const {
    SubmitTitleName,
    SubmitAbstract,
    YearPublished,
    category,
    contactemail,
    course,
    SubmittedName,
    SubmitAuthor,
    OpennedorNot,
    Submittedby,
    studentName,
    studentid,
    referenceCode,
    status,
    recommendation,
    base64,
  } = req.body;

  try {
    await borrowbooksModel.create({
      SubmitTitleName,
      SubmitAbstract,
      YearPublished,
      category,
      // publisheddate,
      contactemail,
      course,
      SubmittedName,
      SubmitAuthor,

      Submittedby,
      studentName,
      OpennedorNot:"New",

      referenceCode,
      status: "pending",
      recommendation: "",
      image: base64,
    });
    
    res.send({ status: "ok" });
  } catch (error) {
    res.send({ status: "error" });
  }
});




app.get("/api/getAllBorrowedBooks", async (req, res) => {
  try {
    const allborrowedBooks = await borrowbooksModel.find({});
    res.send({ status: "ok", data: allborrowedBooks });
  } catch (error) {
    console.log(error);
  }
});



// app.get("/api/getAllBorrowedBooks", async (req, res) => {
//   try {
//     const userData = req.body.userData; // Assuming userData is sent in the request body

//     // Fetch all borrowed books where Submittedby matches userData.email
//     const allborrowedBooks = await borrowbooksModel.find({
//       Submittedby: userData.email,
//     });

//     res.send({ status: "ok", data: allborrowedBooks });
//   } catch (error) {
//     console.log(error);
//     res.status(500).send({ error: "Unable to fetch borrowed books" });
//   }
// });





app.delete("/api/delete-borrowedbook/:id", async (req, res) => {
  try {
     const {id} = req.params;
     const borrowedbook = await borrowbooksModel.findByIdAndDelete(id);
     if(!borrowedbook){
      res.status(404)
      throw new Error(`cannot find any borrowed book with ID ${id}`) 
     } 
     res.status(200).json(borrowedbook);
    } catch (error) {
      res.status(500)
      throw new Error(error.message)
  }
})

app.get("/api/get-singleborrowedbook/:id", async (req, res) => {
  try {
     const {id} = req.body;
     const bbook = await borrowbooksModel.findOne(id);
     if(!bbook){
      res.status(404)
      throw new Error(`cannot find any borrowed book with ID ${id}`) 
     } 
     res.status(200).json(bbook);
  } catch (error) {
      res.status(500)
      throw new Error(error.message)
  //    res.status(500).json({message: error.message}) 
  }
});

app.put("/api/updateborrowedbook/:id", async (req, res) => {
  try {
     const {id} = req.params;
     const bbooks = await borrowbooksModel.findByIdAndUpdate(id, req.body);
     if(!bbooks){
      res.status(404)
      throw new Error(`cannot find any borrowed book with ID ${id}`) 

     } 
     await borrowbooksModel.findById(id);
     res.send({ status: "ok" });
    } catch (error) {
      res.send({ status: "error" });
    }
})


//end borrowed books

//studen borrow book

app.get("/api/borrowedbooklists/:id", async (req, res) => {
  try {
    const id  = "6531779949714d336c9f1271";
    //  const studentid = await studentModel.findById({_id: id})
     const books = await borrowbooksModel.find({});
      res.status(200).send({
      message: "Get borrow books fetch successfully",
      success: true,
      data: books,
    });
  } catch (error) {
    res
      .status(500)
      .send({ message: "Error borrow book info", success: false, error });
  }
});



///////////////////////testing api joining////////////////////////////

app.post("/api/author", async (req, res) => {
  try {
    const {name, email} = req.body;
    const auth = await author.create({
      name,
      email
    });
    
    res.status(200).send(auth);
  } catch (error) {
    res
    .status(500)
    .send({ message: "Error author info", success: false, error });
  }
});


app.post("/api/book", async (req, res) => {
  try {
    const {name, email} = req.body;
    const book = await books.create({
      author_id: req.body.author_id,
      title: req.body.title,

    });
    const rightjoin = await book.save();
    
    res.status(200).send(rightjoin);
  } catch (error) {
    res
    .status(500)
    .send({ message: "Error author info", success: false, error });
  }
});

app.post("/api/authorbookpopulate", async (req, res) => {
  try {
    const abook = await books.find({_id: req.body.right_id}).populate('author_id');

    res.status(200).send(abook);
  } catch (error) {
    res
    .status(500)
    .send({ message: "Error author info", success: false, error });
  }
});




app.use(errorMiddleware);

mongoose.set("strictQuery", false);

mongoose
  .connect(MONGO_URL)
  .then(() => {
    console.log("connected to MongoDB");
    app.listen(PORT, () => {
      console.log(`Node API app is running on port ${PORT}`);
    });
  })
  .catch((error) => {
    console.log(error);
  });
