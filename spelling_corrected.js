const puppeteer = require("puppeteer");
const fs = require("fs");

const scrapeCorrectedQuery = async (query) => {
  const browser = await puppeteer.launch({
    headless: true, // Change to true for production
  });

  const page = await browser.newPage();

  await page.goto(
    `https://www.google.com/search?q=${encodeURIComponent(query)}`,
    {
      waitUntil: "domcontentloaded",
    }
  );

  // Check if Google suggests a corrected query
  const correctedQuery = await page.evaluate(() => {
    const suggestionElement1 = document.querySelector("a#fprsl.gL9Hy");
    if (suggestionElement1) {
      return suggestionElement1.textContent;
    }
    const suggestionElement2 = document.querySelector("a.gL9Hy");
    if (suggestionElement2) {
      return suggestionElement2.textContent;
    }
    const spanSuggestionElement = document.querySelector("span.zqOZs");
    return spanSuggestionElement ? spanSuggestionElement.textContent : null;
  });

  await browser.close();

  return correctedQuery;
};

const saveCorrectedQueryToFile = (correctedQuery) => {
  const data = {
    correctedQuery: correctedQuery,
  };
  const jsonData = JSON.stringify(data, null, 2);
  fs.writeFileSync("spellingcorrected.json", jsonData);
  console.log("Corrected query saved to spellingcorrected.json");
};

const resetCorrectedQueryFile = () => {
  const data = {
    correctedQuery: "",
  };
  const jsonData = JSON.stringify(data, null, 2);
  fs.writeFileSync("spellingcorrected.json", jsonData);
  console.log("Value of correctedQuery in spellingcorrected.json reset.");
};

// Read the search term from the JSON file
const searchTermData = JSON.parse(
  fs.readFileSync("spellingsearchterm.json", "utf8")
);
const query = searchTermData.searchTerm;

scrapeCorrectedQuery(query)
  .then((correctedQuery) => {
    if (correctedQuery) {
      console.log(`Did you mean: ${correctedQuery}`);
      saveCorrectedQueryToFile(correctedQuery);
    } else {
      console.log("No corrected query suggested.");
      resetCorrectedQueryFile();
    }
  })
  .catch((error) => {
    console.error("Error:", error);
  });
