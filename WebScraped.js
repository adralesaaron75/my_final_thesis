const puppeteer = require("puppeteer");
const fs = require("fs");


const scrapeData = async () => {


     const formData = JSON.parse(fs.readFileSync("form_data.json", "utf8"));

  const browser = await puppeteer.launch({
    headless: false,
  });

  const page = await browser.newPage();

  await page.goto("https://scholar.google.com", {
    waitUntil: "domcontentloaded",
  });

  // Wait for the search input field to load
  await page.waitForSelector('input[type="text"][name="q"]');

  // Type into the search input field
  await page.type('input[type="text"][name="q"]', formData.searchTerm);

  // Submit the form
  await page.keyboard.press("Enter");

  // Wait for the search results to load
  await page.waitForSelector("#gs_res_ccl_mid");

  // Extract data from search results
  const results = [];
  let nextPageAvailable = true;

  while (nextPageAvailable) {
    const searchResults = await page.evaluate(() => {
      const resultsList = [];
      // Get all search result items
      const items = document.querySelectorAll(".gs_ri");

      // Iterate over each search result item
      for (const item of items) {
        // Extract title, link, author, and abstract
        const titleElement = item.querySelector("h3.gs_rt a");
        const title = titleElement
          ? titleElement.textContent.trim()
          : "No Title";
        const link = titleElement ? titleElement.href : "No Link";
        const authorElement = item.querySelector(".gs_a");
        const author = authorElement
          ? authorElement.textContent.trim()
          : "No Author";
        const abstractElement = item.querySelector(".gs_rs");
        const abstract = abstractElement
          ? abstractElement.textContent.trim()
          : "No Abstract";

        // Extract specific pattern from abstract
      //  const extractedAbstractMatch = abstract.match(/…\s(.*?)\s…|/);
      const extractedAbstractMatch = abstract.match(/([a-zA-Z].*?[,\.])/);
       const extractedAbstract = extractedAbstractMatch
         ? extractedAbstractMatch[0]
         : "No Extracted Abstract";

        // Push title, link, author, and abstract to the results array
        resultsList.push({ title, link, author, abstract, extractedAbstract });
      }

      return resultsList;
    });

    results.push(...searchResults);

    // Check if "Next" button exists
    const nextButton = await page.$("#pnnext");
    if (nextButton) {
      await Promise.all([
        page.waitForNavigation({ waitUntil: "domcontentloaded" }),
        nextButton.click(),
      ]);
    } else {
      nextPageAvailable = false;
    }
  }


  // Extract full abstracts from each link
  for (let i = 0; i < results.length; i++) {
    const page2 = await browser.newPage(); // Open a new page instance
    await page2.goto(results[i].link, { waitUntil: "domcontentloaded" }); // Visit the link
    await page2.waitForSelector(".abstract", { timeout: 5000 }).catch(() => {}); // Wait for the abstract element (timeout set to 5 seconds)
    const fullAbstract = await page2.evaluate(() => {
      const abstractElement = document.querySelector(".abstract"); // Assuming the abstract has a class name 'abstract'
      return abstractElement
        ? abstractElement.textContent.trim()
        : "No Full Abstract";
    });
    results[i].fullAbstract = fullAbstract; // Store the full abstract
    await page2.close(); // Close the new page
  }

  // Write the results to a JSON file
  fs.writeFileSync("search_results.json", JSON.stringify(results, null, 2));

  // Log success message
  console.log("Search results saved to search_results.json");

  // Close the browser when done
  await browser.close();
};

// Exporting the scrapeData function
module.exports = { scrapeData };

// Calling the scrapeData function
scrapeData();
