const mongoose = require("mongoose");

const subapprovedSchema = new mongoose.Schema(
  {
    SubmitTitleName: {
      type: String,
    },
    SubmitAbstract: {
      type: String,
    },
    yearpublished: {
      type: String,
    },
    publisheddate: {
      type: String,
    },
    authorjoin: {
      type: String,
    },
    contactemail: {
      type: String,
    },
    category: {
      type: String,
    },
    course: {
      type: String,
    },
  },
  {
    collection: "tbl_subapproved",
    timestamps: true, // This line sets the timestamps
  }
);
const subapproved = mongoose.model("tbl_subapproved", subapprovedSchema);
module.exports = subapproved;
