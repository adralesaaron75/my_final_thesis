const mongoose = require("mongoose");

const WebScrapedDatabaseSchema = new mongoose.Schema(
  {
    ScrapedTitle: {
      type: String,
    },
    ScrapedAuthor: {
      type: String,
    },
    ScrapedYear: {
      type: String,
    },
    ScrapedAbstract: {
      type: String,
    },
    ScrapedFullAbstract: {
      type: String,
    },
    ScrapedLink: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);
const webscrapeModel = mongoose.model(
  "tbl_webscrapedSchema",
  WebScrapedDatabaseSchema
);
module.exports = webscrapeModel;


