const mongoose = require('mongoose');

const booksSchema = new mongoose.Schema(
  {
    titlename: {
      type: String,
    },
    contactemail: {
      type: String,
    },
    authorjoin: {
      type: String,
    },
    abstractname: {
      type: String,
    },
    category: {
      type: String,
    },
    publisheddate: {
      type: String,
    },
    yearpublished: {
      type: String,
    },
    course: {
      type: String,
    },
    referencejoin: {
      type: String,
    },
    image: {
      type: String,
    },
  },
  {
    collection: "tbl_books",
    timestamps: true,
  }
);
const booksModel = mongoose.model('tbl_books', booksSchema);
module.exports = booksModel;
