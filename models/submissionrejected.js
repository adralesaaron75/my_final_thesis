const mongoose = require("mongoose");

const subrejectedSchema = new mongoose.Schema(
  {
    SubmitTitleName: {
      type: String,
    },
    SubmitAbstract: {
      type: String,
    },
    recommendation: {
      type: String,
    },
    yearpublished: {
      type: String,
    },
    publisheddate: {
      type: String,
    },
    authorjoin: {
      type: String,
    },
    contactemail: {
      type: String,
    },
    category: {
      type: String,
    },
    course: {
      type: String,
    },
  },
  {
    collection: "tbl_subrejected",
    timestamps: true,
  }
);

const subrejected = mongoose.model("tbl_subrejected", subrejectedSchema);
module.exports = subrejected;
