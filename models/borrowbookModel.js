const mongoose = require("mongoose");

const borrowbooksSchema = new mongoose.Schema(
  {
    SubmitTitleName: {
      type: String,
    },
    SubmitAbstract: {
      type: String,
    },
    YearPublished: {
      type: String,
    },
    category: {
      type: String,
    },
    contactemail: {
      type: String,
    },
    course: {
      type: String,
    },
    Submittedby: {
      type: String,
    },
    SubmittedName: {
      type: String,
      // default: "",
    },
    studentName: {
      type: String,
    },
    SubmitAuthor: {
      type: String,
    },
    image: {
      type: String,
    },
    // borrowed: {
    //   type: Object,
    //   required: true,
    // },
    referenceCode: {
      type: String,
    },
    status: {
      type: String,
    },
    recommendation: {
      type: String,
    },
    OpennedorNot: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);
const borrowbooksModel = mongoose.model("tbl_borrowbooks", borrowbooksSchema);
module.exports = borrowbooksModel;
