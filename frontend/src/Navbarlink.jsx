
import { Routes, Route } from 'react-router-dom'
import Home from './components/Home';
import Aboutus from './components/Aboutus';
import Register from './components/Register';
import Dashboard from './components/dashboard/Dashboard';
import Account from './components/Account';
import Logout from './components/Logout';
import Books from './components/adminside/Library1';
import Borrowedbooks from './components/adminside/ManageSub';
import Returnedbooks from './components/adminside/Users';
import Damagecharge from './components/adminside/Reports';
import Students from './components/adminside/Catalog';
import Admin from './components/adminside/Admin';
// import Bookslist from './components/studentside/Bookslist';
import Notfound from './Notfound';
import Borrowedbookstudent from './components/studentside/Borrowedbookstudent';
import AdminHome from './components/AdminHome';
import SearchBar from './components/studentside/components_students/searchmode';
import WebScraped from './components/adminside/WebScraped';
import ApprovedDash from './components/adminside/pages_admin/ApprovedDash';
import RejectedDash from './components/adminside/pages_admin/RejectedDash';
import SubmitForm from './components/studentside/components_students/SubmitForm';
import ProfileForm from './components/studentside/components_students/MyProfile';
import SubmmitedPage from './components/studentside/components_students/Submitted';
import BorrowBookForm from './components/studentside/components_students/Borrowsample';



import Reportsapproved from './components/adminside/pages_admin/reportsapproved';
import Reportsdisapproved from './components/adminside/pages_admin/reportsdisapproved';
import Reportssubmitted from './components/adminside/pages_admin/reportssubmitted';
import Reportwebscraped from './components/adminside/pages_admin/reportswebscraped';
import ManageSubmitted from './components/adminside/pages_admin/MngeSubmitted';
import ManageApproved from './components/adminside/pages_admin/MngeApproved';
import ManageRejected from './components/adminside/pages_admin/MngeRejected';
import UserReportsDash from './components/adminside/pages_admin/Usersdash';
import AdminReportsDash from './components/adminside/pages_admin/Admindash';
import AdminData from './components/adminside/Admindata';


function App() {
  const isUserSignedIn = !! localStorage.getItem('token');
  return (
         <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Home />} />
            <Route path="/search" element={<SearchBar/>} />
            <Route path="/search/submit" element={<SubmitForm/>} />
            <Route path="/aboutus" element={<Aboutus />} />
            <Route path="/register" element={<Register />} />
            <Route path="/profile" element={<ProfileForm />} />
            <Route path="/submittedhome" element={<SubmmitedPage />} />
            <Route path="/borrowbooks" element={<BorrowBookForm />} />

            {isUserSignedIn && <Route path="/dashboard/dashboard" element={<Dashboard />} />}
            {isUserSignedIn && <Route path="/admin/webscraped" element={<WebScraped />} />}
            {isUserSignedIn && <Route path="/admin/approved" element={<ApprovedDash />} />}
            {isUserSignedIn && <Route path="/admin/reject" element={<RejectedDash />} />}
            {isUserSignedIn && <Route path="/admin/dashboard" element={<AdminHome />} />}
            {isUserSignedIn && <Route path="/admin/institution" element={<Books />} />}
            {isUserSignedIn && <Route path="/admin/users" element={<Borrowedbooks />} />}
            {isUserSignedIn && <Route path="/admin/admin/res" element={<AdminData />} />}
            {isUserSignedIn && <Route path="/admin/managesubmission" element={<Returnedbooks />} />}
            {isUserSignedIn && <Route path="/admin/reports" element={<Damagecharge />} />}
            {isUserSignedIn && <Route path="/admin/catalog" element={<Students />} />}
            {isUserSignedIn && <Route path="/admin/admins" element={<Admin />} />}
            {/* {isUserSignedIn && <Route path="/student/booklists" element={<Bookslist />} />}  */}
            {isUserSignedIn && <Route path="/student/borrowedbooklists" element={<Borrowedbookstudent />} />}      



            {isUserSignedIn && <Route path="/admin/reports/approved" element={<Reportsapproved />} />}
            {isUserSignedIn && <Route path="/admin/reports/disapproved" element={<Reportsdisapproved />} />}
            {isUserSignedIn && <Route path="/admin/reports/submitted" element={<Reportssubmitted />} />}
            {isUserSignedIn && <Route path="/admin/reports/webscraped" element={<Reportwebscraped />} />}
            {isUserSignedIn && <Route path="/admin/reports/admin" element={<AdminReportsDash />} />}
            {isUserSignedIn && <Route path="/admin/reports/users" element={<UserReportsDash />} />}



            {isUserSignedIn && <Route path="/admin/managesubmission/submitted" element={<ManageSubmitted />} />} 
            {isUserSignedIn && <Route path="/admin/managesubmission/approved" element={<ManageApproved />} />} 
            {isUserSignedIn && <Route path="/admin/managesubmission/rejected" element={<ManageRejected />} />} 
                
            <Route path="*" element={<Notfound />} />
            <Route path="/account" element={<Account />} />
            <Route path="/logout" element={<Logout />} />
         </Routes>
  )
}

export default App
