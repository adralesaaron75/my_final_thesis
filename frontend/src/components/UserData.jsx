import React, { useState, useEffect } from 'react';
import Students from './adminside/Catalog';
import { VITE_BACKEND_URL } from '../App';
import axios from 'axios'; // Assuming you're using axios for HTTP requests

const ParentUserData = () => {
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    fetch(`${VITE_BACKEND_URL}/api/userData`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify({
        token: window.localStorage.getItem("token"),
      }),
    })
    .then((res) => {
      if (!res.ok) {
        throw new Error(`HTTP error! Status: ${res.status}`);
      }
      return res.json();
    })
    .then((data) => {
      console.log(data, "userData");
      if (data.data.userType === "student") {
        setStudent(true);
      }
      setUserData(data.data);
    })
    .catch((error) => {
      console.error("Error fetching user data:", error);
      // Handle the error (e.g., set an error state or display a message to the user)
    });
  }, []);

  return (
    <div>
      <h1>Parent Component</h1>
      {/* <Students userData={userData} /> */}
    </div>
  );
};

export default ParentUserData;



// import { VITE_BACKEND_URL } from '../../../App';
// import React, { useState, useEffect } from 'react';
// import axios from 'axios'; // Assuming you're using axios for HTTP requests

// function BorrowBookForm() {
//     const [userData, setUserData] = useState(null);
//     const [student, setStudent] = useState(false);

//     useEffect(() => {
//         fetch(`${VITE_BACKEND_URL}/api/userData`, {
//           method: "POST",
//           headers: {
//             "Content-Type": "application/json",
//             Accept: "application/json",
//           },
//           body: JSON.stringify({
//             token: window.localStorage.getItem("token"),
//           }),
//         })
//         .then((res) => {
//           if (!res.ok) {
//             throw new Error(`HTTP error! Status: ${res.status}`);
//           }
//           return res.json();
//         })
//         .then((data) => {
//           console.log(data, "userData");
//           if (data.data.userType === "student") {
//             setStudent(true);
//           }
//           setUserData(data.data);
//         })
//         .catch((error) => {
//           console.error("Error fetching user data:", error);
//           // Handle the error (e.g., set an error state or display a message to the user)
//         });
//       }, []);
      
  
//     return (
//       <div>
//         {userData ? (
//           <div>
//             <h2>User Data</h2>
//             <p>Name: {userData.name}</p>
//             <p>Email: {userData.email}</p>
//             <p>Course: {userData.course}</p>
//             <p>Year and Section: {userData.yearandsection}</p>
//             {/* Add more user data fields as needed */}
//           </div>
//         ) : (
//           <p>Loading user data...</p>
//         )}
//       </div>
//     );
//   }
  
  
// export default BorrowBookForm;
