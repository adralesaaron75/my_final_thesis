import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { FaBars, FaSignInAlt, FaSignOutAlt, FaEllipsisV, FaBell } from "react-icons/fa";
import { useLocation } from "react-router-dom";
import { VITE_BACKEND_URL } from "../App";
import axios from "axios";

function NavbarMenu() {

  const [userData, setUserData] = useState(null);
  const [student, setStudent] = useState(false);

  useEffect(() => {
      fetch(`${VITE_BACKEND_URL}/api/userData`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify({
          token: window.localStorage.getItem("token"),
        }),
      })
      .then((res) => {
        if (!res.ok) {
          throw new Error(`HTTP error! Status: ${res.status}`);
        }
        return res.json();
      })
      .then((data) => {
        console.log(data, "userData");
        if (data.data.userType === "student") {
          setStudent(true);
        }
        setUserData(data.data);
      })
      .catch((error) => {
        console.error("Error fetching user data:", error);
        // Handle the error (e.g., set an error state or display a message to the user)
      });
    }, []);


    const renderCircleWithLetter = (name) => {
      const startingLetter = name.charAt(0).toUpperCase();
      return (
        <div
          style={{
            width: "30px",
            height: "30px",
            borderRadius: "50%",
            backgroundColor: "gray",
            textAlign: "center",
            lineHeight: "30px",
            color: "white",
            marginRight: "10px",
           
          }}
        >
          {startingLetter}
        </div>
      );
    };
    











  const isUserSignedIn = !!localStorage.getItem("token");
  const [showDrawer, setShowDrawer] = useState(false);
  const location = useLocation();
  const isSubmitPage = location.pathname === "/search/submit";

  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  useEffect(() => {
    const handleScroll = () => {
      setShowDrawer(false);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  let navLinks;
  if (isUserSignedIn) {
    navLinks = (
      <Nav className="ms-auto">
        <Link to="/" className="navbar-brand">
          {/* Your logo */}
        </Link>
        <Navbar.Toggle aria-controls="navbarScroll">
          <FaBars style={{ color: "#fff", fontSize: "1.5rem" }} />
        </Navbar.Toggle>
        <Navbar.Collapse id="navbarScroll">
          <Nav className="ms-auto">
       
            <Nav.Link
              className="navitem"
              onClick={() => setShowDrawer(!showDrawer)}
              style={{padding:'0px'}}
            >

              
              
              {userData && (
  <span className="nav-link text-white">
    <div style={{display:'flex', marginTop:'5px', marginRight:'20px'}}>
  
     {renderCircleWithLetter(userData.name)} {userData.name}
     {/* <FaEllipsisV style={{ marginLeft: "4px", marginTop:'5px'}} /> */}
     </div>
   
  </span>

)}
              {showDrawer && (
                <div
                  style={{
                    position: "fixed",
                    top: 70,
                    right: 0,
                    zIndex: 999,
                    filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.99))'
                  }}
                >
                  <div
                    style={{
                      position: "relative",
                      background: "#f8f9fa",
                      boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
                      borderRadius: "5px",
                      width: "15vw",
                    }}
                  >
                    {/* Your drawer content goes here */}
                    <ul style={{ listStyleType: "none", padding: 0 }}>
                     
                        {isSubmitPage ? (
              <li
              style={{
                padding: "10px 20px",
                borderBottom: "1px solid #ddd",
              }}
            >
              <a
                href="/settings"
                style={{
                  textDecoration: "none",
                  color: "#333",
                  display: "block",
                }}
              >
                <Link
                  to="/search"
                  className="nav-link"
                  style={{ color: "black" }}
                >
                  Go to Search
                </Link>
              </a>
            </li>
            ) : (
              <li
                        style={{
                          padding: "10px 20px",
                          borderBottom: "1px solid #ddd",
                        }}
                      >
                        <a
                          href="/settings"
                          style={{
                            textDecoration: "none",
                            color: "#333",
                            display: "block",
                          }}
                        >
                          <Link
                            to="/search/submit"
                            className="nav-link"
                            style={{ color: "black" }}
                          >
                            Submit Title
                          </Link>
                        </a>
                      </li>

              
            )}
                      <li
                        style={{
                          padding: "10px 20px",
                          borderBottom: "1px solid #ddd",
                        }}
                      >
                        <a
                          href="/manual"
                          style={{
                            textDecoration: "none",
                            color: "#333",
                            display: "block",
                          }}
                        >
                          <Link
                            to="/profile"
                            className="nav-link"
                            style={{ color: "black" }}
                          >
                            Profile
                          </Link>
                        </a>
                      </li>
                      <li
                        style={{
                          padding: "10px 20px",
                          borderBottom: "1px solid #ddd",
                        }}
                      >
                        <a
                          href="/manual"
                          style={{
                            textDecoration: "none",
                            color: "#333",
                            display: "block",
                          }}
                        >
                          <Link
                            to="/search/submit"
                            className="nav-link"
                            style={{ color: "black" }}
                          >
                            Favorites
                          </Link>
                        </a>
                      </li>
                      <li
                        style={{
                          padding: "10px 20px",
                          borderBottom: "1px solid #ddd",
                        }}
                      >
                        <a
                          href="/manual"
                          style={{
                            textDecoration: "none",
                            color: "#333",
                            display: "block",
                          }}
                        >
                          {" "}
                          <Link
                            onClick={logOut}
                            to="/login"
                            className="nav-link"
                            style={{ color: "black" }}
                          >
                            <FaSignOutAlt /> Logout
                          </Link>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              )}
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Nav>
    );
  } else {
    navLinks = (
      <Nav className="ms-auto">
       
        <Nav.Link className="navitem">
          <Link to="/login" className="nav-link">
            <FaSignInAlt /> Login
          </Link>
        </Nav.Link>
      </Nav>
    );
  }

  return (
    <Navbar
      style={{ background: "#800000", minHeight: "1rem" }}
      variant="dark"
      expand="lg"
    >
      <Container fluid>
        <Navbar.Brand>
          {/* Your logo */}
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll">
          <FaBars style={{ color: "#fff", fontSize: "1.5rem" }} />
        </Navbar.Toggle>
        <Navbar.Collapse id="navbarScroll">
          <Nav className="me-auto">
            <Nav.Link style={{color:'white'}}>
            <Link to="/search" className="nav-link">
            Term Frequency-Inverse Document Frequency  for Topic Recommendation and Submission System
            </Link>
            </Nav.Link>
            
          </Nav>
          {navLinks}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
  
}

export default NavbarMenu;
