import { IoIosArrowForward, IoIosArrowDown } from "react-icons/io";
import { Container, Form, Button } from "react-bootstrap";
import React, { useState, useEffect } from "react";
import { VITE_BACKEND_URL } from "../../../App";
import partsOfSpeech from "../../../../../parts_of_speech.json"

const SideTopic = ({ selectedTopics, setSelectedTopics, removeSelectedTopic, selectedCourse, setSelectedCourse, removeSelectedCourse, setSelectedCheckData, selectedYearPass, keywordsPass }) => {
  const [showTopicList, setShowTopicList] = useState(false);
  const [showCourseList, setShowCourseList] = useState(false);
  const [allChecked, setAllChecked] = useState(true);
  const [checkboxesChecked, setCheckboxesChecked] = useState({
    Institutional: true,
    WebScraped: true,
  });
  // const [selectedTopics, setSelectedTopics] = useState([]); // State to store selected topics

  const [selectedYears, setSelectedYears] = useState([]);
  const [selectedYearsString, setSelectedYearsString] = useState("");
  const [startYear, setStartYear] = useState(""); // State to store start year
  const [endYear, setEndYear] = useState(""); // State to store end year
  const [selectedKeywords, setSelectedKeywords] = useState([]);


  const toggleTopicList = () => {
    setShowTopicList(!showTopicList);
  };

  const toggleCourseList = () => {
    setShowCourseList(!showCourseList);
  };

  const handleAllCheckboxChange = () => {
    const newState = !allChecked;
    setAllChecked(newState);
    setCheckboxesChecked({
      Institutional: newState,
      WebScraped: newState,
    });
  };

 const handleCheckboxChange = (name) => {
    const newCheckboxes = { ...checkboxesChecked, [name]: !checkboxesChecked[name] };
    const allCheckboxChecked = Object.values(newCheckboxes).every((val) => val);
    setCheckboxesChecked(newCheckboxes);
    setAllChecked(allCheckboxChecked);
  
    // Log only the keys with a value of true

  };
  
  const [data, setData] = useState([]);

  useEffect(() => {
    getAllBooks();
  }, []);

  // Fetching all books
  const getAllBooks = async () => {
    await fetch(`${VITE_BACKEND_URL}/api/getAllBooks`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data, "bookData");
        setData(data.data);
      });
  };

  const countUniqueCourses = () => {
    const courseCounts = {};
    data.forEach((item) => {
      if (courseCounts[item.course]) {
        courseCounts[item.course]++;
      } else {
        courseCounts[item.course] = 1;
      }
    });
    return courseCounts;
  };

  const uniqueCourses = countUniqueCourses();
  
const countUniqueCategory = () => {
  const courseCategory = {};
  data.forEach((item) => {
    const categories = item.category.split(" | "); // Split category by "|"
    categories.forEach((category) => {
      if (courseCategory[category]) {
        courseCategory[category]++;
      } else {
        courseCategory[category] = 1;
      }
    });
  });
  return courseCategory;
};

const uniqueCategory = countUniqueCategory();
const countUniqueYear = () => {
  const courseYear = {};
  data.forEach((item) => {
    // Extracting the year from the date string
    const year = item.yearpublished.split("-")[0];
    if (courseYear[year]) {
      courseYear[year]++;
    } else {
      courseYear[year] = 1;
    }
  });
  return courseYear;
};


  const uniqueYear = countUniqueYear();

  const handleTopicSelection = (topic) => {
    if (selectedTopics.includes(topic)) {
      setSelectedTopics(selectedTopics.filter((selectedTopic) => selectedTopic !== topic));
    } else {
      setSelectedTopics([...selectedTopics, topic]);
    }
  };


  const handleCourseSelection = (course) => {
    if (selectedCourse.includes(course)) {
      setSelectedCourse(selectedCourse.filter((selectedCourse) => selectedCourse !== course));
    } else {
      setSelectedCourse([...selectedCourse, course]);
    }
  };

  const selectedCheck = () => {
    if (checkboxesChecked.WebScraped && checkboxesChecked.Institutional) {
      return 'All';
    } else {
      let selected = '';
      if (checkboxesChecked.WebScraped) {
        selected += 'Web Scraped ';
      }
      if (checkboxesChecked.Institutional) {
        selected += 'Institutional ';
      }
      return selected.trim(); // Remove trailing space
    }
  };

  setSelectedCheckData(selectedCheck());

 
  const handleYearSelection = (year) => {
    if (selectedYears.includes(year)) {
      setSelectedYears(selectedYears.filter((selectedYear) => selectedYear !== year));
    } else {
      setSelectedYears([...selectedYears, year]);
    }
  };

  useEffect(() => {
    setSelectedYearsString(selectedYears.join(" | "));
  }, [selectedYears]);

  useEffect(() => {
    if (startYear && endYear) {
      setSelectedYearsString(`${startYear} - ${endYear}`);
    } else {
      setSelectedYearsString(selectedYears.join(" | "));
    }
  }, [startYear, endYear, selectedYears]);


  useEffect(() => {
    selectedYearPass(selectedYearsString);
  }, [selectedYearsString, selectedYearPass]);
  

  const [data1, setData1] = useState([]);
  const [joinedAbstractNames, setJoinedAbstractNames] = useState('');
  const [joinedScrapeAbstractNames, setJoinedScrapeAbstractNames] = useState('');
  const [dataScraped, setDataScraped] = useState([]);
  const [alldata, setAllData] = useState([]);
  const [joinedData, setJoinedData] = useState('');
  const [splitData, setSplitData] = useState([]);

  useEffect(() => {
    getAllBooksJoined();
    getAllScraped();
  }, []);

  // Fetching all books
  const getAllBooksJoined = async () => {
    await fetch(`${VITE_BACKEND_URL}/api/getAllBooks`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data, "bookData");
        setData1(data.data);

        // Joining all abstract names
        const joinedNames = data.data.map(book => book.abstractname).join('.');
        setJoinedAbstractNames(joinedNames);
      });
  };

  const getAllScraped = async () => {
    await fetch(`${VITE_BACKEND_URL}/api/getScraped`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data, "bookData");
        setDataScraped(data.data);

        // Joining all abstract names
        const joinedNamesScraped = data.data.map(book => book.ScrapedAbstract).join('.');
        setJoinedScrapeAbstractNames(joinedNamesScraped);
      });
  };

  useEffect(() => {
    // Concatenate the joined abstract names
    const joinedDataString = joinedAbstractNames + ' ' + joinedScrapeAbstractNames;
    setJoinedData(joinedDataString);
  }, [joinedAbstractNames, joinedScrapeAbstractNames]);

  useEffect(() => {
    // Convert joinedData to lowercase
    const lowerCaseJoinedData = joinedData.toLowerCase();
  
    // Split the joined data by space and comma
    if (lowerCaseJoinedData) {
      const splitArray = lowerCaseJoinedData.split(/[",:\s]+/);
  
      // Remove non-alphanumeric characters from each item
      const cleanedSplitArray = splitArray.map(item => item.replace(/[^a-zA-Z0-9]/g, ''));
  
      // Remove all empty strings
      const filteredCleanedSplitArray = cleanedSplitArray.filter(item => item !== '');
  
      // Filter out words that are in partsOfSpeech list
      const filteredWords = filteredCleanedSplitArray.filter(word => !partsOfSpeech.partsOfSpeech.includes(word.toLowerCase()));
  
      // Sort the words alphabetically
      const sortedWords = filteredWords.sort((a, b) => a.localeCompare(b));
  
      // Set the sorted words
      setSplitData(sortedWords);
  
      // Find unique words and count them
      const uniqueWords = new Set(sortedWords);
      const uniqueWordsCount = uniqueWords.size;
  
      // Set the count of unique words
      setUniqueCount(uniqueWordsCount);
    }
  }, [joinedData]);
  

// useState for unique word count
const [uniqueCount, setUniqueCount] = useState(0);
const wordCounts = splitData.reduce((acc, word) => {
  acc[word] = (acc[word] || 0) + 1;
  return acc;
}, {});

// Sort words by count in descending order
const sortedWords = Object.keys(wordCounts).sort((a, b) => wordCounts[b] - wordCounts[a]);

const handleKeywordSelection = (keyword) => {
  if (selectedKeywords.includes(keyword)) {
    setSelectedKeywords(selectedKeywords.filter((selectedWord) => selectedWord !== keyword));
  } else {
    setSelectedKeywords([...selectedKeywords, keyword]);
  }
};

const [joinedSelectedKeywords, setJoinedSelectedKeywords] = useState('');

useEffect(() => {
  // Update joinedSelectedKeywords whenever selectedKeywords changes
  setJoinedSelectedKeywords(selectedKeywords.join(" | "));
}, [selectedKeywords]);

// Pass joinedSelectedKeywords to keywordsPass whenever it changes
useEffect(() => {
  keywordsPass(joinedSelectedKeywords);
}, [joinedSelectedKeywords, keywordsPass]);



const handleKeyDown = (e) => {
  if (e.key === 'Enter') {
    e.preventDefault(); // Prevent form submission

    // Check if startYear is greater than endYear
    if (parseInt(startYear) > parseInt(endYear)) {
      // Swap startYear and endYear
      const temp = startYear;
      setStartYear(endYear);
      setEndYear(temp);
    }
  }
};






 


  
  

  

  return (
    <>
   
   {/* <p>Default checked: {selectedCheck()}</p>
    */}
      <div class="p-2" style={{ width: "80%" }}>
  
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
          onClick={toggleTopicList}
        >
          <span>
            <label
              style={{
                fontFamily: "Arial",
                fontSize: "15px",
                fontWeight: "bold",
                color: "#333",
                cursor: "pointer",
              }}
            >
              Topic
            </label>
          </span>
          {showTopicList ? <IoIosArrowDown /> : <IoIosArrowForward />}
        </div>

        {showTopicList && (
          <>
            <hr
              class="d-flex justify-content-start"
              style={{
                width: "auto",
                borderTop: "1px solid #000000",
                margin: "0",
                marginTop: "10px",
                marginBottom: "10px",
              }}
            />

<ul>
  {selectedTopics.length === 0
    ? Object.entries(uniqueCategory)
        .filter(([category, count]) => {
          const coursesForCategory = data.filter(item => item.category === category).map(item => item.course);
          return selectedCourse.length === 0 || coursesForCategory.some(course => selectedCourse.includes(course));
        })
        .map(([category, count], index) => (
          <li
            key={index}
            className={`list-group-item d-flex justify-content-between align-items-center`}
            style={{ fontSize: '13px', cursor:"pointer" }}
            onClick={() => handleTopicSelection(category)} // Toggle selection
          >
            {category}<span>{count}</span>
          </li>
        ))
    : Object.entries(uniqueCategory)
        .filter(([category, count]) => {
          const coursesForCategory = data.filter(item => item.category === category).map(item => item.course);
          return selectedCourse.length === 0 || coursesForCategory.some(course => selectedCourse.includes(course));
        })
        .filter(([category, count]) => selectedTopics.includes(category)) // Filter based on selected topics
        .map(([category, count], index) => (
          <li
            key={index}
            className={`list-group-item d-flex justify-content-between align-items-center ${selectedTopics.includes(category) ? 'selected' : ''}`}
            style={{ fontSize: '13px', cursor:"pointer" }}
            onClick={() => handleTopicSelection(category)} // Toggle selection
          >
            {category}<span>{count}</span>
          </li>
        ))}
</ul>

          </>
        )}
      </div>

      <div class="p-2" style={{ width: "80%" }}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
          onClick={toggleCourseList}
        >
          <span>
            {" "}
            <label
              style={{
                fontFamily: "Arial",
                fontSize: "15px",
                fontWeight: "bold",
                color: "#333",
                cursor: "pointer",
              }}
            >
              Course
            </label>
          </span>
          {showCourseList ? <IoIosArrowDown /> : <IoIosArrowForward />}
        </div>
        {showCourseList && (
          <>
            <hr
              class="d-flex justify-content-start"
              style={{
                width: "auto", // Set width to auto to allow it to start from the left
                borderTop: "1px solid #000000",
                margin: "0",
                marginTop: "10px",
                marginBottom: "10px",
              }}
            />
           <ul>
  {selectedCourse.length === 0
    ? Object.entries(uniqueCourses)
        .filter(([course, count]) => {
          const topicsForCourse = data.filter(item => item.course === course).map(item => item.category);
          return selectedTopics.length === 0 || topicsForCourse.some(topic => selectedTopics.includes(topic));
        })
        .map(([course, count], index) => (
          <li
            key={index}
            className={`list-group-item d-flex justify-content-between align-items-center`}
            style={{ fontSize: '13px', cursor:"pointer" }}
            onClick={() => handleCourseSelection(course)} // Toggle selection
          >
            {course}<span>{count}</span>
          </li>
        ))
    : Object.entries(uniqueCourses)
        .filter(([course, count]) => {
          const topicsForCourse = data.filter(item => item.course === course).map(item => item.category);
          return selectedTopics.length === 0 || topicsForCourse.some(topic => selectedTopics.includes(topic));
        })
        .filter(([course, count]) => selectedCourse.includes(course)) // Filter based on selected courses
        .map(([course, count], index) => (
          <li
            key={index}
            className={`list-group-item d-flex justify-content-between align-items-center ${selectedCourse.includes(course) ? 'selected' : ''}`}
            style={{ fontSize: '13px' }}
            onClick={() => handleCourseSelection(course)} // Toggle selection
          >
            {course}<span>{count}</span>
          </li>
        ))}
</ul>

       
          </>
        )}

        <div className="mb-3 p-4">
          <label
            style={{
              fontFamily: "Arial",
              fontSize: "15px",
              fontWeight: "bold",
              color: "#333",
            }}
            className="list-group-item d-flex justify-content-between align-items-center"
          >
            All
            <span>
              <Form.Check
                checked={allChecked}
                onChange={handleAllCheckboxChange}
                style={{ filter: 'drop-shadow(3px 2px 1px rgba(0, 0, 0, 0.3))'}}
              />
            </span>
          </label>

          <hr
              class="d-flex justify-content-start"
              style={{
                width: "auto", // Set width to auto to allow it to start from the left
                borderTop: "1px solid #000000",
                margin: "0",
                marginTop: "10px",
                marginBottom: "10px",
              }}
            />
          

          <ul>
            <li className="list-group-item d-flex justify-content-between align-items-center" style={{fontSize:'13px'}}>
            Institutional
              <span>
                <Form.Check
                  checked={checkboxesChecked.Institutional}
                  onChange={() => handleCheckboxChange("Institutional")}
                  style={{ filter: 'drop-shadow(3px 2px 1px rgba(0, 0, 0, 0.3))'}}
                />
              </span>
            </li>
            <li className="list-group-item d-flex justify-content-between align-items-center" style={{fontSize:'13px'}}>
            Web Scraped
              <span>
                <Form.Check
                  checked={checkboxesChecked.WebScraped}
                  onChange={() => handleCheckboxChange("WebScraped")}
                  style={{ filter: 'drop-shadow(3px 2px 1px rgba(0, 0, 0, 0.3))'}}
                />
              </span>
            </li>
          </ul>
        </div>

        <div className="mb-3 p-4">
  <label
    style={{
      fontFamily: "Arial",
      fontSize: "15px",
      fontWeight: "bold",
      color: "#333",
    }}
    className="list-group-item d-flex justify-content-between align-items-center"
  >
    Year
    <span>
    
    </span>
  </label>

  <hr
    class="d-flex justify-content-start"
    style={{
      width: "auto", // Set width to auto to allow it to start from the left
      borderTop: "1px solid #000000",
      margin: "0",
      marginTop: "10px",
      marginBottom: "10px",
    }}
  />

  <ul>
  {Object.keys(uniqueYear).map((year, index) => (
          <li
            key={index}
            className="list-group-item d-flex justify-content-between align-items-center"
            style={{ fontSize: "13px" }}
          >
            {year}
            <span>
              <Form.Check
                style={{ filter: "drop-shadow(3px 2px 1px rgba(0, 0, 0, 0.3))" }}
                onChange={() => handleYearSelection(year)} // Update selected years
              />
            </span>
          </li>
        ))}

    
    {/* Custom Year Range */}
    <li className="list-group-item d-flex justify-content-between align-items-center mt-4" style={{fontSize:'11px'}}>
    <input
  type="text"
  placeholder="Start Year"
  className="form-control mr-2"
  style={{ maxWidth: '110px' }}
  value={startYear}
  onKeyDown={handleKeyDown}
  onChange={(e) => {
    let input = e.target.value.slice(0, 4);
    input = input.replace(/\D/g, ''); 

    if (input.length > 2) {
      const firstTwoDigits = input.slice(0, 2);
      if (firstTwoDigits !== "19" && firstTwoDigits !== "20") {
        input = input.slice(2);
      }
    }
    

    setStartYear(input);

  }}
/>

<input
  type="text"
  placeholder="End Year"
  className="form-control"
  style={{ maxWidth: '100px' }}
  value={endYear}
  onKeyDown={handleKeyDown}
  onChange={(e) => {
    let input = e.target.value.slice(0, 4);
    input = input.replace(/\D/g, ''); 

    if (input.length > 2) {
      const firstTwoDigits = input.slice(0, 2);
      if (firstTwoDigits !== "19" && firstTwoDigits !== "20") {
        input = input.slice(2);
      }
    }
    

    setEndYear(input);
  }}
/>



    </li>
  </ul>
</div>
<div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        
        >
          <span>
            {" "}
            <label
              style={{
                fontFamily: "Arial",
                fontSize: "15px",
                fontWeight: "bold",
                color: "#333",
                cursor: "pointer",
              }}
            >
              Keywords
            </label>
          </span>
         
        
        </div>
        
        <div style={{ marginLeft: "30px", color:'blue' }}>
        {/* Display selected words */}
       

        {/* Display remaining words */}
        {sortedWords.slice(0, 20).map((word, index) => (
          <span
            key={index}
            style={{ marginRight: "23px", display: "inline-block", marginTop: "10px", cursor: "pointer" }}
            onClick={() => handleKeywordSelection(word)} // Toggle selection
          >
            {word}
          </span>
        ))}
      </div>



    
       
       

  



       
        
      </div>
    </>
  );
};

export default SideTopic;
