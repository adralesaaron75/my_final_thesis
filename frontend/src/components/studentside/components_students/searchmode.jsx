import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import ThesisCard from "./ThesisCard";
import ThesisCardScraped from "./ThesisCardScraped";
import Footer from "./Footer";
import SearchForm from "./SearchForm";
import SideTopic from "./SideTopic";
import SearchResultCount from "./ResultCounter";
import logo from "../.././assets/images/logossu.png";
import nofound from "../.././assets/images/No-Found.png";
import { VITE_BACKEND_URL } from "../../../App";
import partsOfSpeech from "../../../../../parts_of_speech.json"


function SearchBar() {
  const [data, setData] = useState([]);
  const [dataScraped, setDataScraped] = useState([]);
  const [selectedTopics, setSelectedTopics] = useState([]);
  const [selectedCourse, setSelectedCourse] = useState([]);
  const [selectedTitle, setSelectedTitle] = useState("");
  const [selectedCheckData, setSelectedCheckData] = useState("");
  const [selectedYearsString, setSelectedYearsString] = useState("");
  const [selectedkeywords, setSelectedkeywords] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 10; // Set your desired items per page value

  useEffect(() => {
    getAllScraped();
    getAllBooks();
  }, []);

  const getAllScraped = async () => {
    try {
      const response = await fetch(`${VITE_BACKEND_URL}/api/getScraped`);
      const jsonData = await response.json();
      setDataScraped(jsonData.data);
    } catch (error) {
      console.error("Error fetching scraped data:", error);
    }
  };

  const getAllBooks = async () => {
    try {
      const response = await fetch(`${VITE_BACKEND_URL}/api/getAllBooks`);
      const jsonData = await response.json();
      setData(jsonData.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const limitText = (text, limit) => {
    return text.length > limit ? text.substring(0, limit) + "..." : text;
  };

  const removeSelectedTopic = (topicToRemove) => {
    setSelectedTopics(selectedTopics.filter((topic) => topic !== topicToRemove));
  };

  const removeSelectedCourse = (courseToRemove) => {
    setSelectedCourse(selectedCourse.filter((course) => course !== courseToRemove));
  };

  const checkYearInRange = (year, yearRange) => {
    if (!yearRange.includes("-")) {
      return yearRange.includes(year.toString());
    } else {
      const [start, end] = yearRange.split("-").map(Number);
      return year >= start && year <= end;
    }
  };

  const removeSelectedKeyword = (keywordToRemove) => {
    setSelectedkeywords(selectedkeywords.split('|').filter(keyword => keyword !== keywordToRemove).join('|'));
  };
  


  

  


  const filteredItems = data.map(item => {
    // Calculate the count of occurrences of the selected title in abstractname and titlename
    const titleOccurrences = (item.abstractname.match(new RegExp(selectedTitle, 'gi')) || []).length +
                             (item.titlename.match(new RegExp(selectedTitle, 'gi')) || []).length;
  
    return { ...item, titleOccurrences }; // Add titleOccurrences to the item
  }).filter(item => {
    // Apply your existing filtering conditions here
    const topicRegex = new RegExp(selectedTopics.join('|'), 'i');
    const categoryMatches = topicRegex.test(item.category);
    const keywords = selectedkeywords.toLowerCase().split(" | ");
    const titleKeywords = selectedTitle
      .toLowerCase()
      .split(" ")
      .filter(word => !partsOfSpeech.partsOfSpeech.includes(word.toLowerCase()));
      console.log(titleKeywords)
  
    return (
      (selectedTopics.length === 0 || categoryMatches) &&
      (selectedCourse.length === 0 || selectedCourse.includes(item.course)) &&
      ( ((selectedTitle === '' || item.abstractname.toLowerCase().includes(selectedTitle.toLowerCase())) ||
         (selectedTitle === '' || item.titlename.toLowerCase().includes(selectedTitle.toLowerCase()))) ||
         (titleKeywords.length === 0 || titleKeywords.some(keyword =>
          item.abstractname.toLowerCase().includes(keyword) ||
          item.titlename.toLowerCase().includes(keyword)
        )))
      &&
      (selectedYearsString === '' || checkYearInRange(item.yearpublished, selectedYearsString)) &&
      (keywords.length === 0 || keywords.some(keyword => 
        item.abstractname.toLowerCase().includes(keyword) || 
        item.titlename.toLowerCase().includes(keyword)
      ))
    );
  }).map(item => {
    // Calculate the total count of occurrences of all keywords in the title
    const totalTitleKeywordCount = selectedTitle
      .toLowerCase()
      .split(" ")
      .filter(word => !partsOfSpeech.partsOfSpeech.includes(word.toLowerCase()))
      .reduce((totalCount, keyword) => {
        const countInAbstract = (item.abstractname.match(new RegExp(keyword, 'gi')) || []).length;
        const countInTitle = (item.titlename.match(new RegExp(keyword, 'gi')) || []).length;
        return totalCount + countInAbstract + countInTitle;
      }, 0);
  
    return { ...item, totalTitleKeywordCount }; // Add totalTitleKeywordCount to the item
  }).sort((a, b) => b.totalTitleKeywordCount - a.totalTitleKeywordCount) // Sort based on totalTitleKeywordCount in descending order
    .sort((a, b) => b.titleOccurrences - a.titleOccurrences); // Sort based on titleOccurrences in descending order
  

  
  const filteredScrapedItems = dataScraped.map(item => {
    // Calculate the count of occurrences of the selected title in ScrapedTitle and ScrapedAbstract
    const titleOccurrences = (item.ScrapedTitle.match(new RegExp(selectedTitle, 'gi')) || []).length +
                             (item.ScrapedAbstract.match(new RegExp(selectedTitle, 'gi')) || []).length;
  
    return { ...item, titleOccurrences }; // Add titleOccurrences to the item
  }).filter(item => {
    // Apply your existing filtering conditions here
    return (
      ((selectedTitle === '' || item.ScrapedTitle.toLowerCase().includes(selectedTitle.toLowerCase())) ||
       (selectedTitle === '' || item.ScrapedAbstract.toLowerCase().includes(selectedTitle.toLowerCase()))) &&
      (selectedkeywords === '' || item.ScrapedAbstract.toLowerCase().includes(selectedkeywords.toLowerCase()))
    );
  }).sort((a, b) => b.titleOccurrences - a.titleOccurrences); // Sort based on titleOccurrences in descending order
  

  const allFilteredItems = [...filteredItems, ...filteredScrapedItems];
  const totalCount = allFilteredItems.length;
  const totalPages = Math.ceil(totalCount / itemsPerPage);

  const handleSelectedTitle = (selectedTitle) => {
    setSelectedTitle(selectedTitle);
  };

  const renderThesisCards = () => {
    return allFilteredItems.slice((currentPage - 1) * itemsPerPage, currentPage * itemsPerPage).map((item, index) => {
      if (item.hasOwnProperty('yearpublished')) {
        return (
          <ThesisCard
            key={index}
            date={item.yearpublished}
            author={item.authorjoin}
            title={item.titlename}
            content={item.abstractname}
            categories={[item.category]}
            course={item.course}
            image={item.image}
          />
        );
      } else {
        return (
          <ThesisCardScraped
            key={index}
            Scrapeddate={item.ScrapedLink}
            Scrapedauthor={item.ScrapedAuthor}
            Scrapedtitle={item.ScrapedTitle}
            Scrapedcontent={item.ScrapedAbstract}
          />
        );
      }
    });
  };

  



  const renderThesisCard = () => {
    const startIndex = (currentPage - 1) * itemsPerPage;
    const endIndex = currentPage * itemsPerPage;
    const slicedItems = filteredItems.slice(startIndex, endIndex);
  
    return slicedItems.map((item, index) => (
      <ThesisCard
        key={index}
        date={item.yearpublished}
        author={item.authorjoin}
        title={item.titlename}
        content={item.abstractname}
        categories={[item.category]}
        course={item.course}
        image={item.image}
      />
    ));
  };
  
  const renderThesisCardScraped = () => {
    // Check if selectedTopics or selectedCourse have a value
    if (selectedTopics.length === 0 && selectedCourse.length === 0) {
      // If neither selectedTopics nor selectedCourse have a value, render ThesisCardScraped
      const startIndex = (currentPage - 1) * itemsPerPage;
      const endIndex = currentPage * itemsPerPage;
      const slicedItems = filteredScrapedItems.slice(startIndex, endIndex);
  
      return slicedItems.map((item, index) => (
        <ThesisCardScraped
          key={index}
          Scrapeddate={item.ScrapedLink}
          Scrapedauthor={item.ScrapedAuthor}
          Scrapedtitle={item.ScrapedTitle}
          Scrapedcontent={item.ScrapedAbstract}
        />
      ));
    } else {
      // If either selectedTopics or selectedCourse have a value, return null (don't render anything)
      return null;
    }
  };
  




  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const renderPageButtons = () => {
    const totalPages = Math.ceil(totalCount / itemsPerPage);
    const pageButtons = [];
    for (let i = 1; i <= totalPages; i++) {
      pageButtons.push(
        <Button
          key={i}
          variant="outline-secondary"
          onClick={() => handlePageChange(i)}
          disabled={i === currentPage}
          style={{marginRight:'3px', marginLeft:'3px'}}
        >
          {i}
        </Button>
      );
    }
    return pageButtons;
  };

  







  return (
    <div>
    

      <div style={{ background: "maroon", width: "100%", height: "30vh" }}>
        <Container>
  
      
        
          
          <div style={{ position: 'relative' }}>
          <SearchForm handleSelectedTitle={handleSelectedTitle} />
            <img src={logo} alt="Logo" width={170} height={170} style={{ filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.99))', position: 'absolute', top: '-7px', left: '97%', transform: 'translateX(-50%)' }} />
          </div>
         
        
          <div style={{ paddingLeft: '50px', width: '80%', display:'flex'}}>
            {selectedTopics.length > 0 && (
              <div style={{ marginTop: "10px" }}>
                {selectedTopics.map((topic, index) => (
                  <div
                    key={index}
                    className="selected-topic d-inline-block me-2 mb-2 px-2 py-1 rounded"
                    style={{ backgroundColor: "white", color: "black", fontSize: "0.7rem", fontWeight: "bold" }}
                  >
                    {topic}
                    <button
                      type="button"
                      className="btn-close ms-2"
                      aria-label="Close"
                      onClick={() => removeSelectedTopic(topic)}
                    ></button>
                  </div>
                ))}
              </div>
            )}

            
            {selectedCourse.length > 0 && (
              <div style={{ marginTop: "10px" }}>
                {selectedCourse.map((course, index) => (
                  <div
                    key={index}
                    className="selected-topic d-inline-block me-2 mb-2 px-2 py-1 rounded"
                    style={{ backgroundColor: "white", color: "black", fontSize: "0.7rem", fontWeight: "bold" }}
                  >
                    {course}
                    <button
                      type="button"
                      className="btn-close ms-2"
                      aria-label="Close"
                      onClick={() => removeSelectedCourse(course)}
                    ></button>
                  </div>
                ))}
              </div>
            )}


{selectedkeywords && (
             <div style={{ marginTop: "10px" }}>
      {selectedkeywords.split('|').map((keyword, index) => (
        <div
          key={index}
          className="selected-topic d-inline-block me-2 mb-2 px-2 py-1 rounded"
          style={{ backgroundColor: "white", color: "black", fontSize: "0.7rem", fontWeight: "bold" }}
        >
          {keyword.trim()} {/* trim to remove leading/trailing whitespace */}
          <button
            type="button"
            className="btn-close ms-2"
            aria-label="Close"
            onClick={() => removeSelectedKeyword(keyword)}
          ></button>
        </div>
      ))}
      </div>
          )}
          </div>

         


        </Container>
      </div>
     

      <Row style={{ marginTop: "20px" }}>
        <Col style={{ marginLeft: "auto" }}>
          <SideTopic
            selectedTopics={selectedTopics}
            setSelectedTopics={setSelectedTopics}
            removeSelectedTopic={removeSelectedTopic}
            selectedCourse={selectedCourse}
            setSelectedCourse={setSelectedCourse}
            removeSelectedCourse={removeSelectedCourse}
            setSelectedCheckData={setSelectedCheckData}
            selectedYearPass={setSelectedYearsString}
            keywordsPass={setSelectedkeywords}
          />
        </Col>
        <Col style={{ marginRight: "3%"}}>
          <div className="d-flex justify-content-end">
            <SearchResultCount
              currentPage={currentPage}
              itemsPerPage={itemsPerPage}
              totalItems={totalCount}
            />
          </div>
          {totalCount === 0 ? (
            <div style={{ padding: '10px', marginTop: '10px', color: 'black', width: "70vw", display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
              <img src={nofound} style={{ maxWidth: '100%', height: 'auto' }} alt="No Results" />
              <p style={{ marginLeft: '70px', fontSize: '1.9rem' }}>No results found</p>
            </div>
          ) : (
            <React.Fragment>
              <div style={{width: "70vw"}}>
              {selectedCheckData === "Institutional" && renderThesisCard()}
      {selectedCheckData === "Web Scraped" && renderThesisCardScraped()}
      {selectedCheckData === "All" && renderThesisCards()}
      {selectedCheckData === "" && renderThesisCards()}
                
              </div>
             

         

            </React.Fragment>
          )}
          
          
          <div className="d-flex justify-content-end mt-5">
      {/* Previous Page Button */}
      <Button
        variant="outline-secondary"
        onClick={() => handlePageChange(currentPage - 1)}
        disabled={currentPage === 1}
        style={{marginRight:'10px'}}
      >
        Previous
      </Button>
      {/* Page Buttons */}
      {renderPageButtons()}
      {/* Next Page Button */}
      <Button
        variant="outline-secondary"
        onClick={() => handlePageChange(currentPage + 1)}
        disabled={currentPage === totalPages}
        style={{marginLeft:'10px'}}
      >
        Next
      </Button>
    </div>

        </Col>
      </Row>
      <Footer />
      
    </div>
    
  );
}

export default SearchBar;
