import React, { useState } from "react";

const StarRating = () => {
  const [rating, setRating] = useState(0);

  const handleClick = (value) => {
    setRating(value);
  };

  return (
    <div>
      {[1, 2, 3, 4, 5].map((value) => (
        <Star
          key={value}
          filled={value <= rating}
          onClick={() => handleClick(value)}
        />
      ))}
    </div>
  );
};

const Star = ({ filled, onClick }) => {
  return (
    <span
      style={{
        color: filled ? "#f5e105" : "black",
        cursor: "pointer",
        fontSize: "25px",
      }}
      onClick={onClick}
    >
      {filled ? "★" : "☆"}
    </span>
  );
};

export default StarRating;
