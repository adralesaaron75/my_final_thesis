import { Form, Button, Nav, Container, ModalBody,NavLink } from 'react-bootstrap';
import { FaUserCircle, FaEye,FaPlus} from 'react-icons/fa';
import Footer from './Footer';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Table, Modal, Row, Col} from 'react-bootstrap';
import { VITE_BACKEND_URL } from '../../../App';
import Badge from "react-bootstrap/esm/Badge";



function SubmmitedPage() {

  const [userData, setUserData] = useState(null);
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [step, setStep] = useState(1);

  useEffect(() => {
      fetchUserData();
  }, []);

  useEffect(() => {
      if (userData) {
          fetchBorrowedBooks();
      }
  }, [userData]);

  const fetchUserData = async () => {
      try {
          setIsLoading(true);
          // Fetch user data here
          const response = await axios.post(`${VITE_BACKEND_URL}/api/userData`, {
              token: window.localStorage.getItem("token")
          });
          setIsLoading(false);
          setUserData(response.data.data);
      } catch (error) {
          setIsLoading(false);
          console.error('Error fetching user data:', error);
      }
  }

  const fetchBorrowedBooks = async () => {
      try {
          setIsLoading(true);
          const id = getId();
          const response = await axios.get(`${VITE_BACKEND_URL}/api/borrowedbooklists/${id}`);
          setIsLoading(false);
          if (response.data.success) {
              const filteredData = response.data.data.filter(item => item.Submittedby === userData.email);
              setData(filteredData);
          }
      } catch (error) {
          setIsLoading(false);
          console.error('Error fetching borrowed books:', error);
      }
  }

  const getId = () => {
      // Your getId logic
  }

  const [showModal, setShowModal] = useState(false);


  const handleModalClose = () => {
    setShowModal(false);
    setSelectedTitle("");
    setSelectedAbstract("");
    setStep(1); // Reset step to 1 when modal is closed
  };


  const [formData, setFormData] = useState({
    SubmitTitleName: "",
    SubmitAbstract: "",
    category: "",
    YearPublished: "",
    course: "",
    contactemail: "",
    Submittedby: "",
    SubmittedName: "",
    SubmitAuthor:"",
    base64:"",
    wordCount: 0 // Add wordCount to the initial state
  });

  
  const handleChange = (e) => {
    const { name, value } = e.target;
    let wordCount = name === "SubmitAbstract" ? value.trim().split(/\s+/).length : formData.wordCount; // Only count words if changing the abstract
    setFormData({
      ...formData,
      [name]: value,
      wordCount: wordCount
    });
  };

  
  const [errors, setErrors] = useState({});

  const handleNext = async () => {
    try {
      const errors = {};
      
      // Validate the title
      if (formData.SubmitTitleName.trim() === '') {
        errors.SubmitTitleName = 'Title is required';
      } else if (formData.SubmitTitleName.trim().split(/\s+/).length < 5) {
        errors.SubmitTitleName = 'Title must contain invalid';
      }
      
      // Validate the abstract
      if (formData.SubmitAbstract.trim() === '') {
        errors.SubmitAbstract = 'Abstract is required';
      } else if (formData.SubmitAbstract.trim().split(/\s+/).length < 150) {
        errors.SubmitAbstract = 'Abstract must contain at least 150 words';
      }
      
      // Update the errors state
      setErrors(errors);
  
      // Proceed to the next step if there are no errors
      if (Object.keys(errors).length === 0) {
        setStep(step + 1);
      }
    } catch (error) {
      console.error('Validation error:', error);
      // Handle validation errors
    }
  };
    

  const colorsData = [
    { label: 'Desktop Games', value: 'Desktop Games' },
    { label: 'Desktop Applications', value: 'Desktop Applications' },
    { label: 'Web Games', value: 'Web Games' },
    { label: 'Web Applications', value: 'Web Applications' },
    { label: 'Mobile Games', value: 'Mobile Games' },
    { label: 'Mobile Applications', value: 'Mobile Applications' },
    { label: 'Enterprise and E-commerce', value: 'Enterprise and E-commerce' },
    { label: 'Organization Applications', value: 'Organizational Applications' },
    { label: 'Other', value: 'Other' },
  ];

  const [selectedColors, setSelectedColors] = useState([]);

  const toggleColor = (color) => {
    setSelectedColors((prevSelected) => {
      if (prevSelected.includes(color)) {
        return prevSelected.filter((c) => c !== color);
      } else {
        return [...prevSelected, color];
      }
    });
    onChange(color);
  };

  const removeColor = (color) => {
    setSelectedColors((prevSelected) => prevSelected.filter((c) => c !== color));
  };

  const [image, setImage] = useState(""); // Excluded based on your request

function covertToBase64(e) {
  const file = e.target.files[0];
  if (file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log(reader.result); // Base64 encoded string
      setImage(reader.result);
      setFormData({
        ...formData,
        base64: reader.result,
      });
    };
    reader.onerror = (error) => {
      console.error("Error reading the file:", error);
    };
  } else {
    console.error("No file selected.");
  }
}


const [authors, setAuthors] = useState([{ id: 1, name: "", email: "" }]);


const addAuthor = () => {
  const newId = authors.length + 1;
  const newAuthor = { id: newId, name: "", email: "" }; // Create a new author object with empty fields
  setAuthors([...authors, newAuthor]);
};

const removeAuthor = (id) => {
  setAuthors(prevAuthors => {
    const updatedAuthors = prevAuthors.filter(author => author.id !== id);
    // Update the IDs of the remaining authors
    return updatedAuthors.map((author, index) => ({
      ...author,
      id: index + 1
    }));
  });
};


const [selectedTitle, setSelectedTitle] = useState("");
const [selectedAbstract, setSelectedAbstract] = useState("");

// Function to handle when FaEye icon is clicked
const handleViewDetails = (title, abstract) => {
  setSelectedTitle(title);
  setSelectedAbstract(abstract);
};











  return (
    <>
      <div className="container mt-4">
        {/* Account page navigation*/}
        <Nav className="nav-borders">
          <Nav.Link className="nav-link active" href="/profile" >Profile</Nav.Link>
          <Nav.Link className="nav-link" href="/submittedhome" >Submitted</Nav.Link>
        </Nav>
        <hr className="my-4" />
        <Container>
          <table className="table">
            <thead>
              <tr>
                <th></th>
                <th>Title</th>
                <th>Submitted Date</th>
                <th>Status</th>
              </tr>
            </thead>
         
            {userData ? (
                  
                       
                        <tbody>
                            {data.map((item, index) => {

        const submissionDate = new Date(item.createdAt);
     
        const formattedDate =`${submissionDate.getMonth() + 1}-${submissionDate.getDate()}-${submissionDate.getFullYear()}`;
        return (
            <tr key={index}>
               <td onClick={() => { handleViewDetails(item.SubmitTitleName, item.SubmitAbstract); setShowModal(true); }}><FaEye/></td>
                <td>{item.SubmitTitleName}</td>
                <td>{formattedDate}</td> {/* Display the formatted date */}
                <td>
                {item.status === "pending" ? (
                    <Badge bg="warning">{item.status}</Badge>
                ) : item.status === "approved" ? (
                    <Badge bg="success">{item.status}</Badge>
                ) : (
                    <Badge bg="danger">{item.status}</Badge>
                )}
                </td>
            </tr>
        );
    })}
                        </tbody>

            ) : (
                <p>Loading user data...</p>
            )}
            
          </table>
        
        </Container>
      </div>



      <Modal size="xl" show={showModal} onHide={handleModalClose} >

        <ModalBody>

        <Container
        style={{ filter: "drop-shadow(0 15px 14px rgba(128, 0, 0, 0.5))" }}
      >


       
      </Container>

      <Form.Group controlId="formSubmitTitleName">
      <Form.Label>Title</Form.Label>
      <Form.Control
        type="text"
        name="SubmitTitleName"
        placeholder="Enter title"
        value={selectedTitle} // Display selected title
        onChange={handleChange}
      />
    </Form.Group>
    <Form.Group controlId="formSubmitTitleName">
      <Form.Label>Title</Form.Label>
      <Form.Control
        type="text"
        name="SubmitTitleName"
        placeholder="Enter title"
        value={selectedAbstract} // Display selected title
        onChange={handleChange}
      />
    </Form.Group>



        </ModalBody>


      </Modal>













      <Footer />
    </>
  );
}

export default SubmmitedPage;
