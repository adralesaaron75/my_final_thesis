import React, { useState } from 'react';

const HeartReacting = () => {
  const [isLiked, setIsLiked] = useState(false);

  const handleLikeClick = () => {
    setIsLiked(!isLiked);
  };

  return (
    <span
      style={{
        color: isLiked ? "red" : "black",
        cursor: "pointer",
        fontSize: "25px",
      }}
      onClick={handleLikeClick}
    >
      {isLiked ? "♥" : "♡"}
    </span>
  );
};

export default HeartReacting;
