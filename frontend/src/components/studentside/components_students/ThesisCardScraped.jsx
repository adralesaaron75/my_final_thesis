import React, { useState, useEffect } from "react";
import { Row, Col, Modal, Container } from "react-bootstrap";
import { FaBookmark } from 'react-icons/fa';

import { FaHeart } from "react-icons/fa";
// import StarRating from "./StarRating";
import HeartReacting from "./Heart";
import {
  IoIosArrowDown,
  IoIosArrowUp,
  IoIosArrowForward,
} from "react-icons/io";

const ThesisCardScraped = ({
  Scrapeddate,
  Scrapedtitle,
  Scrapedcontent,
  Scrapedauthor,
 
}) => {
  const [expanded, setExpanded] = useState(false);
  const [truncatedTitle, setTruncatedTitle] = useState("");
  const [truncatedContent, setTruncatedContent] = useState(Scrapedcontent);
  const [showTopicList, setShowTopicList] = useState(false);

  const toggleTopicList = () => {
    setShowTopicList(!showTopicList);
  };

  useEffect(() => {
    if (Scrapedtitle.length > 50) {
      setTruncatedTitle(Scrapedtitle.substring(0, 50) + "...");
    } else {
      setTruncatedTitle(Scrapedtitle);
    }
  }, [Scrapedtitle]);

  const handleArrowClick = () => {
    setExpanded(!expanded);
  };

  useEffect(() => {
    if (expanded) {
      setTruncatedContent(Scrapedcontent);
    } else {
      if (Scrapedcontent.length > 450) {
        setTruncatedContent(Scrapedcontent.substring(0, 450) + "...");
      } else {
        setTruncatedContent(Scrapedcontent);
      }
    }
  }, [expanded, Scrapedcontent]);



  const [showModal, setShowModal] = useState(false);

  const handleModalClose = () => {
    setShowModal(false);
  };




  return (
    <div>
    <div
      style={{
        padding: "10px",
        width: "70vw",
        height: expanded ? "auto" : "auto",
        background: "White",
        filter: "drop-shadow(0 9px 8px rgba(0, 0, 0, 0.15))",
        marginTop: "30px",
      }}
      onClick={() => setShowModal(true)}
      className="d-flex justify-content-center"
    >
      
      <Row>
        <Col>
        <FaBookmark color="Blue"/>
       
         
        
          
         

        </Col>

        <Col className="d-flex justify-content-end">
          
            <p><HeartReacting /></p>
    
        </Col>

        <div >
          
            <div style={{ marginLeft: "30px" }}>
            <h3 style={{ fontWeight:'bold', fontFamily:'ITC Garamond'}}>{truncatedTitle}  </h3>
              <h6 style={{ fontSize: "10px", color: "#888" }}>
              {Scrapedauthor} - {Scrapeddate}
              </h6>
            </div>
          
            <p
              style={{
                marginLeft: "70px",
                cursor: "pointer",
              }}
              // onClick={handleArrowClick}
            >
              {truncatedContent}
            </p>
         
        </div>

        <footer style={{ marginTop: "auto" }}>
          <div
            className="d-flex justify-content-center "
            
          >
            {expanded ? (
              <IoIosArrowUp style={{ cursor: "pointer" }} />
            ) : (
              <IoIosArrowDown style={{ cursor: "pointer" }} />
            )}
          </div>
        </footer>
      </Row>

      </div>

      <Modal size="xl" show={showModal} onHide={handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title style={{width:'92%'}}></Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* <p>{content}</p> */}
          <Container>
  <Row>
  <Col md={2}>
  <div style={{ background: '#7F7F7F', width: '100%', height: '100%', borderRadius: '4px' }}>
    {/* Placeholder content */}
    Picture Box
  </div>
</Col>

    <Col style={{ background: 'white' }}>
    <Row >
                  <Col md={1}>
                    <p
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                        fontSize:'12px',
                      }}
                    >
                      Title:
                    </p>
                  </Col>
                  <Col>
                    <p>
                      <span style={{ color: "black", fontSize: "17px" }}>
                        {Scrapedtitle}
                      </span>
                    </p>
                  </Col>
                </Row>
               
                <Row>
                  <Col md={1} >
                    <p
                      style={{
                        color: "black",
                        fontSize:'12px',
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Author:
                    </p>
                  </Col>
                  <Col xs={3} md={11}>
                    <p>
                      <span style={{ color: "black", fontSize: "12px" }}>
                        {Scrapedauthor}
                      </span>
                    </p>
                  </Col>
                </Row>
               
                <Row>
                  <Col md={1} >
                    <p
                      style={{
                        color: "black",
                        fontSize:'12px',
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Year:
                    </p>
                  </Col>
                  <Col xs={3} md={11}>
                    <p>
                      <span style={{ color: "black", fontSize: "12px" }}>
                        {Scrapeddate}
                      </span>
                    </p>
                  </Col>
                </Row>


                
    </Col>
    
  </Row>

  


  
</Container>
<Container className="mt-5">
  <div>
  
    <div style={{marginLeft:'30px'}}>Abstract:</div>
    <Container className="mt-3" style={{ width:'90%', textIndent: '70px' }}>{Scrapedcontent}</Container>
    </div>
    <div className="mt-5">

    <div class="p-2" style={{ width: "25%" }}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                  onClick={toggleTopicList}
                 
                >
                  
                  <div
                    style={{ marginLeft: "30px" }}
                    
                  >
                    <h5>...</h5>
                  </div>
                  {showTopicList ? <IoIosArrowDown /> : <IoIosArrowForward />}
                </div>
              </div>
              {showTopicList && (
          <>
            <hr
              class="d-flex justify-content-start"
              style={{
                width: "auto", // Set width to auto to allow it to start from the left
                borderTop: "1px solid #000000",
                margin: "0",
                marginTop: "10px",
                marginBottom: "10px",
              }}
            />
            
          </>
        )}

    </div>
    
  
</Container>



        </Modal.Body>
        <Modal.Footer>
          <button className="btn btn-secondary" onClick={handleModalClose}>
            Close
          </button>
        </Modal.Footer>
      </Modal>

      
    </div>
  );
};

export default ThesisCardScraped;
