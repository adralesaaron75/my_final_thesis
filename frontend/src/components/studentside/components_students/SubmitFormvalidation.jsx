
import * as Yup from "yup";

export const SubmitFormValidation = Yup.object({
  SubmitTitleName: Yup.string()
    .min(5, "Title must be at least 5 characters")
    .required("Title is required"),
  SubmitAbstract: Yup.string()
    .matches(/\b\w+\b/g, {
      message: "Abstract must contain at least 150 words",
      excludeEmptyString: true
    })
    .required("Abstract is required")
});
