import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Form, Button, Nav, Container } from 'react-bootstrap';
import { FaUserCircle } from 'react-icons/fa';
import Footer from './Footer';
import { NavLink } from "react-router-dom";
import { VITE_BACKEND_URL } from '../../../App';

function ProfilePage() {
  // Get User Data
  const [userData, setUserData] = useState(null);
  const [student, setStudent] = useState(false);

  useEffect(() => {
    fetchUserData();
  }, []);

  const fetchUserData = () => {
    fetch(`${VITE_BACKEND_URL}/api/userData`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify({
        token: window.localStorage.getItem("token"),
      }),
    })
    .then((res) => {
      if (!res.ok) {
        throw new Error(`HTTP error! Status: ${res.status}`);
      }
      return res.json();
    })
    .then((data) => {
      console.log(data, "userData");
      if (data.data.userType === "student") {
        setStudent(true);
      }
      setUserData(data.data);
    })
    .catch((error) => {
      console.error("Error fetching user data:", error);
      // Handle the error (e.g., set an error state or display a message to the user)
    });
  };

  // Handle form submission
  const handleFormSubmit = (event) => {
    event.preventDefault();
    // Prepare updated user data from form inputs
    const updatedUserData = {
      name: event.target.inputUsername.value,
      firstName: event.target.inputFirstName.value,
      lastName: event.target.inputLastName.value,
      course: event.target.inputCourse.value,
      yearandsection: event.target.inputYear.value,
      email: event.target.inputEmailAddress.value,
      phoneNumber: event.target.inputPhone.value,
    };
    // Send updated user data to the server
    axios.post(`${VITE_BACKEND_URL}/api/updateUserData`, updatedUserData)
      .then((response) => {
        // Optionally, handle success response
        console.log("User data updated successfully:", response.data);
        // Fetch updated user data
        fetchUserData();
      })
      .catch((error) => {
        console.error("Error updating user data:", error);
        // Handle the error (e.g., set an error state or display a message to the user)
      });
  };

  return (
    <>
      <div className="container mt-4">
        {/* Account page navigation*/}
        <Nav className="nav-borders">
          <Nav.Link className="nav-link active" href="/profile">Profile</Nav.Link>
          <Nav.Link className="nav-link active" href="/submittedhome">Submitted</Nav.Link>
        </Nav>
        <hr className="my-4" />
        <Container>
          <div className="row">
            <div className="col-lg-3">
              {/* Profile picture card*/}
              <div className="card mb-4">
                <div className="card-header">Profile Picture</div>
                <div className="card-body text-center">
                  {/* Profile picture image*/}
                  <div className="profile-picture mt-3">
                    <FaUserCircle size={90} />
                  </div>
                  {/* Profile picture help block*/}
                  <div className="small font-italic text-muted mt-3">Name</div>
                  {/* Profile picture upload button*/}
                  <Button className="btn btn-primary mt-4" style={{ backgroundColor: "maroon", color: "white" }}>Upload</Button>
                </div>
              </div>
            </div>

            <div className="col-lg-9">
              {/* Account details card*/}
              <div className="card mb-4">
                <div className="card-header">Account Details</div>
                <div className="card-body">
                  <Form onSubmit={handleFormSubmit}>
                    {/* Form Group (username)*/}
                    <div className="mb-3">
                      <label className="small mb-1" htmlFor="inputUsername">Username</label>
                      <input className="form-control" id="inputUsername" type="text" placeholder="Enter your username" defaultValue={userData?.name} />
                    </div>
                    {/* Form Row*/}
                    <div className="row gx-3 mb-3">
                      {/* Form Group (first name)*/}
                      <div className="col-md-6">
                        <label className="small mb-1" htmlFor="inputFirstName">First name</label>
                        <input className="form-control" id="inputFirstName" type="text" placeholder="Enter your first name" defaultValue={userData?.firstName} />
                      </div>
                      {/* Form Group (last name)*/}
                      <div className="col-md-6">
                        <label className="small mb-1" htmlFor="inputLastName">Last name</label>
                        <input className="form-control" id="inputLastName" type="text" placeholder="Enter your last name" defaultValue={userData?.lastName} />
                      </div>
                    </div>
                    {/* Form Row*/}
                    <div className="row gx-3 mb-3">
                      {/* Form Group (organization name)*/}
                      <div className="col-md-9">
                        <label className="small mb-1" htmlFor="inputCourse">Course</label>
                        <input className="form-control" id="inputCourse" type="text" placeholder="Enter your Course" defaultValue={userData?.course} />
                      </div>
                      {/* Form Group (location)*/}
                      <div className="col-md-3">
                        <label className="small mb-1" htmlFor="inputYear">Year</label>
                        <input className="form-control" id="inputYear" type="text" placeholder="Enter Year" defaultValue={userData?.yearandsection} />
                      </div>
                    </div>
                    {/* Form Row*/}
                    <div className="row gx-3 mb-3">
                      {/* Form Group (phone number)*/}
                      <div className="col-md-7">
                        <label className="small mb-1" htmlFor="inputEmailAddress">Email address</label>
                        <input className="form-control" id="inputEmailAddress" type="email" placeholder="Enter your email address" defaultValue={userData?.email} />
                      </div>
                      {/* Form Group (birthday)*/}
                      <div className="col-md-5">
                        <label className="small mb-1" htmlFor="inputPhone">Phone number</label>
                        <input className="form-control" id="inputPhone" type="tel" placeholder="Enter your phone number" defaultValue={userData?.phoneNumber} />
                      </div>
                    </div>
                    {/* Save changes button*/}
                    <Button type="submit" className="btn btn-primary float-end mt-5" style={{ backgroundColor: "maroon", color: "white" }}>Save changes</Button>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </Container>
      </div>
      <Footer />
    </>
  );
}

export default ProfilePage;
