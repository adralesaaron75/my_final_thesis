import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Form, Container, Button} from 'react-bootstrap';
import { VITE_BACKEND_URL } from '../../../App';
import partsOfSpeech from '../../../../../parts_of_speech.json';
import Filterdate from '../../adminside/components_admin/Filterdate';
import spellingquery from "../../../../../spellingcorrected.json"
function BorrowBookForm() {

  const [data, setData] = useState([]);
  const [joinedAbstractNames, setJoinedAbstractNames] = useState('');
  const [joinedScrapeAbstractNames, setJoinedScrapeAbstractNames] = useState('');
  const [dataScraped, setDataScraped] = useState([]);
  const [alldata, setAllData] = useState([]);
  const [joinedData, setJoinedData] = useState('');
  const [splitData, setSplitData] = useState([]);

  useEffect(() => {
    getAllBooks();
    getAllScraped();
  }, []);

  // Fetching all books
  const getAllBooks = async () => {
    await fetch(`${VITE_BACKEND_URL}/api/getAllBooks`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data, "bookData");
        setData(data.data);

        // Joining all abstract names
        const joinedNames = data.data.map(book => book.abstractname).join('.');
        setJoinedAbstractNames(joinedNames);
      });
  };

  const getAllScraped = async () => {
    await fetch(`${VITE_BACKEND_URL}/api/getScraped`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data, "bookData");
        setDataScraped(data.data);

        // Joining all abstract names
        const joinedNamesScraped = data.data.map(book => book.ScrapedAbstract).join('.');
        setJoinedScrapeAbstractNames(joinedNamesScraped);
      });
  };

  useEffect(() => {
    // Concatenate the joined abstract names
    const joinedDataString = joinedAbstractNames + ' ' + joinedScrapeAbstractNames;
    setJoinedData(joinedDataString);
  }, [joinedAbstractNames, joinedScrapeAbstractNames]);

  useEffect(() => {
  // Split the joined data by space and comma
  if (joinedData) {
    const splitArray = joinedData.split(/[",:\s]+/);

    // Remove non-alphanumeric characters from each item
    const cleanedSplitArray = splitArray.map(item => item.replace(/[^a-zA-Z0-9]/g, ''));

    // Remove all empty strings
    const filteredCleanedSplitArray = cleanedSplitArray.filter(item => item !== '');

    // Filter out words that are in partsOfSpeech list
    const filteredWords = filteredCleanedSplitArray.filter(word => !partsOfSpeech.partsOfSpeech.includes(word.toLowerCase()));

    // Sort the words alphabetically
    const sortedWords = filteredWords.sort((a, b) => a.localeCompare(b));

    // Set the sorted words
    setSplitData(sortedWords);

    // Find unique words and count them
    const uniqueWords = new Set(sortedWords);
    const uniqueWordsCount = uniqueWords.size;

    // Set the count of unique words
    setUniqueCount(uniqueWordsCount);
  }
}, [joinedData]);

// useState for unique word count
const [uniqueCount, setUniqueCount] = useState(0);
const wordCounts = splitData.reduce((acc, word) => {
  acc[word] = (acc[word] || 0) + 1;
  return acc;
}, {});

// Sort words by count in descending order
const sortedWords = Object.keys(wordCounts).sort((a, b) => wordCounts[b] - wordCounts[a]);









const [searchTerm, setSearchTerm] = useState('');
const [correctedQuery, setCorrectedQuery] = useState('');


const handleChange = (e) => {
  setSearchTerm(e.target.value);
};

useEffect(() => {
  // Set the correctedQuery when the component mounts
  if (spellingquery && spellingquery.correctedQuery) {
    setCorrectedQuery(spellingquery.correctedQuery);
  }
}, []);

const handleSaveAndRun = () => {
  // Save the form value to a JSON file on the backend
  axios.post('http://localhost:3000/spellingsaveFormToJson', { searchTerm })
    .then(() => {
      // After saving, run the Node.js script
      return axios.post('http://localhost:3000/runcodeforspelling', { searchTerm });
    })
    .then(response => {
      console.log(response.data);
      // Handle success of running the script, if needed
    })
    .catch(error => {
      console.error(error);
      // Handle error, if needed
    });
};

const handleCorrectedQueryClick = () => {
  // Update the search term with the corrected query
  setSearchTerm(correctedQuery);
};










  return (
//     <div>
//       <div>
        
//         <h3>Split Data:  {splitData.length}</h3>
//         <h3>Split Data:</h3>
//         <ul>
//         {sortedWords.map((word, index) => (
//     <li key={index}>
//       {word} 
//       ({wordCounts[word]})
//     </li>
//   ))}
// </ul>







//       </div>
//     </div>

<Container>
<Form.Control
                 type="text"
                 placeholder="Enter search term"
                 value={searchTerm}
                 onChange={handleChange}
                    style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
              />
               <Button onClick={handleSaveAndRun} type="submit" style={{background:'maroon'}}>
              Next
            </Button>

            {spellingquery.correctedQuery !== "" &&
      <p>Did You mean: <span onClick={handleCorrectedQueryClick} style={{ textDecoration: 'underline', fontStyle: 'italic', color: 'blue', cursor: "pointer" }}>{correctedQuery}</span></p>
    }

</Container>
  );
}

export default BorrowBookForm;
