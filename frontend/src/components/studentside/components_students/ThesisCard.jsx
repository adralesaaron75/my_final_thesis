import React, { useState, useEffect } from "react";
import { Row, Col, Modal, Container, Button } from "react-bootstrap";
import { FaBookmark, FaWindowClose } from 'react-icons/fa';
import partsOfSpeech from '../../../../../parts_of_speech.json';

import { FaHeart } from "react-icons/fa";
// import StarRating from "./StarRating";
import HeartReacting from "./Heart";
import {
  IoIosArrowDown,
  IoIosArrowUp,
  IoIosArrowForward,
} from "react-icons/io";

const ThesisCard = ({
  date,
  title,
  content,
  author,
  categories,
  course,
  image
}) => {
  const [expanded, setExpanded] = useState(false);
  const [truncatedTitle, setTruncatedTitle] = useState("");
  const [truncatedContent, setTruncatedContent] = useState(content);
  const [showTopicList, setShowTopicList] = useState(false);


  const toggleTopicList = () => {
    setShowTopicList(!showTopicList);
  };

  useEffect(() => {
    if (title.length > 50) {
      setTruncatedTitle(title.substring(0, 50) + "...");
    } else {
      setTruncatedTitle(title);
    }
  }, [title]);

  const handleArrowClick = () => {
    setExpanded(!expanded);
  };

  useEffect(() => {
    if (expanded) {
      setTruncatedContent(content);
    } else {
      if (content.length > 450) {
        setTruncatedContent(content.substring(0, 450) + "...");
      } else {
        setTruncatedContent(content);
      }
    }
  }, [expanded, content]);



  const [showModal, setShowModal] = useState(false);

  const handleModalClose = () => {
    setShowModal(false);
  };

  const combinedText = `${title}, ${content}`;
  const lowerCaseJoinedData = combinedText.toLowerCase();
  const wordsArray = lowerCaseJoinedData.split(/[\s.]+/);
  const cleanedSplitArray = wordsArray.map(item => item.replace(/[^a-zA-Z0-9]/g, '')).filter(item => item !== '');;
  const filteredWords = cleanedSplitArray.filter(word => !partsOfSpeech.partsOfSpeech.includes(word.toLowerCase()));
  const sortedWords = filteredWords.sort((a, b) => a.localeCompare(b));

  const wordCounts = {};
sortedWords.forEach((word) => {
  if (wordCounts[word]) {
    wordCounts[word]++;
  } else {
    wordCounts[word] = 1;
  }
});

// Sort the words based on their counts in descending order
const sortedWordsByCount = Object.keys(wordCounts).sort((a, b) => wordCounts[b] - wordCounts[a]);


const containerStyle = {
  margin: "35px auto",
  width: '98%',

};

const contentStyle = {
  color: 'black'
};

const sidebarStyle = {
  width: '20vw',
  float: 'right',
  padding: '20px',
  backgroundColor: '#f9f9f9',
  border: '1px solid #ddd',
  borderRadius: '5px',
  marginLeft: '25px',
  marginBottom: '25px',
  boxShadow: "0px 2px 5px rgba(0, 0, 0, 0.3)",
  
};




  return (
    <div>
    <div
      style={{
        padding: "10px",
        width: "70vw",
        height: expanded ? "auto" : "auto",
        background: "White",
        filter: "drop-shadow(0 9px 8px rgba(0, 0, 0, 0.15))",
        marginTop: "30px",
      }}
      onClick={() => setShowModal(true)}
      className="d-flex justify-content-center"
    >
      
      <Row>
        <Col>
        <FaBookmark color="maroon"/>
       
         
        
          
         

        </Col>

        <Col className="d-flex justify-content-end">
          
            <p><HeartReacting /></p>
    
        </Col>

        <div >
          
            <div style={{ marginLeft: "30px" }}>
              <h3 style={{ fontWeight:'bold', fontFamily:'Bahnschrift SemiBold'}}>{truncatedTitle}  </h3>
              <h6 style={{ fontSize: "10px", color: "#888" }}>
              {author} - {date}
              </h6>
            </div>
          
            <p
              style={{
                marginLeft: "70px",
                cursor: "pointer",
                fontFamily: "Segoe UI Semilight",
                fontSize:'17px',
                
              }}
              // onClick={handleArrowClick}
            >
              {truncatedContent}
            </p>
         
        </div>

        <footer style={{ marginTop: "auto" }}>
          <div
            className="d-flex justify-content-center "
            
          >
            {expanded ? (
              <IoIosArrowUp style={{ cursor: "pointer" }} />
            ) : (
              <IoIosArrowDown style={{ cursor: "pointer" }} />
            )}
          </div>
        </footer>
      </Row>

      </div>

      <Modal size="xl" show={showModal} onHide={handleModalClose} >
      {/* <Modal.Header style={{ background: '#FBF0CB' }} closeButton closeButtonProps={{ variant: 'outline-danger' }}>

      
          
        
        </Modal.Header> */}
<Button
  onClick={() => setShowModal(false)}
  
  style={{
    position: 'absolute',
    top: '-10px',
    right: '-10px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: "white",
    border: "none",
    borderRadius: "50%",
    width: "35px",
    height: "35px",
    fontSize: "1rem", // Increase the font size
    cursor: "pointer",
    boxShadow: "0px 2px 5px rgba(0, 0, 0, 0.3)",
    outline: "none",
    marginBottom: "20px",
    zIndex:'9999',
    padding:'0',
    background:"maroon"
  }}
>
  <FaWindowClose />
</Button>




        <Modal.Body style={{background:'#fbfaf9', marginBottom:'20px'}}>
          {/* <p>{content}</p> */}

          


          
<Container style={{ display: 'flex', minHeight: 'auto' }}>
  <div style={containerStyle}>
  <div style={ sidebarStyle }>
  <div style={{ display: 'flex', justifyContent: 'center' }}>
    <div style={{ 
      width: '9vw', 
      height: '25vh', 
      backgroundColor: '#ddd', 
      display: 'flex', 
      justifyContent: 'center', 
      alignItems: 'center' 
    }}>
         {image == "" || image == null ? (
            <img
              width={60}
              height={60}
              src="https://st.depositphotos.com/2934765/53192/v/450/depositphotos_531920820-stock-illustration-photo-available-vector-icon-default.jpg"
              alt="default image"
              style={{ width: "auto", height: "auto%" }} // Changed width and height
            />
          ) : (
            <img width={60} height={60} src={image} style={{ width: "100%", height: "auto" }} /> // Changed width and height
          )}
       
    </div>
</div>

  <Container className="mt-5">

  <Row>

  <Col xs={3}>
    <p
      style={{
        color: "black",
        fontSize: '12px',
        fontWeight: "bold",
        textAlign: "start",
      }}
    >
      
      Category:
    </p>
  </Col>
  <Col xs={9}>
    <p style={{ color: "black", fontSize: "12px" }}>
      {categories}
    </p>
  </Col>
</Row>


<Row>

<Col xs={3}>
  <p
    style={{
      color: "black",
      fontSize: '12px',
      fontWeight: "bold",
      textAlign: "start",
    }}
  >
      Author:
  </p>
</Col>
<Col xs={9}>
  <p style={{ color: "black", fontSize: "12px" }}>
  {author.split('|').map((name, index) => (
        <span key={index}>{name.trim()}<br /></span>
      ))}
  </p>
</Col>
</Row>

<Row>

<Col xs={3}>
  <p
    style={{
      color: "black",
      fontSize: '12px',
      fontWeight: "bold",
      textAlign: "start",
    }}
  >
      Course:
  </p>
</Col>
<Col xs={9}>
  <p style={{ color: "black", fontSize: "12px" }}>
  {course}
  </p>
</Col>
</Row>

<Row>

<Col xs={3}>
  <p
    style={{
      color: "black",
      fontSize: '12px',
      fontWeight: "bold",
      textAlign: "start",
    }}
  >
      Year:
  </p>
</Col>
<Col xs={9}>
  <p style={{ color: "black", fontSize: "12px" }}>
  {date}
  </p>
</Col>
</Row>

<Row>

<Col xs={3}>
  <p
    style={{
      color: "black",
      fontSize: '12px',
      fontWeight: "bold",
      textAlign: "start",
    }}
  >
      Keyword:
  </p>
</Col>
<Col xs={9}>
  <p style={{ color: "blue", fontSize: "12px" }}>

       
      {sortedWordsByCount.map((word, index) => {
        // Check if the count of the current word is greater than 1
        if (wordCounts[word] > 3) {
          return (
            <span key={index} style={{ marginRight: "10px", display: "inline-block"}}>
              {word}
              </span>
          );
        }
        return null; 
      })}
  
  
  </p>
</Col>
</Row>






               
              
              
                
 

  


  
</Container>

  
  
</div>


    <div style={contentStyle}>
    <h3 style={{
  fontWeight: 'bold',
  fontFamily: 'Lucida Sans Typewriter',
  color: '#4A89D6', // Text color
 
  textShadow: '-1px 2px 2px rgba(0, 0, 0, 0.4)', // Text shadow
  // backgroundColor: 'lightgray', // Background color
  padding: '10px', // Padding
  borderRadius: '5px', // Border radius
}}>
  {title}
</h3>

    
<Container className="mt-5">
  <div>
  
    <div style={{marginLeft:'30px'}}>Abstract:</div>
 

    <Container className="mt-3" style={{ width:'90%', textIndent: '70px', fontFamily:'Dubai', fontSize:'19px'}}>{content}</Container>
    </div>
    
    
  
</Container>


    </div>
    
  </div>
</Container>
<div className="mt-2">

    <div class="p-2" style={{ width: "25%" }}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                  onClick={toggleTopicList}
                 
                >
                  
                  <div
                    style={{ marginLeft: "30px" }}
                    
                  >
                    <h5>Reference</h5>
                  </div>
                  {showTopicList ? <IoIosArrowDown /> : <IoIosArrowForward />}
                </div>
              </div>
              {showTopicList && (
          <>
            <hr
              class="d-flex justify-content-start"
              style={{
                width: "auto", // Set width to auto to allow it to start from the left
                borderTop: "1px solid #000000",
                margin: "0",
                marginTop: "10px",
                marginBottom: "10px",
              }}
            />

<div style={{ marginLeft: "70px", color:'blue'}}>

  <Container>
      
         
                   
                 



    </Container>
    
                      </div>
            
          </>
        )}
        
        

    </div>




        </Modal.Body>
      
       
      </Modal>

      
    </div>
  );
};

export default ThesisCard;
