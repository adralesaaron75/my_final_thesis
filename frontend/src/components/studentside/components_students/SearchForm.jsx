import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form, InputGroup, Button} from 'react-bootstrap';
import { FaSearch } from 'react-icons/fa';
import { VITE_BACKEND_URL } from "../../../App";
import partsOfSpeech from "../../../../../parts_of_speech.json"
import spellingquery from "../../../../../spellingcorrected.json"
import axios from 'axios';
import Loading from '../../adminside/components_admin/Loading';

const SearchForm = ({ handleSelectedTitle }) => {


  const [searchTerm, setSearchTerm] = useState('');
  const [data, setData] = useState([]);
  const [joinedAbstractNames, setJoinedAbstractNames] = useState('');
  const [filteredItems, setFilteredItems] = useState([]);
  const [selectedIndex, setSelectedIndex] = useState(-1); // Track the index of the selected item\
  const [loading, setLoading] = useState(true);


 const handleChange = (e) => {
  const inputValue = e.target.value;
  setSearchTerm(inputValue);
  filterItems(inputValue);

  // Check if input value is empty
  if (inputValue.trim() === '') {
    // Reload the window
    // window.location.reload();
   
  }
};

const handleSelectedTitleWithDelay = (selectedTitle) => {
  // Delay execution by 5 seconds
  setTimeout(() => {
    // Pass the selected title to the parent component after 5 seconds
    handleSelectedTitle(selectedTitle);
  }, 5000); // 5000 milliseconds = 5 seconds
};



  const handleKeyDown = (e) => {
    if (e.key === 'ArrowDown') {
      e.preventDefault(); // Prevent scrolling
      setSelectedIndex(prevIndex => (prevIndex < filteredItems.length - 1) ? prevIndex + 1 : prevIndex);
    } else if (e.key === 'Enter') {
      e.preventDefault(); // Prevent form submission
      handleLoading()
      if (selectedIndex !== -1) {
        setSearchTerm(filteredItems[selectedIndex].props.children.replace(/<[^>]+>/g, '')); // Remove HTML tags
        setSelectedIndex(-1);
        // Pass the selected item to the parent component
        handleSelectedTitle(filteredItems[selectedIndex].props.children.replace(/<[^>]+>/g, ''));
      } else {
        // If no item is selected, pass the current value of the search term
        handleSelectedTitleWithDelay(searchTerm);

      }
      setFilteredItems([]); // Clear filtered items
      handleSaveAndRun()
    }
  };

  useEffect(() => {
    getAllData();
  }, []);
  
  const getAllData = async () => {
    try {
      // Fetching books data
      const booksResponse = await fetch(`${VITE_BACKEND_URL}/api/getAllBooks`);
      const booksData = await booksResponse.json();
      console.log(booksData, "bookData");

  
      // Fetching scraped data
      const scrapedResponse = await fetch(`${VITE_BACKEND_URL}/api/getScraped`);
      const scrapedData = await scrapedResponse.json();
      console.log(scrapedData, "scrapedData");
  
      // Joining all abstract names and title names from books data
      const booksNames = booksData.data.map(book => `${book.abstractname}. ${book.titlename}`).join('. ');
  
      // Assuming ScrapedTitle is an array of strings containing titles
      const scrapedTitles = scrapedData.data.map(item => `${item.ScrapedTitle}. ${item.ScrapedAbstract}`).join('. ');
      // Joining all scraped titles
     
  
      // Combine both sets of names
      const allNames = `${booksNames}. ${scrapedTitles}`;
  
      setJoinedAbstractNames(allNames);
      setFilteredItems(uniqueListItems); // Initialize filteredItems with all unique items
      setLoading(false);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  

  // Splitting joined names by period and comma, setting unique, and sorting from shortest to longest
  const uniqueSplittedNames = Array.from(new Set(joinedAbstractNames
    .toLowerCase() // Convert to lowercase
    .split(/[.,]/)
    .map(name => name.trim()) // Trim whitespace
    .filter(name => name !== '') // Remove empty strings
  )).sort((a, b) => a.length - b.length);

  const uniqueListItems = uniqueSplittedNames.map((name, index) => (
    <li key={index} className={selectedIndex === index ? 'list-unstyled selected' : 'list-unstyled'} onClick={() => handleItemClick(name)}>{name}</li>
  ));

 
const filterItems = (searchTerm) => {
  
  if (!searchTerm) {
    setFilteredItems([]);
    return;
  }

  const filteredSet = new Set();
  uniqueListItems.forEach((item) => {
    const itemText = item.props.children.toLowerCase();
    if (itemText.includes(searchTerm.toLowerCase())) {
      const startIndex = itemText.indexOf(searchTerm.toLowerCase());
      const spaceIndex = itemText.lastIndexOf(' ', startIndex);
      const extractedText = spaceIndex !== -1 ? itemText.substring(spaceIndex + 1) : itemText.substring(startIndex);
   // Replace the current line inside filterItems function
const highlightedText = extractedText.replace(new RegExp(searchTerm.toLowerCase(), 'gi'), match => `<strong style="font-weight: bold; background-color: #f7f7f7">${match}</strong>`);





      // Check word count and exclude if greater than 15
      const wordCount = extractedText.trim().split(/\s+/).length;
      if (wordCount <= 15) {
        filteredSet.add(highlightedText);
      }
    }
  });
  // Convert set to array and sort filtered items from shortest to longest
  let filteredArray = [...filteredSet].sort((a, b) => a.length - b.length);
  // Limit to 12 items
  filteredArray = filteredArray.slice(0, 12);
  // Convert filtered items back to JSX elements
  const filteredElements = filteredArray.map((text, index) => <li key={index} dangerouslySetInnerHTML={{ __html: text }} className={selectedIndex === index ? 'list-unstyled selected' : 'list-unstyled'} onClick={() => handleItemClick(text)} />);
  setFilteredItems(filteredElements);
};













const handleItemClick = (text) => {
  setSearchTerm(text.replace(/<[^>]+>/g, '')); // Remove HTML tags
  setSelectedIndex(-1);
  // Pass the selected item to the parent component
  handleSelectedTitleWithDelay(text.replace(/<[^>]+>/g, ''));
  handleLoading()
  setFilteredItems([]); // Clear filtered items
 
};





const [correctedQuery, setCorrectedQuery] = useState('');



useEffect(() => {
  // Set the correctedQuery when the component mounts
  if (spellingquery && spellingquery.correctedQuery) {
    setCorrectedQuery(spellingquery.correctedQuery);
  }
}, []);



const handleSaveAndRun = () => {
  // Save the form value to a JSON file on the backend
  axios.post('http://localhost:3000/spellingsaveFormToJson', { searchTerm })
    .then(() => {
      // After saving, run the Node.js script
      return axios.post('http://localhost:3000/runcodeforspelling', { searchTerm });
    })
    .then(response => {
      console.log(response.data);
      // Handle success of running the script, if needed
    })
    .catch(error => {
      console.error(error);
      // Handle error, if needed
    });
};

const handleCorrectedQueryClick = () => {
  // Update the search term with the corrected query
  setSearchTerm(correctedQuery);
};



const handleLoading = () => {
  setLoading(true);
  setTimeout(() => {
    setLoading(false);
  }, 6000);
};





 
  return (
    <Container style={{ position: "relative" }}>
      {/* <Button onClick={handleLoading}>
        click
      </Button> */}
       {loading && (
          <div
            style={{
              position: "fixed",
              top: 0,
              left: 0,
              width: "100%",
              height: "100%",
              background: "rgba(255, 255, 255, 0.5)",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              zIndex: 9999,
            }}
          >
            <Loading />
          </div>
        )}
      <Row>
        <Col>
          <Form.Group>
            <InputGroup className="mb-3" style={{ width: "50vw", marginTop: "10px" }}>
              <InputGroup.Text id="basic-addon1" style={{ borderRadius: "20px 0 0 20px", background: "#f0f0f0", border: "none" }}>
                <FaSearch />
              </InputGroup.Text>
              <Form.Control
                type="text"
                placeholder="Enter search term"
                value={searchTerm}
                onChange={handleChange}
                onKeyDown={handleKeyDown}
                style={{ borderRadius: "0 20px 20px 0", border: "1px solid #ccc" }}
                spellCheck={true} 
              />
              
              <Form.Group style={{ position: 'absolute', left: 0, top: 'calc(100% + 0px)', marginLeft: '40px', width: '92%', background: 'white', zIndex: 2, color: 'black', display: filteredItems.length && filteredItems.length <= 13 ? 'block' : 'none' }}>
  {searchTerm && (
    <div>
      <ul style={{ cursor: 'pointer' }}>
        {filteredItems}
      </ul>
    </div>
  )}
</Form.Group>



            </InputGroup>
          </Form.Group>
          {searchTerm && spellingquery.correctedQuery !== "" && (
  <p style={{color:'white'}}>
    Did you mean: <span onClick={handleCorrectedQueryClick} style={{ textDecoration: 'underline', fontStyle: 'italic', color: 'blue', cursor: "pointer" }}>
      {correctedQuery}
    </span>
  </p>
)}

        </Col>
      </Row>
    </Container>
  );
}

export default SearchForm;


