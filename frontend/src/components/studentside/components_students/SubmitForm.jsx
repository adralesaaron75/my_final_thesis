import React, { useState, useEffect } from 'react';
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import footerssu from "../.././assets/images/footerSSu.png";
import { Container, Row, Col, Dropdown } from "react-bootstrap";
import { FaPlus, FaTimes } from "react-icons/fa";
import ChipDropCourse from "../../adminside/components_admin/DropdownCourse";
import ChipCust from "../../adminside/components_admin/DropdownCategory";
import { VITE_BACKEND_URL } from "../../../App";
import axios from 'axios'; // Assuming you're using axios for HTTP requests
import { NavLink } from 'react-router-dom';
import { toast } from 'react-toastify';
import { SubmitFormValidation } from './SubmitFormvalidation';
import * as Yup from "yup";
import 'react-toastify/dist/ReactToastify.css';
import { Formik } from 'formik';




function SubmitForm() {
  const [step, setStep] = useState(1);
  const [formData, setFormData] = useState({
    SubmitTitleName: "",
    SubmitAbstract: "",
    category: "",
    YearPublished: "",
    course: "",
    contactemail: "",
    Submittedby: "",
    SubmittedName: "",
    SubmitAuthor:"",
    base64:"",
    wordCount: 0, // Add wordCount to the initial 
    OpennedorNot:"",

  });

  

  
  
  








  
//Get User Datra
const [userData, setUserData] = useState(null);
const [student, setStudent] = useState(false);

useEffect(() => {
    fetch(`${VITE_BACKEND_URL}/api/userData`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify({
        token: window.localStorage.getItem("token"),
      }),
    })
    .then((res) => {
      if (!res.ok) {
        throw new Error(`HTTP error! Status: ${res.status}`);
      }
      return res.json();
    })
    .then((data) => {
      console.log(data, "userData");
      if (data.data.userType === "student") {
        setStudent(true);
      }
      setUserData(data.data);
    })
    .catch((error) => {
      console.error("Error fetching user data:", error);
      // Handle the error (e.g., set an error state or display a message to the user)
    });
  }, []);



  const handleChange = (e) => {
    const { name, value } = e.target;
    let wordCount = name === "SubmitAbstract" ? value.trim().split(/\s+/).length : formData.wordCount; // Only count words if changing the abstract
    setFormData({
      ...formData,
      [name]: value,
      wordCount: wordCount
    });
  };
  



  // const handleSubmit = async (e) => {
  //   e.preventDefault();
  //   try {
  //     // Ensure userData is available
     
  //     if (userData) {
  //       const response = await axios.post(`${VITE_BACKEND_URL}/api/add-borrowbook`, {
  //         ...formData,
  //         category: selectedColors.join(' | '),
  //         Submittedby: userData.email,
  //         SubmittedName: userData.name
  //       });
  //       console.log(response.data); // Assuming you want to log response
  //       // Reset form data after successful submission
  //       setFormData({
  //         SubmitTitleName: '',
  //         SubmitAbstract: '',
  //         YearPublished: '',
  //         category: '',
  //         course: '',
  //         contactemail: '',
  //         Submittedby: userData.email,
  //         SubmittedName: userData.name,
  //         daysborrow: '',
  //         studentName: '', // Reset studentName field
  //         studentid: '',
  //         referenceCode: '',
  //       });
  //       setSelectedColors([]);
  //       toast.success("Add Successfully");

  //     // Delay the redirection by 1 second (1000 milliseconds)
  //     setTimeout(() => {
  //       window.location.href = "/submittedhome";
  //     }, 2000);
      
  //     } else {
  //       console.error("User data is not available.");
  //     }
  //   } catch (error) {
  //     console.error('Error submitting form:', error);
  //   }
  // };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      // Ensure userData is available
      if (userData) {
        const errors = {};
  
        // Validate the contact email for each author
        authors.forEach((author) => {
          if (author.email.trim() === '') {
            errors[`contactemail-${author.id}`] = `Contact email for Author ${author.id} is required`;
          } else if (!author.email.trim().endsWith('@gmail.com')) {
            errors[`contactemail-${author.id}`] = `Contact email for Author ${author.id} must end with @gmail.com`;
          }
        });
  
        authors.forEach((author) => {
          if (author.name.trim() === '') {
            errors[`author-${author.id}`] = `Author ${author.id} name is required`;
          } else if (author.name.trim().split(/\s+/).length < 2) {
            errors[`author-${author.id}`] = `Author ${author.id} name must invalid`;
          }
        });
  
        if (formData.YearPublished.trim() === '') {
          errors['YearPublished'] = 'Year Published is required';
        } 
  
        if (formData.course.trim() === '') {
          errors['course'] = 'Course is required';
        }
  
  
        if (selectedColors.length === 0) {
          errors['category'] = 'At least one category must be selected';
        }
  
        // Update the errors state
        setErrors(errors);
  
        // Proceed with submission if there are no errors
        if (Object.keys(errors).length === 0) {
          const response = await axios.post(`${VITE_BACKEND_URL}/api/add-borrowbook`, {
            ...formData,
            category: selectedColors.join(' | '),
            Submittedby: userData.email,
            SubmittedName: userData.name,
            SubmitAuthor: displayAuthors(), // Save displayAuthors result to SubmitAuthor field
            base64: image, // Include base64 encoded image

            
          });
          console.log(response.data); // Assuming you want to log response
          // Reset form data after successful submission
          setFormData({
            SubmitTitleName: '',
            SubmitAbstract: '',
            YearPublished: '',
            category: '',
            course: '',
            contactemail: '',
            Submittedby: userData.email,
            SubmittedName: userData.name,
            daysborrow: '',
            studentName: '', // Reset studentName field
            SubmitAuthor: displayAuthors(),
            studentid: '',
            referenceCode: '',
            base64: formData.base64, // Use formData.base64 instead of image
            OpennedorNot:''
          });
          setSelectedColors([]);
          toast.success("Add Successfully");
  
          // Delay the redirection by 1 second (1000 milliseconds)
          setTimeout(() => {
            window.location.href = "/submittedhome";
          }, 2000);
        } else {
          console.error('Form submission has errors:', errors);
        }
      } else {
        console.error("User data is not available.");
      }
    } catch (error) {
      console.error('Error submitting form:', error);
    }
  };
  
  
  



  const [image, setImage] = useState(""); // Excluded based on your request

function covertToBase64(e) {
  const file = e.target.files[0];
  if (file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log(reader.result); // Base64 encoded string
      setImage(reader.result);
      setFormData({
        ...formData,
        base64: reader.result,
      });
    };
    reader.onerror = (error) => {
      console.error("Error reading the file:", error);
    };
  } else {
    console.error("No file selected.");
  }
}







  const [authors, setAuthors] = useState([{ id: 1, name: "", email: "" }]);


  const addAuthor = () => {
    const newId = authors.length + 1;
    const newAuthor = { id: newId, name: "", email: "" }; // Create a new author object with empty fields
    setAuthors([...authors, newAuthor]);
  };
  
  const removeAuthor = (id) => {
    setAuthors(prevAuthors => {
      const updatedAuthors = prevAuthors.filter(author => author.id !== id);
      // Update the IDs of the remaining authors
      return updatedAuthors.map((author, index) => ({
        ...author,
        id: index + 1
      }));
    });
  };
  


  const [errors, setErrors] = useState({});

  const handleNext = async () => {
    try {
      const errors = {};
      
      // Validate the title
      if (formData.SubmitTitleName.trim() === '') {
        errors.SubmitTitleName = 'Title is required';
      } else if (formData.SubmitTitleName.trim().split(/\s+/).length < 5) {
        errors.SubmitTitleName = 'Title must contain invalid';
      }
      
      // Validate the abstract
      if (formData.SubmitAbstract.trim() === '') {
        errors.SubmitAbstract = 'Abstract is required';
      } else if (formData.SubmitAbstract.trim().split(/\s+/).length < 150) {
        errors.SubmitAbstract = 'Abstract must contain at least 150 words';
      }
      
      // Update the errors state
      setErrors(errors);
  
      // Proceed to the next step if there are no errors
      if (Object.keys(errors).length === 0) {
        setStep(step + 1);
      }
    } catch (error) {
      console.error('Validation error:', error);
      // Handle validation errors
    }
  };
    


  

  const colorsData = [
    { label: 'Desktop Games', value: 'Desktop Games' },
    { label: 'Desktop Applications', value: 'Desktop Applications' },
    { label: 'Web Games', value: 'Web Games' },
    { label: 'Web Applications', value: 'Web Applications' },
    { label: 'Mobile Games', value: 'Mobile Games' },
    { label: 'Mobile Applications', value: 'Mobile Applications' },
    { label: 'Enterprise and E-commerce', value: 'Enterprise and E-commerce' },
    { label: 'Organization Applications', value: 'Organizational Applications' },
    { label: 'Other', value: 'Other' },
  ];

  const [selectedColors, setSelectedColors] = useState([]);

  const toggleColor = (color) => {
    setSelectedColors((prevSelected) => {
      if (prevSelected.includes(color)) {
        return prevSelected.filter((c) => c !== color);
      } else {
        return [...prevSelected, color];
      }
    });
    onChange(color);
  };

  const removeColor = (color) => {
    setSelectedColors((prevSelected) => prevSelected.filter((c) => c !== color));
  };




  const displayAuthors = () => {
    return authors.map((author, index) => {
      // Construct the string for each author
      let authorInfo = `${author.name}(${author.email})`;
  
      // Append comma if it's not the last author
      if (index !== authors.length - 1) {
        authorInfo += '| ';
      }
  
      return authorInfo;
    }).join(''); // Join the array of author strings into a single string
  };


  
  







  return (
    <>
      <Container
        style={{ filter: "drop-shadow(0 15px 14px rgba(128, 0, 0, 0.5))" }}
      >
        <Row className="justify-content-md-center mt-5">
          <Col xs={2}>
            <div style={{ padding: "10px" }}>
              <ul style={{ listStyleType: "none", padding: 0 }}>
                <li
                  onClick={() => setStep(1)}
                  style={{
                    background: step === 1 ? "maroon" : "white",
                    color: step === 1 ? "white" : "black",
                    padding: "10px",
                    marginBottom: "5px",
                    borderRadius: "5px",
                    cursor: "pointer",
                  }}
                >
                  Step 1 
                </li>
                {formData.SubmitTitleName.trim().split(/\s+/).length >= 5 && formData.SubmitAbstract.trim().split(/\s+/).length >= 150 && (
  <li
    onClick={() => {
      if (step === 2) {
        return;
      }
      setStep(2);
    }}
    style={{
      background: step === 2 ? "maroon" : "white",
      color: step === 2 ? "white" : "black",
      padding: "10px",
      marginBottom: "5px",
      borderRadius: "5px",
      cursor: "pointer",
      opacity: 1,
      pointerEvents: "auto",
    }}
  >
    Step 2
  </li>
)}

              </ul>
            </div>
          </Col>

          <Col xs={8}>

            <div className="card p-5">
              {step === 1 && (
               <Form onSubmit={handleSubmit}>
      <Form.Group controlId="formSubmitTitleName">
        <Form.Label>Title</Form.Label>
        <Form.Control
          type="text"
          name="SubmitTitleName"
          placeholder="Enter title"
          value={formData.SubmitTitleName}
          onChange={handleChange}
          isInvalid={!!errors.SubmitTitleName}
        />
        <Form.Control.Feedback type="invalid">{errors.SubmitTitleName}</Form.Control.Feedback>
      </Form.Group>

      <Form.Group controlId="formSubmitAbstract" className="mt-4">
        <Form.Label>Abstract</Form.Label>
        <p
          style={{
            position: 'absolute',
            top: '155px',
            right: '50px',
            display: 'flex',
            fontSize: '12px'
          }}
        >
          {formData.wordCount}/150
        </p>

        <Form.Control
          as="textarea"
          rows={10}
          name="SubmitAbstract"
          placeholder="Enter abstract"
          value={formData.SubmitAbstract}
          onChange={handleChange}
          isInvalid={!!errors.SubmitAbstract}
        />
        <Form.Control.Feedback type="invalid">{errors.SubmitAbstract}</Form.Control.Feedback>
      </Form.Group>

      <Button
        className="mt-3"
        variant="primary"
        onClick={handleNext}
        style={{
          backgroundColor: 'maroon',
          color: 'white',
          float: 'right'
        }}
      >
        Next
      </Button>
    </Form>
              )}





              
              {step === 2 && (
                <Form onSubmit={handleSubmit}>



<div className="d-flex justify-content-between">
            <div style={{ width: "90vw", marginRight: "5px" }}>
            <Form.Group controlId="categoryname">
  <Form.Label>Category</Form.Label>
  <div>
    <select
      className={`form-select ${errors['category'] ? 'is-invalid' : ''}`}
      value={''}
      onChange={(e) => toggleColor(e.target.value)}
    >
      <option value="">Select a Category</option>
      {colorsData.map((color) => (
        <option
          key={color.value}
          value={color.value}
          className={selectedColors.includes(color.value) ? 'selected' : ''}
        >
          {color.label}
        </option>
      ))}
    </select>

    {selectedColors.length > 0 && (
      <div className="card p-2">
        <div className="selected-colors mb-1">
          {selectedColors.map((colorValue) => {
            const selectedColor = colorsData.find(item => item.value === colorValue);
            return (
              <div
                key={colorValue}
                className="selected-color d-inline-block me-2 mb-2 px-2 py-1 rounded"
                style={{  color: 'black', fontSize: '0.7rem', fontWeight: 'bold' }}
              >
                {selectedColor.label}
                <button
                  type="button"
                  className="btn-close ms-2"
                  aria-label="Close"
                  onClick={() => removeColor(colorValue)}
                ></button>
              </div>
            );
          })}
        </div>
      </div>
    )}

    <Form.Control.Feedback type="invalid">{errors['category']}</Form.Control.Feedback>
  </div>
</Form.Group>
            </div>
            <div style={{ width: "20vw" }}>
              <Form.Group controlId="yearpublished">
                <Form.Label>Year</Form.Label>
                <Form.Control
    type="date"
    name="YearPublished"
    value={formData.YearPublished}
    onChange={handleChange}
    isInvalid={!!errors['YearPublished']} // Set isInvalid based on error presence
    style={{ width: "100%" }}
  />
  <Form.Control.Feedback type="invalid">{errors['YearPublished']}</Form.Control.Feedback>
</Form.Group>
            </div>
          </div>






                  
<div className="d-flex justify-content-between mt-3">
            <div style={{ width: "40%", marginRight: "5px" }}>
            <Form.Group controlId="Course">
  <Form.Label>Course</Form.Label>
  <Form.Control
    as="select"
    className={`form-select ${errors['course'] ? 'is-invalid' : ''}`}
    value={formData.course}
    onChange={handleChange}
    name="course"
  >
    <option value="">Select a Course</option>
    <option value="BSCS">BSCS</option>
    <option value="BSIT">BSIT</option>
    <option value="BSIS">BSIS</option>
  </Form.Control>
  <Form.Control.Feedback type="invalid">{errors['course']}</Form.Control.Feedback>
</Form.Group>
            </div>
            <div style={{ width: "60%" }}>
              <Form.Group controlId="uploadimage">
              <Form.Label >Upload Image</Form.Label>
            <Form.Control
              type="file"
              accept="image/*"
              required
              onChange={covertToBase64}
              placeholder="No file Chosen"
              style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
            />
          </Form.Group>
            </div>
          </div>





                {authors.map((author, index) => (
  <div
    key={author.id}
    style={{ display: "flex", marginBottom: "10px", marginTop:'15px'}}
  >
    <Form.Group
      style={{
        flexGrow: 1,
        marginRight: "10px",
        marginBottom: 0,
      }}
    >
      <Form.Label>{`Author ${author.id}`}</Form.Label>
      <Form.Control
  type="text"
  name={`author-${author.id}`}
  placeholder={`Enter Author ${author.id}`}
  onChange={(e) => {
    // Validate input to allow only alphabets, spaces, and possibly other characters like hyphens or apostrophes
    const regex = /^[a-zA-Z\s'-]*$/;
    const inputValue = e.target.value;

    // If the input matches the regex pattern or is empty, update the author name
    if (inputValue === '' || regex.test(inputValue)) {
      const updatedAuthors = [...authors];
      updatedAuthors[index] = {
        ...updatedAuthors[index],
        name: inputValue,
      };
      setAuthors(updatedAuthors);
    }
  }}
  value={author.name}
  isInvalid={!!errors[`author-${author.id}`]}
/>
<Form.Control.Feedback type="invalid">
  {errors[`author-${author.id}`]}
</Form.Control.Feedback>


    </Form.Group>

    <Form.Group
      style={{
        flexGrow: 1,
        marginRight: "10px",
        marginBottom: 0,
        width: "50px",
        marginLeft: "10px",
      }}
    >
      <Form.Label>{`Contact Email ${author.id}`}</Form.Label>
      <Form.Control
  type="text"
  placeholder={`Enter Contact Email ${author.id}`}
  onChange={(e) => {
    const updatedAuthors = [...authors];
    updatedAuthors[index] = {
      ...updatedAuthors[index],
      email: e.target.value,
    };
    setAuthors(updatedAuthors);
  }}
  value={author.email}
  isInvalid={!!errors[`contactemail-${author.id}`]}
/>
<Form.Control.Feedback type="invalid">
  {errors[`contactemail-${author.id}`]}
</Form.Control.Feedback>

    </Form.Group>
    {index === 0 ? (
                        <Button
                          className="mt-4"
                          variant="primary"
                          size="sm"
                          style={{
                            borderRadius: "50%",
                            width: "30px",
                            height: "30px",
                          }}
                          onClick={addAuthor}
                        >
                          <FaPlus />
                        </Button>
                      ) : (
                        <Button
                          className="mt-4"
                          variant="danger"
                          size="sm"
                          style={{
                            borderRadius: "50%",
                            width: "30px",
                            height: "30px",
                          }}
                          onClick={() => removeAuthor(author.id)}
                        >
                          <FaTimes />
                        </Button>
                      )}

    {/* Render comma if it's not the last author */}
    {index !== authors.length - 1 && <span>, </span>}
  </div>
))}











<NavLink exact to="/submittedhome">


                  <Button
                    className="mt-3"
                    variant="primary"
                    onClick={handleSubmit}
                    style={{
                      backgroundColor: "maroon",
                      color: "white",
                      float: "right",
                    }}
                  >
                    Submit
                  </Button>
                  </NavLink>
                  
                </Form>
              )}
            </div>
          </Col>
        </Row>
      </Container>

      <img
        src={footerssu}
        alt="Logo"
        style={{ width: "100%", marginTop: "5%" }}
      />
      <footer
        style={{
          background: "maroon",
          padding: "20px",
          color: "white",
          textAlign: "center",
        }}
      >
        <p>&copy; 2024-All Rights Reserved.</p>
      </footer>
    </>
  );
}

export default SubmitForm;
