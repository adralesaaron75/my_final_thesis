import { CDBBox, CDBContainer } from "cdbreact";
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from "cdbreact";
import React, { useState } from 'react';
import SidebarLayout from "./components_admin/SideBarLayout";
import FloatingButton from "./components_admin/FloatingAdd";
import { Container, Card, Form, Button, Row, Col} from "react-bootstrap";
import { FaPlus, FaTimes  } from 'react-icons/fa';

export default function Damagecharge({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  const [authors, setAuthors] = useState([{ id: 1, name: "Author 1" }]);

  const addAuthor = () => {
    const newId = authors.length + 1;
    setAuthors([...authors, { id: newId, name: `Author ${newId}` }]);
  };

  const removeAuthor = (id) => {
    setAuthors(authors.filter(author => author.id !== id));
  };

  const [showFormControl, setShowFormControl] = useState(false);

  const handleButtonClick = () => {
    setShowFormControl(true);
  };


  
  return (
    <div
      style={{ display: "flex", height: "auto", overflow: "scroll initial"  }}
    >
      <CDBBox display="flex" alignContent="start">
        <CDBSidebar textColor="#fff" backgroundColor="#800000" style={{height:'100%'}}>
          <SidebarLayout/>
        </CDBSidebar>
      </CDBBox>


      <CDBBox
        
        display="flex"
        justifyContent="start"
        style={{
          width: "100%",
          height: "100%",
        }}
      >
       <FloatingButton/>


       <Container style={{ flexGrow: 1, padding: "20px" }}>
      <Card>
        <Card.Header>
          <Card.Body style={{ display: "flex", flexDirection: "column" }}>
            {authors.map((author, index) => (
              <div key={author.id} style={{ display: "flex", marginBottom: "10px" }}>
                <Form.Group style={{ flexGrow: 1, marginRight: "10px", marginBottom: 0 }}>
                  <Form.Label>{author.name}</Form.Label>
                  <Form.Control
                    type="text"
                    name={`author-${author.id}`}
                    placeholder={author.name}
                  />
                </Form.Group>
                {index === 0 ? (
                  <Button className="mt-4" variant="outline-primary" size="sm" style={{ borderRadius: "50%", width: "30px", height: "30px" }} onClick={addAuthor}><FaPlus /></Button>
                ) : (
                  <Button className="mt-4" variant="outline-danger" size="sm" style={{ borderRadius: "50%", width: "30px", height: "30px" }} onClick={() => removeAuthor(author.id)}><FaTimes /></Button>
                )}
              </div>
            ))}


<Form.Group style={{ flexGrow: 1, marginRight: "10px", marginBottom: 0 }}>
                  <Form.Label>Other  <Button  size="sm" style={{ borderRadius: "50%"}}><FaPlus /></Button></Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Other"
                  
                   
                  />
                </Form.Group>

          </Card.Body>
        </Card.Header>
      </Card>
    </Container>

      </CDBBox>

    </div>
  );
}
