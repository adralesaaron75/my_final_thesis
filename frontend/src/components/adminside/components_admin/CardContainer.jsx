import React, { useState, useEffect } from "react";
import { Container, Form, Row, Col, Card, Table, Modal, Button, Nav } from "react-bootstrap";
import { FaEdit, FaTrash, FaPencilAlt} from "react-icons/fa";
import { VITE_BACKEND_URL } from "../../../App";
import EditModal from "../AddEditDel/EditInst";
import axios from "axios";
import { toast } from "react-toastify";

export default function CardLib({ userType }) {
  const [data, setData] = useState([]);
  const [showModal, setShowModal] = useState(false);

  const [selectedTitleName, setSelectedTitleName] = useState('');
  const [selectedAbsract, setSelectedAbsract] = useState('');
  const [selectedId, setSelectedId] = useState('');
  const [selectedCourse, setSelectedCourse] = useState('');
  const [selectedYear, setSelectedYear] = useState('');
  const [selectedCategory, setSelectedCategory] = useState('');


  const [isEditingTitle, setIsEditingTitle] = useState(false); 
  const [isEditingAbstract, setIsEditingAbstract] = useState(false); 
  const [isEditingCourse, setIssEditingCourse] = useState(false); 
  const [isEditingYear, setIsEditingYear] = useState(false); 
  const [isEditingCategory, setIsEditingCategory] = useState(false); 
  const [titleChanged, setTitleChanged] = useState(false);
  const [AbstractChanged, setAbstractChanged] = useState(false);
  const [CourseChanged, setCourseChanged] = useState(false);
  const [YearChanged, setYearChanged] = useState(false);
  const [CategoryChanged, setCategoryChanged] = useState(false);

  const handleTitleEdit = () => {
    setIsEditingTitle(true); // Enable title editing
  };


  const handleAbstractEdit = () => {
    setIsEditingAbstract(true); // Enable title editing
  };

  const handleCourseEdit = () => {
    setIssEditingCourse(true); // Enable title editing
  };
  const handleyearEdit = () => {
    setIsEditingYear(true); // Enable title editing
  };
  const handleCategoryEdit = () => {
    setIsEditingCategory(true); // Enable title editing
  };

  const handleCloseModal = () => {
    setShowModal(false);
    setIsEditingTitle(false); // Enable title editing
    setIsEditingAbstract(false);
    setIssEditingCourse(false);
    setIsEditingYear(false);
    setIsEditingCategory(false);
    setTitleChanged(false);
    setAbstractChanged(false);
    setCategoryChanged(false);
    setCourseChanged(false);
    setYearChanged(false);
  };

  const handleEditClick = (title, abstract, id, course, yearpublished, category) => {
    setSelectedTitleName(title);
    setSelectedAbsract(abstract);
    setSelectedId(id); 
    setSelectedCourse(course);
    setSelectedYear(yearpublished);
    setSelectedCategory(category);
    setShowModal(true);
  };

  useEffect(() => {
    getAllBooks();
  }, []);

  const getAllBooks = async () => {
    await fetch(`${VITE_BACKEND_URL}/api/getAllBooks`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        setData(data.data);
      });
  };

  const handleUpdate = async () => {
    try {
      // Check if both title name and abstract are not empty
      if (selectedTitleName.trim() === '' || selectedAbsract.trim() === '') {
        toast.error('Title and abstract cannot be empty');
        return;
      }
  
      // Prepare the data to be sent to the backend
      const formData = {
        titlename: selectedTitleName,
        abstractname: selectedAbsract,
        course: selectedCourse,
        yearpublished: selectedYear,
        category: selectedCategory,
       
      };
  
      // Send a PUT request to update the submission
      const response = await axios.put(`${VITE_BACKEND_URL}/api/updatebook/${selectedId}`, formData);
      
      // Check if the update was successful
      if (response.data.status === 'ok') {
        toast.success('Submission updated successfully');
        // Optionally, you can refresh the data after update
        getAllBooks();
        // Close the modal after successful update
        handleCloseModal();
      } else {
        toast.error('Failed to update submission');
      }
    } catch (error) {
      console.error('Error updating submission:', error);
      toast.error('Failed to update submission');
    }
  };


  const handleDelete = async (id) => {
    try {
      // Send a DELETE request to delete the book
      await axios.delete(`${VITE_BACKEND_URL}/api/delete-book/${id}`);
      
      // Update the UI by fetching all books again
      getAllBooks();
  
      // Optionally, show a success message
      toast.success('Deleted successfully');
    } catch (error) {
      console.error('Error deleting book:', error);
      toast.error('Failed to delete book');
    }
  };
  

 

  const limitText = (text, limit) => {
    return text.length > limit ? text.substring(0, limit) + "..." : text;
  };

  const handleTitleChange = (e) => {
    setSelectedTitleName(e.target.value);
    // Check if the new value is different from the original value
    setTitleChanged(e.target.value !== selectedTitleName);
  };

  const handleAbstractChange = (e) => {
    setSelectedAbsract(e.target.value);
    // Check if the new value is different from the original value
    setAbstractChanged(e.target.value !== selectedAbsract);
  };

  const handleCourseChange = (e) => {
    setSelectedCourse(e.target.value);
    // Check if the new value is different from the original value
    setCourseChanged(e.target.value !== selectedCourse);
  };
  const handleyearChange = (e) => {
    setSelectedYear(e.target.value);
    // Check if the new value is different from the original value
    setYearChanged(e.target.value !== selectedYear);
  };
  const handleCategoryChange = (e) => {
    setSelectedCategory(e.target.value);
    // Check if the new value is different from the original value
    setCategoryChanged(e.target.value !== selectedCategory);
  };






  
  const colorsData = [
    { label: 'Desktop Games', value: 'Desktop Games' },
    { label: 'Desktop Applications', value: 'Desktop Applications' },
    { label: 'Web Games', value: 'Web Games' },
    { label: 'Web Applications', value: 'Web Applications' },
    { label: 'Mobile Games', value: 'Mobile Games' },
    { label: 'Mobile Applications', value: 'Mobile Applications' },
    { label: 'Enterprise and E-commerce', value: 'Enterprise and E-commerce' },
    { label: 'Organization Applications', value: 'Organizational Applications' },
    { label: 'Other', value: 'Other' },
  ];

  const [selectedColors, setSelectedColors] = useState([]);

  const toggleColor = (color) => {
    setSelectedColors((prevSelected) => {
      if (prevSelected.includes(color)) {
        return prevSelected.filter((c) => c !== color);
      } else {
        return [...prevSelected, color];
      }
    });
    onChange(color);
  };

  const removeColor = (color) => {
    setSelectedColors((prevSelected) => prevSelected.filter((c) => c !== color));
  };








  return (
    <>
      {data.map((item, index) => (
        <Container
          key={index}
          style={{ marginBottom: "20px" }}
          className="mt-3 mb-4"
        >
          <Card>
            <Card.Header
              style={{
                position: "relative",
                backgroundColor: "maroon",
                borderBottom: "4px solid #dee2e6",
              }}
            >
              <h6 style={{ marginBottom: "0", marginLeft: "10px", width: "60vw", color:'white' }}>
                {limitText(item.titlename, 80)}
              </h6>
              <div style={{ position: "absolute", top: "50%", transform: "translateY(-50%)", right: "10px", zIndex: 1 }}>
                <Button variant="primary" size="sm" style={{ marginRight: "5px", fontSize: "0.7rem" }} onClick={() => handleEditClick(item.titlename, item.abstractname, item._id, item.course,item.yearpublished,item.category)}>
                  <FaEdit style={{ fontSize: "0.7rem" }} />
                </Button>
                <Button 
  variant="danger" 
  size="sm" 
  className="btn-sm" 
  style={{ fontSize: "0.7rem", marginLeft: "5px" }}
  onClick={() => handleDelete(item._id)} // Pass the book ID to the handler
>
  <FaTrash style={{ fontSize: "0.7rem" }} />
</Button>

                <Button variant="info" size="sm" className="btn-sm" style={{ fontSize: "0.7rem", marginLeft: "5px" }}>
                  Show
                </Button>
              </div>
            </Card.Header>

            <Card.Body>
            <Container>
                <Row>
                  <Col>
                    <p>
                      <span
                        style={{
                          color: "black",
                          fontSize: "14px",
                          textAlign: "center",
                        }}
                      >
                        {item.link}
                      </span>
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col xs={3} md={2}>
                    <p
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Year:
                    </p>
                  </Col>
                  <Col>
                    <p>
                      <span style={{ color: "black", fontSize: "14px" }}>
                        {item.yearpublished}
                      </span>
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col xs={3} md={2}>
                    <p
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Category:
                    </p>
                  </Col>
                  <Col>
                    <p>
                      <span style={{ color: "black", fontSize: "14px" }}>
                        {item.category}
                      </span>
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col xs={3} md={2}>
                    <p
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Author:
                    </p>
                  </Col>
                  <Col>
  <ul className="list-unstyled">
    {item.authorjoin && item.authorjoin.trim() !== "" && item.authorjoin.split('|').map((authorjoin, index) => (
      <li key={index}>
        {authorjoin.trim()} {/* Trim to remove any leading/trailing whitespace */}
      </li>
    ))}
  </ul>
</Col>

                </Row>
                <Row>
                  <Col xs={3} md={2}>
                    <p
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Abstract:
                    </p>
                  </Col>
                  <Col>
                    <p>
                      <span style={{ color: "black", fontSize: "14px" }}>
                        {limitText(item.abstractname, 475)}
                      </span>
                    </p>
                  </Col>
                </Row>
              </Container>
            </Card.Body>
          </Card>
        </Container>
      ))}

<Modal show={showModal} size="lg" onHide={handleCloseModal}>
  <Modal.Header closeButton>
    <Modal.Title>Edit Title & Abstract</Modal.Title>
  </Modal.Header>
  <Modal.Body>
    <Form className="mt-2">
      <Form.Group controlId="formTitle" style={{marginBottom:'15px'}}>
      <div style={{ position: 'relative' }}>
        <Form.Label>Title</Form.Label>
        <FaPencilAlt
            style={{ position: 'absolute', left: '45px', top: '35%', transform: 'translateY(-50%)', cursor: 'pointer', color: 'blue' }}
            onClick={handleTitleEdit} // Enable title editing when clicked
          />
        </div>
       
        <Form.Control
  type="text"
  name="title"
  placeholder="Enter Title"
  value={selectedTitleName}
  onChange={(e) => {
    handleTitleChange(e); // Tawagin ang handleTitleChange kapag may pagbabago
  }}
  disabled={!isEditingTitle}
  style={{ border: '1px solid #ced4da', borderRadius: '0.25rem', padding: '0.375rem 0.75rem' }}
/>

        
    
      </Form.Group>
      <Form.Group controlId="formAbstract" style={{marginBottom:'15px'}}>
      <div style={{ position: 'relative' }}>
        <Form.Label>Abstract</Form.Label>
        <FaPencilAlt
            style={{ position: 'absolute', left: '80px', top: '35%', transform: 'translateY(-50%)', cursor: 'pointer', color: 'blue' }}
            onClick={handleAbstractEdit} // Enable title editing when clicked
          />
        </div>
        {isEditingAbstract ? (
       <Form.Control
       as="textarea"
       rows={10}
       value={selectedAbsract}
       onChange={(e) => {
         handleAbstractChange(e); // Tawagin ang handleAbstractChange kapag may pagbabago
       }}
       disabled={!isEditingAbstract}
       style={{ border: '1px solid #ced4da', borderRadius: '0.25rem', padding: '0.375rem 0.75rem' }}
     />
  ) :   <Form.Control

  value={isEditingAbstract ? selectedAbsract : limitText(selectedAbsract, 80)}
  onChange={(e) => setSelectedAbsract(e.target.value)}
  disabled={!isEditingAbstract} // Disable the title field when not editing
  style={{ border: '1px solid #ced4da', borderRadius: '0.25rem', padding: '0.375rem 0.75rem' }}
/> }
      </Form.Group>

      <Form.Group controlId="formCourse" style={{marginBottom:'15px'}}>
      <div style={{ position: 'relative' }}>
        <Form.Label>Course</Form.Label>
        <FaPencilAlt
            style={{ position: 'absolute', left: '70px', top: '35%', transform: 'translateY(-50%)', cursor: 'pointer', color: 'blue' }}
            onClick={handleCourseEdit} // Enable title editing when clicked
          />
        </div>

<Form.Control
                          as="select" // Change input type to select for dropdown
                          className="form-select"
                          value={selectedCourse}
                          onChange={(e) => {
                            handleCourseChange(e); // Tawagin ang handleTitleChange kapag may pagbabago
                          }}
                       
                          disabled={!isEditingCourse}
                          style={{ border: '1px solid #ced4da', borderRadius: '0.25rem', padding: '0.375rem 0.75rem' }}
                        >
                             <option value="">Select a Course</option>
                          <option value="BSCS">BSCS</option>
                          <option value="BSIT">
                          BSIT
                          </option>
                          <option value="BSIS">
                          BSIS
                          </option>
                        </Form.Control>


        
    
      </Form.Group>

      <Form.Group controlId="formYear" style={{marginBottom:'15px'}}>
      <div style={{ position: 'relative' }}>
        <Form.Label>Year</Form.Label>
        <FaPencilAlt
            style={{ position: 'absolute', left: '50px', top: '35%', transform: 'translateY(-50%)', cursor: 'pointer', color: 'blue' }}
            onClick={handleyearEdit} // Enable title editing when clicked
          />
        </div>
       
      

<Form.Control
                  type="number"
                  placeholder="yyyy"
                  value={selectedYear}
                  onChange={(e) => {
                    handleyearChange(e); // Tawagin ang handleTitleChange kapag may pagbabago
                  }}
                  disabled={!isEditingYear}
                  style={{ border: '1px solid #ced4da', borderRadius: '0.25rem', padding: '0.375rem 0.75rem' }}
                
                
                />

        
    
      </Form.Group>

      <Form.Group controlId="formCategory">
      <div style={{ position: 'relative' }}>
        <Form.Label>Category</Form.Label>
        <FaPencilAlt
            style={{ position: 'absolute', left: '85px', top: '35%', transform: 'translateY(-50%)', cursor: 'pointer', color: 'blue' }}
            onClick={handleCategoryEdit} // Enable title editing when clicked
          />
        </div>
       
        {!isEditingCategory ? (
    <Form.Control
    type="text"
    name="title"
    placeholder="Enter Title"
    value={selectedCategory}
    disabled={!isEditingYear}
    onChange={(e) => {
      handleCategoryChange(e); // Tawagin ang handleTitleChange kapag may pagbabago
    }}
    style={{ border: '1px solid #ced4da', borderRadius: '0.25rem', padding: '0.375rem 0.75rem' }}
  />
  ) : (
    <div>
      <select
        className={`form-select`}
        // value={''}
        // onChange={(e) => toggleColor(e.target.value)}
        // style={{ fontFamily: "Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)' }}
        value={selectedCategory}
        onChange={(e) => {
          handleCategoryChange(e); // Tawagin ang handleTitleChange kapag may pagbabago
        }}
        style={{ border: '1px solid #ced4da', borderRadius: '0.25rem', padding: '0.375rem 0.75rem' }}
      >
        <option value="">Select a Category</option>
        {colorsData.map((color) => (
          <option
            key={color.value}
            value={color.value}
            className={selectedColors.includes(color.value) ? 'selected' : ''}
          >
            {color.label}
          </option>
        ))}
      </select>

      {selectedColors.length > 0 && (
        <div className="card p-2">
          <div className="selected-colors mb-1">
            {selectedColors.map((colorValue) => {
              const selectedColor = colorsData.find(item => item.value === colorValue);
              return (
                <div
                  key={colorValue}
                  className="selected-color d-inline-block me-2 mb-2 px-2 py-1 rounded"
                  style={{ color: 'black', fontSize: '0.7rem', fontWeight: 'bold' }}
                >
                  {selectedColor.label}
                  <button
                    type="button"
                    className="btn-close ms-2"
                    aria-label="Close"
                    onClick={() => removeColor(colorValue)}
                  ></button>
                </div>
              );
            })}
          </div>
          {/* <Form.Control.Feedback type="invalid">{errors.category}</Form.Control.Feedback> */}
          {selectedColors.length < 0 && (
            <Form.Group controlId="selectedItems" className="mb-3">
              <Form.Control type="text" value={selectedColors.join(' | ')} readOnly />
            </Form.Group>
          )}
        </div>
      )}
    </div>
  )}

        
    
      </Form.Group>




      
      



    </Form>
    <div className="text-end mt-4">
    <Button
  variant="primary"
  onClick={handleUpdate}
  disabled={!titleChanged && !AbstractChanged && ! CourseChanged  && ! YearChanged && ! CategoryChanged} // Disable if title or abstract not changed
>
  Update
</Button>
    </div>
  </Modal.Body>
</Modal>

    </>
  );
}
