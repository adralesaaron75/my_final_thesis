import React from "react";
import { Card, Row, Col } from "react-bootstrap";

const BoxDash = () => {
  return (
    <Row>
      <Col sm={4}>
        <Card style={{ backgroundColor: "#8E7AB5", height: '25vh' }}>
          <Card.Body>
            <Card.Title
              className="text-center mb-4"
              style={{
                color: "white",
                fontFamily: "cursive",
                fontSize: "25px",
              }}
            >
              Recent Activity
            </Card.Title>
            <Row>
              <Col>
                <ul style={{ color: "white" }}>
                  <li className="list-group-item d-flex justify-content-between align-items-center">
                    New Registered Users
                    <span
                      className="badge badge-primary badge-pill"
                      style={{ backgroundColor: "#ff6347" }}
                    >
                      14
                    </span>
                  </li>
                  <li className="list-group-item d-flex justify-content-between align-items-center">
                    Approved Submissions
                    <span
                      className="badge badge-primary badge-pill"
                      style={{ backgroundColor: "#ff6347" }}
                    >
                      4
                    </span>
                  </li>
                  <li className="list-group-item d-flex justify-content-between align-items-center">
                    Rejected Submissions
                    <span
                      className="badge badge-primary badge-pill"
                      style={{ backgroundColor: "#ff6347" }}
                    >
                      1
                    </span>
                  </li>
                </ul>
              </Col>
            </Row>
          </Card.Body>
        </Card>
      </Col>
      <Col sm={4}>
        <Card style={{ backgroundColor: "#D7E4C0", height: '25vh' }}>
          <Card.Body>
            <Card.Title
              className="text-center mb-4"
              style={{
                color: "black",
                fontFamily: "cursive",
                fontSize: "25px",
              }}
            >
              Registered Users
            </Card.Title>
            <Card.Text className="text-center" style={{ fontSize: "50px" }}>
              50
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col sm={4}>
        <Card style={{ backgroundColor: "#FFB996", height: '25vh' }}>
          <Card.Body>
            <Card.Title
              className="text-center mb-4"
              style={{
                color: "white",
                fontFamily: "cursive",
                fontSize: "25px",
              }}
            >
              Active Users
            </Card.Title>
            <Card.Text
              className="text-center"
              style={{ fontSize: "50px", color: "white" }}
            >
              50
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
};

export default BoxDash;
