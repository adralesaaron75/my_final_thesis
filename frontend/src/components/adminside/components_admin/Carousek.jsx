import React, { useState } from 'react';
import { Carousel, Form, Button, Container } from 'react-bootstrap';
import SubmitForm from '../../studentside/components_students/SubmitForm';
import WebScrapedForm from './WebscrapedForm';

function CarouselWithForm() {
  const [email, setEmail] = useState('');

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // Here you can handle form submission logic
    console.log('Form submitted with email:', email);
  };

  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh', background:'black'}}>
      <Container style={{ zIndex: 999}}>
        <Form onSubmit={handleSubmit} style={{ zIndex: 999}}>
          <Carousel>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="holder.js/800x400?text=First slide&bg=f5f5f5"
                alt="First slide"
              />
             
            </Carousel.Item>
            <Carousel.Item>
            <WebScrapedForm/>
             
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="holder.js/800x400?text=Third slide&bg=e5e5e5"
                alt="Third slide"
              />
            </Carousel.Item>
          </Carousel>
        </Form>
      </Container>
    </div>
  );
}

export default CarouselWithForm;
