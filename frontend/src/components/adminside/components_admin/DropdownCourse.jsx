// import React from 'react';

// const ChipDropCourse = ({ value, onChange }) => {
//   const colorsData = [
//     { label: 'BSCS', value: '#F23F82' },
//     { label: 'BSIT Processing', value: '#2F5D81' },
//     { label: 'BSIS', value: '#4CD242' },
//   ];

//   return (
//     <div>
//       <select
//         className="form-select"
//         value={value}
//         onChange={(e) => onChange(e.target.value)}
//       >
//         <option value="">Select a Course</option>
//         {colorsData.map((color) => (
//           <option key={color.value} value={color.value}>
//             {color.label}
//           </option>
//         ))}
//       </select>
//     </div>
//   );
// };

// export default ChipDropCourse;


import React from 'react';
import { Form } from 'react-bootstrap';

const ChipDropCourse = ({ value, onChange }) => {
  const colorsData = [
    { label: 'BSCS', value: '#F23F82' },
    { label: 'BSIT Processing', value: '#2F5D81' },
    { label: 'BSIS', value: '#4CD242' },
  ];

  return (
    <div>
      <Form.Select
        value={value}
        onChange={(e) => onChange(e.target.value)}
      >
        <option value="">Select a Course</option>
      
          <option > 'BSCS'
           
          </option>
       
      </Form.Select>
    </div>
  );
};

export default ChipDropCourse;
