import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { CDBBox, CDBContainer } from "cdbreact";
import { Link } from "react-router-dom";
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from "cdbreact";

import { NavLink } from "react-router-dom";
import React, { useState } from "react";
import { IoIosArrowForward, IoIosArrowDown } from "react-icons/io";

export default function SidebarLayout({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  const [showLibraries, setShowLibraries] = useState(false);
  const [showReports, setShowReports] = useState(false);

  return (
    <>
      <CDBSidebarHeader
        prefix={<i className="fa fa-bars fa-large"></i>}
      ></CDBSidebarHeader>

      <CDBSidebarContent className="sidebar-content">
        <CDBSidebarMenu>
          <NavLink exact to="/admin/dashboard" activeClassName="activeClicked">
            <CDBSidebarMenuItem 
              icon="columns" 
              style={{
                color: location.pathname === '/admin/dashboard' ? 'black' : 'white',
                backgroundColor: location.pathname === '/admin/dashboard' ? 'white' : 'transparent',
                
              }}
            >
              Dashboard
            </CDBSidebarMenuItem>
          </NavLink>

          <>
            <CDBSidebarMenuItem
              icon="book"
              onClick={() =>
                setShowLibraries((showLibraries) => !showLibraries)
              }
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <span>Library</span>
                {showLibraries ? <IoIosArrowDown /> : <IoIosArrowForward />}
              </div>
            </CDBSidebarMenuItem>

            {showLibraries && (
              <div style={{ marginLeft: "50px" }}>
                <NavLink
                  exact
                  to="/admin/institution"
                  activeClassName="activeClicked"
                >
                  <CDBSidebarMenuItem icon="sticky-note">
                    Institutional
                  </CDBSidebarMenuItem>
                </NavLink>
                <NavLink
                  exact
                  to="/admin/webscraped"
                  activeClassName="activeClicked"
                >
                  <CDBSidebarMenuItem icon="sticky-note">
                    Web Scraped
                  </CDBSidebarMenuItem>
                </NavLink>
              </div>
            )}

           

            <NavLink exact to="/admin/admin/res" activeClassName="activeClicked">
            <CDBSidebarMenuItem 
              icon="book" 
              style={{
                color: location.pathname === '/admin/admin/res' ? 'black' : 'white',
                backgroundColor: location.pathname === '/admin/admin/res' ? 'white' : 'transparent'
              }}
            >
              Users
            </CDBSidebarMenuItem>
          </NavLink>


            <NavLink
              exact
              to="/admin/managesubmission/submitted"
              activeClassName="activeClicked"
            >
              <CDBSidebarMenuItem 
              icon="book" 
              style={{
                color: location.pathname === '/admin/managesubmission/submitted' ? 'black' : 'white',
                backgroundColor: location.pathname === '/admin/managesubmission/submitted' ? 'white' : 'transparent'
              }}
            >
              Manage Submission
            </CDBSidebarMenuItem>
            </NavLink>

            <NavLink
              exact
              // to="/admin/reports"
              activeClassName="activeClicked"
            >
              <CDBSidebarMenuItem 
              icon="address-book" 
              style={{
                color: location.pathname === '/admin/reports' ? 'black' : 'white',
                backgroundColor: location.pathname === '/admin/reports' ? 'white' : 'transparent'
              }}
              onClick={() =>
                setShowReports((showReports) => !showReports)
              }
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <span>Reports</span>
                {showReports ? <IoIosArrowDown /> : <IoIosArrowForward />}
              </div>
              
            </CDBSidebarMenuItem>


            {showReports && (
              <div style={{ marginLeft: "50px" }}>
                <NavLink
                  exact
                  to="/admin/reports/submitted"
                  activeClassName="activeClicked"
                >
                  <CDBSidebarMenuItem icon="sticky-note">
                    All Submissions
                  </CDBSidebarMenuItem>
                </NavLink>
                <NavLink
                  exact
                  to="/admin/reports/admin"
                  activeClassName="activeClicked"
                >
                  <CDBSidebarMenuItem icon="sticky-note">
                  All Users
                  </CDBSidebarMenuItem>
                </NavLink>
              </div>
            )}








            </NavLink>

            {/* <NavLink exact to="/admin/catalog" activeClassName="activeClicked">
              <CDBSidebarMenuItem 
              icon="box" 
              style={{
                color: location.pathname === '/admin/catalog' ? 'black' : 'white',
                backgroundColor: location.pathname === '/admin/catalog' ? 'white' : 'transparent'
              }}
            >
              Catalog
            </CDBSidebarMenuItem>
            </NavLink> */}
{/* 
            <NavLink exact to="/admin/admins" activeClassName="activeClicked">
              <CDBSidebarMenuItem icon="box">
                <Link to="/admin/admins">Catalog 4</Link>
              </CDBSidebarMenuItem>
            </NavLink> */}
          </>

          <NavLink
            exact
            to="/"
            target="_blank"
            activeClassName="activeClicked"
          >
            <CDBSidebarMenuItem>
              <Button variant="primary" onClick={logOut} className="mt-5">
                Log out
              </Button>
            </CDBSidebarMenuItem>
          </NavLink>
        </CDBSidebarMenu>
      </CDBSidebarContent>
     

      <CDBSidebarFooter style={{ textAlign: "center", marginBottom:"150px"}}>
        <div
          style={{
            padding: "20px 5px",
           
          }}
        ></div>
      </CDBSidebarFooter>
    </>
  );
}
