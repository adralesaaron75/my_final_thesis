import React, { useState, useEffect, useRef } from 'react';
import { FaBars, FaSignInAlt } from "react-icons/fa";
import { Container, Row, Col, Form, Table } from "react-bootstrap";
import AddModal from '../AddEditDel/AddInst';
import WebAdd from '../AddEditDel/WebAdd';

const FloatingButton = () => {
  const [isClicked, setIsClicked] = useState(false);
  const [isBackgroundDark, setIsBackgroundDark] = useState(false); // New state for background color

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (!isClicked && isBackgroundDark) {
        setIsBackgroundDark(false); // Reset background color when clicked outside the button
        setIsClicked(false); // Close button when clicked outside
      }
    };

    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [isClicked, isBackgroundDark]); // Listen to changes in isClicked and isBackgroundDark states

  const handleClick = () => {
    setIsClicked(!isClicked);
    setIsBackgroundDark(!isBackgroundDark); // Toggle background color
  };

  const handleBackgroundClick = () => {
    setIsBackgroundDark(false); // Close background when clicked
    setIsClicked(false); // Close button when clicked outside
  };

  const [isAddModalOpen, setIsAddModalOpen] = useState(false);

  const handleAddModal = () => {
    console.log("Opening AddModal");
    setIsAddModalOpen(true);
  };

  return (
    <div>
      {isBackgroundDark && ( // Render background if isBackgroundDark is true
        <div
          onClick={handleBackgroundClick}
          style={{
            position: "fixed",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            background: "rgba(0, 0, 0, 0.9)",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            zIndex: 9999,
          }}
        />
      )}
      <div style={{ position: 'fixed', bottom: '20px', right: '20px', filter: 'drop-shadow(9px 10px 5px rgba(0, 0, 0, 0.3))', zIndex: '9999' }}>
        {isClicked && (
          <div style={{
            position: 'absolute',
            bottom: 'calc(100% + 10px)',
            right: '50%',
            transform: 'translateX(50%)',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center'

          }}>


            <AddModal />
            <WebAdd />




          </div>
        )}
        <button
          onClick={handleClick}
          style={{
            backgroundColor: '#007bff',
            color: 'white',
            border: 'none',
            borderRadius: '50%',
            width: '60px',
            height: '60px',
            fontSize: '1.5rem',
            cursor: 'pointer',
            boxShadow: '0px 2px 5px rgba(0, 0, 0, 0.3)',
            outline: 'none',
            transition: 'transform 0.3s ease', // Add transition for smooth animation
          }}
        >
          {isClicked ? '×' : '+'}
        </button>
      </div>
    </div>
  );
};

export default FloatingButton;
