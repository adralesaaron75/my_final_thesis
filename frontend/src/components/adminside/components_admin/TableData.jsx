import React, { useState, useEffect } from "react";
import { Table, Button } from "react-bootstrap";
import { toast } from "react-toastify";
import { VITE_BACKEND_URL } from "../../../App";

export default function TbleData({ userType }) {
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [booksPerPage] = useState(5);

  useEffect(() => {
    getAllBooks();
  }, []);

  const getAllBooks = async () => {
    try {
      const response = await fetch(`${VITE_BACKEND_URL}/api/getAllBooks`);
      if (!response.ok) {
        throw new Error("Failed to fetch books");
      }
      const data = await response.json();
      setData(data.data);
    } catch (error) {
      console.error("Error fetching books:", error);
      toast.error("Failed to fetch books");
    }
  };

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const truncateString = (str, maxLength) => {
    if (typeof str !== 'string') return str; // Ensure it's a string
    if (str.length > maxLength) {
      return str.substring(0, maxLength) + "...";
    }
    return str;
  };

  const truncateTitle = (str, maxLength) => {
    if (typeof str !== 'string') return str; // Ensure it's a string
    if (str.length > maxLength) {
      return str.substring(0, maxLength) + "...";
    }
    return str;
  };

  return (
    <div style={{ display: "flex", height: "100vh", overflow: "scroll initial" }}>
      <div style={{ marginLeft: "20px" }}>
        <h2>Library 1</h2>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Title Name</th>
              <th>Abstract</th>
              {/* <th>Book Number/Code</th> */}
              <th>Course</th>
              <th>Category</th>
              <th>Published Date</th>
              <th>yearpublished</th>
            </tr>
          </thead>
          <tbody>
            {data
              .slice((currentPage - 1) * booksPerPage, currentPage * booksPerPage)
              .map((book, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td style={{width:'15vw', height:'10vh'}}>{truncateTitle(book.titlename, 20)}</td>
                  <td style={{width:'20vw'}}>{truncateString(book.abstractname, 30)}</td>
                  {/* <td>{book.contactemail}</td> */}
                  <td>{book.authorjoin}</td>
                  <td>{book.category}</td>
                  <td>{book.publisheddate}</td>
                  <td>{book.yearpublished}</td>
                </tr>
              ))}
          </tbody>
        </Table>
        <div className="pagination">
          <Button
            variant="outline-secondary"
            onClick={() => handlePageChange(currentPage - 1)}
            disabled={currentPage === 1}
          >
            Previous
          </Button>
          <span style={{ margin: "0 10px" }}>Page {currentPage}</span>
          <Button
            variant="outline-secondary"
            onClick={() => handlePageChange(currentPage + 1)}
            disabled={currentPage * booksPerPage >= data.length}
          >
            Next
          </Button>
        </div>
      </div>
    </div>
  );
}
