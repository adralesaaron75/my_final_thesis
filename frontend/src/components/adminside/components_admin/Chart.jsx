// ChartComponent.jsx

import React, { useEffect, useRef } from 'react';
import Chart from 'chart.js/auto'

const ChartComponent = () => {
    const chartRef = useRef(null);

    useEffect(() => {
        const ctx = chartRef.current.getContext('2d');
        const myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Machine Learning', 'Artificila Intelligence', 'Data Science', 'Mobile App', 'Cybersecurity'],
                datasets: [{
                    label: 'Submissions',
                    data: [12, 19, 3, 5, 2],
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        return () => {
            
            myChart.destroy();
        };
    }, []);

    return <canvas ref={chartRef} />;
};

export default ChartComponent;
