import React, { useState } from 'react';
import { Form } from 'react-bootstrap';

const ChipCust = ({ value, onChange }) => {
  const colorsData = [
    { label: 'Web Scraping', value: 'Web Scraping' },
    { label: 'Computational Linguistics', value: 'Computational Linguistics' },
    { label: 'Natural Language Processing', value: 'Natural Language Processing' },
    // { label: 'Network Security', value: 'Network Security' },
    // { label: 'Cybersecurity ', value: 'Cybersecurity' },
    // { label: 'Computer Vision', value: 'Computer Vision' },
    // { label: 'Image Processing', value: 'Image Processing' },
    // { label: 'AI', value: 'AI' },
    // { label: 'Machine Learning', value: 'Machine Learning' },
    // { label: 'Robotics', value: 'Robotics' }
  ];

  const [selectedColors, setSelectedColors] = useState([]);

  const toggleColor = (color) => {
    setSelectedColors((prevSelected) => {
      if (prevSelected.includes(color)) {
        return prevSelected.filter((c) => c !== color);
      } else {
        return [...prevSelected, color];
      }
    });
    onChange(color);
  };

  const removeColor = (color) => {
    setSelectedColors((prevSelected) => prevSelected.filter((c) => c !== color));
  };



  return (
    <div>
      <select
        className="form-select"
        value={''}
        onChange={(e) => toggleColor(e.target.value)}
      >
        <option value="">Select a Category</option>
        {colorsData.map((color) => (
          <option
            key={color.value}
            value={color.value}
            className={selectedColors.includes(color.value) ? 'selected' : ''}
          >
            {color.label}
          </option>
        ))}
      </select>

      {selectedColors.length > 0 && (
        <div className="card p-2">
          <div className="selected-colors mb-1">
            {selectedColors.map((colorValue) => {
              const selectedColor = colorsData.find(item => item.value === colorValue);
              return (
                <div
                  key={colorValue}
                  className="selected-color d-inline-block me-2 mb-2 px-2 py-1 rounded"
                  style={{  color: 'black', fontSize: '0.7rem', fontWeight: 'bold' }}
                >
                  {selectedColor.label}
                  <button
                    type="button"
                    className="btn-close ms-2"
                    aria-label="Close"
                    onClick={() => removeColor(colorValue)}
                  ></button>
                </div>
              );
            })}
          </div>
          <Form.Group controlId="selectedItems">
            <Form.Control type="text" value={selectedColors.join(' | ')} readOnly />
          </Form.Group>
        </div>
      )}
    </div>
  );
};

export default ChipCust;
