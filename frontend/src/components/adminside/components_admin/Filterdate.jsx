import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { CDBBox, CDBContainer } from "cdbreact";
import { Link } from "react-router-dom";
import {
  CDBSidebar
} from "cdbreact";
import { NavLink } from "react-router-dom";
import { Container, Form, Row, Col } from "react-bootstrap";
import React, { useState, useEffect} from 'react';
import { FaPlus, FaTimes, FaArrowLeft, FaArrowRight } from "react-icons/fa";

export default function Filterdate({ setDateRange }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };
  const [fromDate, setFromDate] = useState('');
  const [toDate, setToDate] = useState('');
  const [selectedPreset, setSelectedPreset] = useState('');
  const [presetSelected, setPresetSelected] = useState(false); 
  const [dateRangeValue, setDateRangeValue] = useState('');

  useEffect(() => {
   
    if (fromDate === "" || toDate === "") {
      setDateRange(null);
    } else {
      setDateRange(`${fromDate} to ${toDate}`);
    }
  }, [fromDate, toDate, setDateRange]);
  
  
  


  const handleFromDateChange = (event) => {
    const selectedFromDate = event.target.value;
    setFromDate(selectedFromDate);
  
    // Reset the selected preset
    setSelectedPreset('');
    setPresetSelected(false);
  
    // Check if the selected "From Date" is greater than the "To Date"
    if (new Date(selectedFromDate) > new Date(toDate)) {
      // Swap the values of "From Date" and "To Date"
      setToDate(selectedFromDate);
      setFromDate(toDate);
    }
  };
  
  
const handleToDateChange = (event) => {
  const selectedToDate = event.target.value;
  setToDate(selectedToDate);

  // Reset the selected preset
  setSelectedPreset('');
  setPresetSelected(false);

  // Check if the selected "To Date" is less than the "From Date"
  if (new Date(selectedToDate) < new Date(fromDate)) {
    // Swap the values of "From Date" and "To Date"
    setFromDate(selectedToDate);
    setToDate(fromDate);
  }
};
  

  const handlePresetChange = (preset) => {
    setSelectedPreset(preset);
    setPresetSelected(true); 

    const today = new Date();
    let tempFromDate = new Date();
    let tempToDate = new Date();

    switch (preset) {
      case 'today':
  tempFromDate = new Date();
  tempToDate = new Date();
  break;
      case 'yesterday':
        tempFromDate.setDate(today.getDate() - 1);
        tempToDate.setDate(today.getDate() - 1);
        break;
      case 'last7days':
        tempFromDate.setDate(today.getDate() - 7);
        break;
      case 'lastWeek':
        const lastMonday = new Date(today.setDate(today.getDate() - today.getDay() - 14));
        const lastSunday = new Date(today.setDate(today.getDate() - today.getDay() + 7));
        tempFromDate = lastMonday;
        tempToDate = lastSunday;
        break;
      case 'last2weeks':
        tempFromDate.setDate(today.getDate() - 14);
        break;
      case 'thisMonth':
        tempFromDate.setDate(1);
        break;
      case 'lastMonth':
        tempFromDate.setMonth(today.getMonth() - 1);
        tempFromDate.setDate(1);
        tempToDate.setDate(0);
        break;
      default:
        break;
    }

    setFromDate(tempFromDate.toISOString().split('T')[0]);
    setToDate(tempToDate.toISOString().split('T')[0]);
  };
  const handlePrevButtonClick = () => {
    if (!presetSelected || fromDate === '' || toDate === '') {
      // Handle if no preset is selected or fromDate/toDate is empty
      return;
    }
  
    const today = new Date();
    let tempFromDate = new Date(fromDate);
    let tempToDate = new Date(toDate);
  
    switch (selectedPreset) {
      case 'today':
        tempFromDate = new Date();
        tempToDate = new Date();
        break;
      case 'yesterday':
        tempFromDate.setDate(tempFromDate.getDate() - 1);
        tempToDate.setDate(tempToDate.getDate() - 1);
        break;
      case 'last7days':
        tempFromDate.setDate(tempFromDate.getDate() - 7);
        tempToDate.setDate(tempToDate.getDate() - 7);
        break;
      case 'lastWeek':
        tempFromDate.setDate(tempFromDate.getDate() - 14);
        tempToDate.setDate(tempToDate.getDate() - 14);
        break;
      case 'last2weeks':
        tempFromDate.setDate(tempFromDate.getDate() - 14);
        tempToDate.setDate(tempToDate.getDate() - 14);
        break;
        case 'thisMonth':
          tempFromDate.setDate(0); // Set to the last day of the previous month
          tempFromDate.setDate(1); // Set to the first day of the current month
          tempToDate.setDate(0); // Set to the last day of the previous month
          break;
      case 'lastMonth':
        tempFromDate.setMonth(tempFromDate.getMonth() - 1);
        tempFromDate.setDate(1);
        tempToDate.setDate(0);
        break;
      default:
        break;
    }
  
    setFromDate(tempFromDate.toISOString().split('T')[0]);
    setToDate(tempToDate.toISOString().split('T')[0]);
  };

  const handleNextButtonClick = () => {
    if (!presetSelected || fromDate === '' || toDate === '') {
      // Handle if no preset is selected or fromDate/toDate is empty
      return;
    }
  
    const today = new Date();
    let tempFromDate = new Date(fromDate);
    let tempToDate = new Date(toDate);
  
    switch (selectedPreset) {
      case 'yesterday':
        tempFromDate.setDate(tempFromDate.getDate() + 1);
        tempToDate.setDate(tempToDate.getDate() + 1);
        break;
      case 'last7days':
        tempFromDate.setDate(tempFromDate.getDate() + 7);
        tempToDate.setDate(tempToDate.getDate() + 7);
        break;
      case 'lastWeek':
        tempFromDate.setDate(tempFromDate.getDate() + 14);
        tempToDate.setDate(tempToDate.getDate() + 14);
        break;
      case 'last2weeks':
        tempFromDate.setDate(tempFromDate.getDate() + 14);
        tempToDate.setDate(tempToDate.getDate() + 14);
        break;
        case 'thisMonth':
          tempFromDate.setMonth(tempFromDate.getMonth() + 1);
          tempFromDate.setDate(1); // Set to the first day of the next month
          tempToDate = new Date(tempFromDate);
          tempToDate.setMonth(tempToDate.getMonth() + 1);
          tempToDate.setDate(0); // Set to the last day of the next month
          break;
      case 'lastMonth':
        tempFromDate.setMonth(tempFromDate.getMonth() + 2);
        tempFromDate.setDate(1);
        tempToDate.setDate(0);
        break;
      default:
        break;
    }
  
    setFromDate(tempFromDate.toISOString().split('T')[0]);
    setToDate(tempToDate.toISOString().split('T')[0]);
  };
  

   `${fromDate} / ${toDate}`;

  return (
    
       
   <Form >
  

    
    
    <Row className="justify-content-end mb-3">
      <Col md={2}>
        <Button size="m" style={{ float: 'right', width: '50px' }} onClick={handleNextButtonClick} disabled={!presetSelected || fromDate === '' || toDate === ''}>
          <FaArrowRight />
        </Button>
        <Button size="m" style={{ marginRight: '5px', float: 'right', width: '50px' }} onClick={handlePrevButtonClick}  disabled={!presetSelected || fromDate === '' || toDate === ''}>
          <FaArrowLeft />
        </Button>
      </Col>
      <Col md={2}>
        <Form.Control
          type="date"
          value={fromDate}
          onChange={handleFromDateChange}
          placeholder="From Date"
        />
      </Col>
      
      <Col md={2}>
        <Form.Control
          type="date"
          value={toDate}
          onChange={handleToDateChange}
          placeholder="To Date"
        />
      </Col>
      <Col md={2}>
        <Form.Control
          as="select"
          value={selectedPreset} // Set the value to the selectedPreset state
          onChange={(e) => handlePresetChange(e.target.value)}
          className="form-select"
        >
          <option value="" disabled selected>Choose one</option>
          <option value="today">Today</option>
          <option value="yesterday">Yesterday</option>
          <option value="last7days">Last 7 days</option>
          <option value="lastWeek">Last week</option>
          <option value="last2weeks">Last 2 weeks</option>
          <option value="thisMonth">This month</option>
          <option value="lastMonth">Last month</option>
        </Form.Control>
      </Col>
    </Row>
  </Form>
 
 
  
        
     
  );
}
