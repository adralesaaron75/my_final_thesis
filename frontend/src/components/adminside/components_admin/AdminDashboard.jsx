
import React from "react";
import ChartComponent from "./Chart";
import { Link } from "react-router-dom";
import BoxDash from "./BoxDashboards";
import { Container, Row, Col, Card, CardBody } from "react-bootstrap";
import Widget1 from "../admin_widget/widget1";
import Widget2 from "../admin_widget/widget2";
import Widget3 from "../admin_widget/widget3";
import Widget4 from "../admin_widget/widget4";
import Widget5 from "../admin_widget/widget5";
import Widget6 from "../admin_widget/widget6";
import TodayDash from "../admin_widget/TodayWidget/TodayDash";
import WeeklyDash from "../admin_widget/WeeklyWidget/WeeklyDash";
import Widget7 from "../admin_widget/widget7";
import Widget8 from "../admin_widget/widget8";
import Widget9 from "../admin_widget/widget9";
import Widget10 from "../admin_widget/widget10";


const AdminDashboard = () => {
  return (
    <div className="dashboard p-1" style={{ width: "100%" }}>
      {/* <div className="sidebar">
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link active" href="#">
              Category
            </a>
          </li>
          <li class="nav-item">
            <Link className="nav-link" to="/admin/approved">
              Approved
            </Link>
          </li>
          <li class="nav-item">
            <Link className="nav-link" to="/admin/reject">
              Rejected
            </Link>
          </li>
        
        </ul>
      </div> */}
      <div className="main-content p-4">
        <div
          className="charts-container"
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <div
            className="chart-container"
            style={{ width: '100%', height: "100%"}}
          >

           {/* <BoxDash/> */}

           <TodayDash/>
           <WeeklyDash/>

            {/* <ChartComponent /> */}

           <Container className="mt-5">
            <Row>
              <Col>
              <Widget2/>
              <Widget8/>
              <Widget1/>
              
             
              <Widget6/>
              </Col>
              <Col>
              <Widget7/>
              <Widget9/>
              <Widget10/>
              <Widget3/>
              
              {/* <Widget5/> */}
             
              </Col>

            </Row>
           </Container>


          </div>
        </div>
      </div>
    </div>
  );
};

export default AdminDashboard;
