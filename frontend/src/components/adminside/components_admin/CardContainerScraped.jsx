import React, { useState, useEffect } from "react";
import { Card, Container, Button, Col, Row } from "react-bootstrap";
import { FaEdit, FaTrash } from "react-icons/fa";
import { VITE_BACKEND_URL } from "../../../App";
import EditModal from "../AddEditDel/EditInst";

export default function CardLibScraped({ userType }) {
  const [data, setData] = useState([]);

  useEffect(() => {
    getAllScraped();
  }, []);

  // Fetching all books
  const getAllScraped = async () => {
    await fetch(`${VITE_BACKEND_URL}/api/getScraped`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data, "Scraped Data");
        setData(data.data);
      });
  };

  // Function to limit text to a certain number of characters
  const limitText = (text, limit) => {
    return text.length > limit ? text.substring(0, limit) + "..." : text;
  };

  return (
    <>
      {data.map((item, index) => (
        <Container
          key={index}
          style={{ marginBottom: "20px" }}
          className="mt-3 mb-4"
        >
          <Card>
            <Card.Header
              style={{
                position: "relative",
                backgroundColor: "maroon",
                borderBottom: "4px solid #dee2e6",
                // filter: "drop-shadow(9 9px 9px rgba(9, 9, 9, 0.8))"
              }}
            >
              <h6
                style={{ marginBottom: "0", marginLeft: "10px", width: "60vw", color:'white' }}
              >
                {limitText(item.ScrapedTitle, 80)}
              </h6>
              <div
                style={{
                  position: "absolute",
                  top: "50%",
                  transform: "translateY(-50%)",
                  right: "10px",
                  zIndex: 1,
                }}
              >
                <EditModal />
                <Button
                  variant="danger"
                  size="sm"
                  className="btn-sm"
                  style={{ fontSize: "0.7rem", marginLeft: "5px" }}
                >
                  <FaTrash style={{ fontSize: "0.7rem" }} />
                </Button>
                <Button
                  variant="info"
                  size="sm"
                  className="btn-sm"
                  style={{ fontSize: "0.7rem", marginLeft: "5px" }}
                >
                  Show
                </Button>
              </div>
            </Card.Header>

            <Card.Body>
              <Container>
                <Row>
                  <Col xs={3} md={2}>
                    <p
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Link:
                    </p>
                  </Col>
                  <Col>
                    <p>
                      <span
                        style={{
                          color: "black",
                          fontSize: "14px",
                          textAlign: "center",
                        }}
                      >
                        {item.ScrapedLink}
                      </span>
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col xs={3} md={2}>
                    <p
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Year:
                    </p>
                  </Col>
                  <Col>
                    <p>
                      <span style={{ color: "black", fontSize: "14px" }}>
                        {item.ScrapedAuthor}
                      </span>
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col xs={3} md={2}>
                    <p
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Category:
                    </p>
                  </Col>
                
   
                  <Col>
                    <p>
                      <span style={{ color: "black", fontSize: "14px" }}>
                        {item.ScrapedAbstract}
                      </span>
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col xs={3} md={2}>
                    <p
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Author:
                    </p>
                  </Col>
                
                  <Col>
  <ul className="list-unstyled">
    {item.ScrapedAuthor && item.ScrapedAuthor.trim() !== "" && item.ScrapedAuthor.split('|').map((ScrapedAuthor, index) => (
      <li key={index}>
        {ScrapedAuthor.trim()} 
      </li>
    ))}
  </ul>
</Col>

                  
                  
                </Row>
                <Row>
                  <Col xs={3} md={2}>
                    <p
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Abstract:
                    </p>
                  </Col>
                  <Col>
                    <p>
                      <span style={{ color: "black", fontSize: "14px" }}>
                        {limitText(item.ScrapedAuthor, 475)}
                      </span>
                    </p>
                  </Col>
                </Row>
              </Container>
            </Card.Body>
          </Card>
        </Container>
      ))}
    </>
  );
}
