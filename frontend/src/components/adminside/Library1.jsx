import Button from "react-bootstrap/Button";
import logo from "../assets/images/2072841.png";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { toast } from "react-toastify";
import { CDBBtn, CDBBox, CDBCard, CDBCardBody, CDBContainer } from "cdbreact";
import { Link, useParams } from "react-router-dom";
import Table from "react-bootstrap/Table";
import {
  CDBSidebar,
} from "cdbreact";
import { NavLink } from "react-router-dom";
import axios from "axios";
import { VITE_BACKEND_URL } from "../../App";
import SidebarLayout from "./components_admin/SideBarLayout";


import FloatingButton from "./components_admin/FloatingAdd";
import CardLib from "./components_admin/CardContainer";

import SearchForm from "../studentside/components_students/SearchForm";
import { Card, Container, Form, InputGroup  } from "react-bootstrap";
import { FaFilter, FaSearch } from 'react-icons/fa';
import { GrSort } from "react-icons/gr";




export default function Library({ userType }) {
  const [data, setData] = useState([]);
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  const [showFilter, setShowFilter] = useState(false); // State variable to manage filter visibility

 

  // Fetch all books
  useEffect(() => {
    getAllBooks();
  }, []);

  // Fetching all books
  const getAllBooks = async () => {
    await fetch(`${VITE_BACKEND_URL}/api/getAllBooks`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data, "bookData");
        setData(data.data);
      });
  };

  // End fetch all books



  const categoryoptions= [
    { label: 'Mobile Game, Game Development and Virtual Reality', value: 'Mobile Game, Game Development and Virtual Reality' },
    { label: 'Information Retrieval and Search Engines', value: 'Information Retrieval and Search Engines' },
    { label: 'Artificial Intelligence and Machine Learning', value: 'Artificial Intelligence and Machine Learning' },
    { label: 'E-commerce and Digital Marketing', value: 'E-commerce and Digital Marketing' },
    { label: 'Social Network Analysis and Mining ', value: 'Social Network Analysis and Mining' },
    { label: 'Health Informatics', value: 'Health Informatics' },
    { label: 'Natural Language Processing (NLP)', value: 'Natural Language Processing (NLP)' },
    { label: 'Geographic Information Systems (GIS)', value: 'Geographic Information Systems (GIS)' },
    { label: 'Educational Technology', value: 'Educational Technology' },
    { label: 'Robotic Systems and Automation', value: 'Robotic Systems and Automation' },
    { label: 'Desktop Games and Applications', value: 'Desktop Games and Applications' },
    { label: 'Web Games and Applications', value: 'Web Games and Applications' },
    { label: 'Mobile Games and Applications', value: 'Mobile Games and Applications' },
    { label: 'Enterprise and E-commerce', value: 'Enterprise and E-commerce' },
    { label: 'Organization Applications', value: 'Organizational Applications' },
  ];

  const [selectedCategory, setSelectedCategory] = useState(""); // State variable for selected category
  const [selectedCourse, setSelectedCourse] = useState(""); // State variable for selected course
  const [selectedFromDate, setSelectedFromDate] = useState(""); // State variable for selected from date
  const [selectedToDate, setSelectedToDate] = useState(""); 

  const removeCategory = () => {
    setSelectedCategory("");
  };

  // Function to clear selected course
  const removeCourse = () => {
    setSelectedCourse("");
  };

  // Function to clear selected date range
  const clearDate = () => {
    setSelectedFromDate("");
    setSelectedToDate("");
  };



  const [sortByTitle, setSortByTitle] = useState(false);
  const [sortByCategory, setSortByCategory] = useState(false);
  const [sortByCourse, setSortByCourse] = useState(false);


  const [showDrawer, setShowDrawer] = useState(false);

    useEffect(() => {
    const handleScroll = () => {
      setShowDrawer(false);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div
      style={{ display: "flex", height: "auto", overflow: "scroll initial"  }}
    >
      {/* For Sidebar */}
      <CDBBox display="flex" alignContent="start" >
        <CDBSidebar textColor="#fff" backgroundColor="#800000" style={{height:'100%'}}>
          <SidebarLayout />
        </CDBSidebar>
      </CDBBox>

      <CDBBox
        display="flex"
        justifyContent="center"
       
        style={{
          width: "100%",
          height: "100%",
          justifyItems:'center',
        }}
      >

        <FloatingButton/>
       
        
        <Row>
          <Col>
          <CDBContainer>
          <div className="d-flex justify-content-between align-items-center mt-5">
          <InputGroup className="mb-3" style={{ width: "50vw", marginTop: "10px" }}>
              <InputGroup.Text id="basic-addon1" style={{ borderRadius: "20px 0 0 20px", background: "#f0f0f0", border: "none" }}>
                <FaSearch />
              </InputGroup.Text>
              <Form.Control
                type="text"
                placeholder="Enter search term"
                style={{ borderRadius: "0 20px 20px 0", border: "1px solid #ccc" }}
                spellCheck={true} 
              />
              



            </InputGroup>
          </div>

          <div className="d-flex justify-content-end align-items-center mt-3">
            <div className="d-flex align-items-end">
              <div className="mr-3" style={{marginRight:'25px'}}>
                <FaFilter onClick={() => setShowFilter(!showFilter)} /> Filter {/* Toggle filter visibility */}
              </div>
              <div  onClick={() => setShowDrawer(!showDrawer)}>
        <GrSort /> Sort
      </div>
            </div>
          </div>
          
          {showFilter && ( // Show filter card if showFilter is true
            <CDBCard className="mt-2">
              <CDBCardBody>
                {selectedCategory || selectedCourse || selectedFromDate || selectedToDate ? (
                  <div className="mt-3" style={{marginLeft:'25px'}}>
                    Keyword: 
                    {selectedCategory ?(
                      <div
                        className="selected-color d-inline-block me-2 mb-2 px-2 py-1 rounded"
                        style={{ color: 'black', fontSize: '0.7rem', fontWeight: 'bold' }}
                      >
                        {selectedCategory}
                        <button
                          type="button"
                          className="btn-close ms-2"
                          aria-label="Close"
                          onClick={() => removeCategory()}
                        ></button>
                      </div>
                    ):null}
                    {selectedCourse ?(
                      <div
                        className="selected-color d-inline-block me-2 mb-2 px-2 py-1 rounded"
                        style={{ color: 'black', fontSize: '0.7rem', fontWeight: 'bold' }}
                      >
                        {selectedCourse}
                        <button
                          type="button"
                          className="btn-close ms-2"
                          aria-label="Close"
                          onClick={() => removeCourse()}
                        ></button>
                      </div>
                    ):null}
                    {(selectedFromDate && selectedToDate) ? (
                      <div
                        className="selected-color d-inline-block me-2 mb-2 px-2 py-1 rounded"
                        style={{ color: 'black', fontSize: '0.7rem', fontWeight: 'bold' }}
                      >
                        {selectedFromDate} To {selectedToDate}
                        <button
                          type="button"
                          className="btn-close ms-2"
                          aria-label="Close"
                          onClick={() => clearDate()}
                        ></button>
                      </div>
                    ) : null}

                    <hr
                      className="d-flex justify-content-center"
                      style={{
                        width: "90%",
                        borderTop: "1px solid #000000",
                        margin: "auto",
                        marginTop: "10px",
                        marginBottom: "10px",
                      }}
                    />
                  </div>
                ) : null}

                <Container>
                  <Row className="justify-content-center mt-3 mb-3"> {/* Added justify-content-center class */}
                    <Col md={3}>
                      <Row>
                        <Col>
                          <Form.Group className="mb-3">
                            <Form.Label>From:</Form.Label>
                            <Form.Control type="date"   onChange={(e) => setSelectedFromDate(e.target.value)}/>
                          </Form.Group>
                        </Col>
                        <Col>
                          <Form.Group className="mb-3">
                            <Form.Label>To:</Form.Label>
                            <Form.Control type="date"  onChange={(e) => setSelectedToDate(e.target.value)}/>
                          </Form.Group>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={3}>
                      <Form.Group className="mb-3">
                        <Form.Label>Category:</Form.Label>
                        <Form.Control as="select" className="form-select" value={selectedCategory} onChange={(e) => setSelectedCategory(e.target.value)}>
                          {categoryoptions.map((option) => (
                            <option key={option.value} value={option.value}>{option.label}</option>
                          ))}
                        </Form.Control>
                      </Form.Group>
                    </Col>
                    <Col md={3}>
                      <Form.Group className="mb-3">
                        <Form.Label>Course:</Form.Label>
                        <Form.Control as="select" className="form-select" value={selectedCourse} onChange={(e) => setSelectedCourse(e.target.value)}>
                          <option value="BSCS">BSCS</option>
                          <option value="BSIT">BSIT</option>
                          <option value="BSIS">BSIS</option>
                        </Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>    
                  <Row className="justify-content-end mb-4">
                    <Col md={3}>
                      <Button variant="primary" >Apply Filters</Button> 
                    </Col>
                  </Row>
                </Container>
              </CDBCardBody>
            </CDBCard>
          )}

          <div className="d-flex justify-content-end mt-3">
            <div>
              Showing 1 - 10 out of 11
            </div>
          </div>

          </CDBContainer>

          <CardLib/>

          </Col>
        </Row>
      </CDBBox>


     

{showDrawer && (
                <div
                  style={{
                    position: "fixed",
                    top: 230,
                    right: 75,
                    zIndex: 999,
                    filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.99))'
                  }}
                >
                   <div
                    style={{
                      position: "relative",
                      background: "#f8f9fa",
                      boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
                      borderRadius: "5px",
                      width: "15vw",
                    }}
                  >
                    <Container className="p-3">
                    <Form.Check
      type="radio"
      name="sortOption"
      label="Sort by Title"
      checked={sortByTitle}
      onChange={() => {
        setSortByTitle(true);
        setSortByCategory(false);
        setSortByCourse(false);
      }}
    />
    {/* Radio button for category sorting */}
    <Form.Check
      type="radio"
      name="sortOption"
      label="Sort by Category"
      checked={sortByCategory}
      onChange={() => {
        setSortByTitle(false);
        setSortByCategory(true);
        setSortByCourse(false);
      }}
    />
    <Form.Check
      type="radio"
      name="sortOption"
      label="Sort by Course"
      checked={sortByCourse}
      onChange={() => {
        setSortByTitle(false);
        setSortByCategory(false);
        setSortByCourse(true);
      }}
    />
    </Container>
                 
                  </div>
                </div>
)}

    </div>
  );
}
