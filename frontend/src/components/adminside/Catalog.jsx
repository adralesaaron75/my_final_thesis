
import { CDBBox, CDBContainer } from "cdbreact";
import { Link } from "react-router-dom";
import {
  CDBSidebar
} from "cdbreact";
import { NavLink } from "react-router-dom";
import SidebarLayout from "./components_admin/SideBarLayout";
import TbleData from "./components_admin/TableData";
import { Container, Form, Row, Col, Card, Table, Modal, Button, Nav } from "react-bootstrap";
import React, { useState,useEffect } from 'react';
import { FaPlus, FaTimes, FaArrowLeft, FaArrowRight } from "react-icons/fa";
import StudentList from "./pages_admin/studentlist";
import { FaEdit, FaSave, FaGreaterThan, FaLessThan } from "react-icons/fa";
import { VITE_BACKEND_URL } from "../../App";
import axios from "axios";
import Badge from "react-bootstrap/esm/Badge";
import SearchResultCount from "../studentside/components_students/ResultCounter";
import {BsSortDown, BsSortUp} from 'react-icons/bs'
import { MdEditNote, MdDeleteSweep } from "react-icons/md";





export default function Students({  }) {
  // export default function Students({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  
  
  const [students, setStudents] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [books, setBooks] = useState([]);
  const [sortBy, setSortBy] = useState("email");
  const [sortOrder, setSortOrder] = useState("asc");
  const [isEditMode, setIsEditMode] = useState(false);

  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage] = useState(10);
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;

  

  useEffect(() => {
    // Fetch data from your backend API when the component mounts
    axios.get('http://localhost:3000/api/getAllUser') // Adjust the URL according to your server setup
      .then(response => {
        setStudents(response.data.data);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, []);

  useEffect(() => {
    // Map student data to book objects when students state updates
    const mappedBooks = students.map(student => ({
      id: student._id,
      name: student.name,
      username: student.course,
      email: student.email,
      role: student.userType,
      status: "Approved"
    }));
    setBooks(mappedBooks);
  }, [students]);

  const handleSort = (key) => {
    if (sortBy === key) {
      setSortOrder(sortOrder === "asc" ? "desc" : "asc");
    } else {
      setSortBy(key);
      setSortOrder("asc");
    }
  };

  const handleSearch = (e) => {
    setSearchQuery(e.target.value);
  };

  const filteredBooks = books.filter(book =>
    book.username.toLowerCase().includes(searchQuery.toLowerCase()) ||
    book.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const sortedBooks = filteredBooks.sort((a, b) => {
    if (sortOrder === "asc") {
      return a[sortBy] > b[sortBy] ? 1 : -1;
    } else {
      return a[sortBy] < b[sortBy] ? 1 : -1;
    }
  });

  const toggleEditMode = () => {
    setIsEditMode(!isEditMode);
  };

  const renderFirstColumn = (index) => {
    if (isEditMode) {
      return <td  style={{width:'20px'}}><input type="checkbox" /></td>;
    } else {
      return <td  style={{width:'20px'}}>{index + 1}</td>;
    }
  };

  const currentItems = sortedBooks.slice(indexOfFirstItem, indexOfLastItem);

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };



  const [totalCount, setTotalCount] = useState(0);

  useEffect(() => {
    // Fetch total count from backend API
    axios.get('http://localhost:3000/api/countAllUser')
      .then(response => {
        setTotalCount(response.data.totalCount);
      })
      .catch(error => {
        console.error('Error fetching total count:', error);
      });
  }, []);



 


    




 

  return (
    <div
      style={{ display: "flex", height: "auto", overflow: "scroll initial" }}
    >
      <CDBBox display="flex" alignContent="start">
        <CDBSidebar textColor="#fff" backgroundColor="#800000" style={{height:'100%'}}>
          <SidebarLayout />
        </CDBSidebar>
      </CDBBox>

      <CDBBox
        // display="flex"
        justifyContent="center"
        style={{
          width: "100%",
          height: "100%",
        }}
      >

        

      <Container >
       
        <div className="d-flex justify-content-end mb-4">
            <SearchResultCount
              currentPage={currentPage}
              itemsPerPage={itemsPerPage}
              totalItems={sortedBooks.length}
            />
          </div>

        <Table className="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th onClick={() => handleSort("name")}>
                    Name{' '}
                    {sortBy === "name" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th onClick={() => handleSort("username")}>
                    User Name{' '}
                    {sortBy === "username" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th onClick={() => handleSort("email")}>
                    Email{' '}
                    {sortBy === "email" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th onClick={() => handleSort("role")}>
                    Role{' '}
                    {sortBy === "role" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              {currentItems.map((book, index) => (
                  <tr key={index}>
                    {renderFirstColumn(index)}
                    <td>{book.name}</td>
                    <td>{book.username}</td>
                    <td>{book.email}</td>
                    <td>{book.role}</td>
                    <td>{book.status}</td>
                    
                  </tr>
                ))}
              </tbody>
            </Table>
        
      </Container>

      <div style={{ display: 'flex', justifyContent: 'center', marginTop: '45px', marginBottom:'20px'}}>
  <nav aria-label="Page navigation example">
    <ul className="pagination">
      <li className={`page-item ${currentPage === 1 && 'disabled'}`}>
        <button className="page-link" onClick={() => paginate(currentPage - 1)} aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span className="sr-only">Previous</span>
        </button>
      </li>
      {Array.from({ length: Math.ceil(sortedBooks.length / itemsPerPage) }, (_, i) => (
        <li key={i} className={`page-item ${currentPage === i + 1 && 'active'}`}>
          <button className="page-link" onClick={() => paginate(i + 1)}>{i + 1}</button>
        </li>
      ))}
      <li className={`page-item ${currentPage === Math.ceil(sortedBooks.length / itemsPerPage) && 'disabled'}`}>
        <button className="page-link" onClick={() => paginate(currentPage + 1)} aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span className="sr-only">Next</span>
        </button>
      </li>
    </ul>
  </nav>
</div>










<Container fluid>
  <Card>
    <Card.Header>
    <Table className="table" style={{}}>
              <thead>
                <tr>
                  <th style={{width:'20px'}}>#</th>
                  <th onClick={() => handleSort("name")} style={{width:'150px'}}>
                    Name{' '}
                    {sortBy === "name" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th onClick={() => handleSort("username")}  style={{width:'120px'}}>
                    User Name{' '}
                    {sortBy === "username" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th onClick={() => handleSort("email")}  style={{width:'200px'}}>
                    Email{' '}
                    {sortBy === "email" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th onClick={() => handleSort("role")}  style={{width:'200px'}}>
                    Role{' '}
                    {sortBy === "role" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th style={{width:'200px'}}>Status</th>
                 
                </tr>
              </thead>
             
            </Table>
      
    </Card.Header>
    <Card.Body style={{ background:'yellow'}}>
     
    <Table className="table">
             
              <tbody>
              {currentItems.map((book, index) => (
                  <tr key={index}>
                    {renderFirstColumn(index)}
                    <td style={{width:'150px'}}>{book.name}</td>
                    <td style={{width:'120px'}}>{book.username}</td>
                    <td  style={{width:'200px'}}>{book.email}</td>
                    <td  style={{width:'200px'}}>{book.role}</td>
                    <td  style={{width:'200px'}}>active</td>
                  
                    
                  </tr>
                ))}
              </tbody>
            </Table>
          
           



    </Card.Body>
  </Card>


</Container>








        
      </CDBBox>
    </div>
  );
}
