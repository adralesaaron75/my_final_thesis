import { CDBBox, CDBContainer } from "cdbreact";
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from "cdbreact";
import React, { useState } from "react";
import { Link, useLocation  } from "react-router-dom";
import { NavLink } from "react-router-dom";
import Reportsnav from "./pages_admin/navbarreports";

import SidebarLayout from "./components_admin/SideBarLayout";
import BoxDash from "./components_admin/BoxDashboards";
import FloatingButton from "./components_admin/FloatingAdd";

import { Card, Row, Col, Container, Navbar, Nav} from "react-bootstrap";
import Footer from "../studentside/components_students/Footer";

export default function Admin({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  return (
    <>
      <div
        style={{ display: "flex", height: "auto", overflow: "scroll initial" }}
      >
        <CDBBox display="flex" alignContent="start">
          <CDBSidebar
            textColor="#fff"
            backgroundColor="#800000"
            style={{ height: "100%" }}
          >
            <SidebarLayout />
          </CDBSidebar>
        </CDBBox>

        <>
          <CDBBox
            display="flex"
            justifyContent="center"
            style={{
              width: "100%",
              height: "100%",
            }}
          >
 <Container className="mt-5">
        <Reportsnav/>
        <Container>
       Approved
            </Container>
            </Container>
           
          </CDBBox>
        </>
      </div>
    </>
  );
}
