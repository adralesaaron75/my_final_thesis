import { CDBBox, CDBContainer } from "cdbreact";
import {
  CDBSidebar,
} from "cdbreact";
import React, { useState } from 'react';
import SidebarLayout from "./components_admin/SideBarLayout";
import ChipCust from "./components_admin/DropdownCategory";
import { Container, Row, Col, Form, Table, Button } from "react-bootstrap";
import FloatingButton from "./components_admin/FloatingAdd";
import {BsSortDown, BsSortUp} from 'react-icons/bs'
import { MdEditNote, MdDeleteSweep } from "react-icons/md";


export default function Returnedbooks({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };


  const [selectedColors, setSelectedColors] = useState([]);

  const handleColorChange = (color) => {
    setSelectedColors((prevSelected) => {
      if (prevSelected.includes(color)) {
        return prevSelected.filter((c) => c !== color);
      } else {
        return [...prevSelected, color];
      }
    });
  };


  const [searchQuery, setSearchQuery] = useState("");
  const [books, setBooks] = useState([
    { id: 1, submissionId: 30001, title: "Title Sample 1", author: "Jose Rizal", submissionDate: "12/20/1915", status: "Approved" },
    { id: 2, submissionId: 30002, title: "Title Sample 2", author: "Juan Luna", submissionDate: "1/5/1912", status: "Pending" },
    { id: 3, submissionId: 30003, title: "Title Sample 3", author: "Emilio Aguinaldo", submissionDate: "7/2/1905", status: "Rejected" }
  ]);
  const [sortBy, setSortBy] = useState("submissionId");
  const [sortOrder, setSortOrder] = useState("asc");

  const handleSort = (key) => {
    if (sortBy === key) {
      setSortOrder(sortOrder === "asc" ? "desc" : "asc");
    } else {
      setSortBy(key);
      setSortOrder("asc");
    }
  };

  const filteredBooks = books.filter(book =>
    book.title.toLowerCase().includes(searchQuery.toLowerCase()) ||
    book.author.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const sortedBooks = filteredBooks.sort((a, b) => {
    if (sortOrder === "asc") {
      return a[sortBy] > b[sortBy] ? 1 : -1;
    } else {
      return a[sortBy] < b[sortBy] ? 1 : -1;
    }
  });

  const handleSearch = (e) => {
    setSearchQuery(e.target.value);
  };


  const [isEditMode, setIsEditMode] = useState(false);

  const toggleEditMode = () => {
    setIsEditMode(!isEditMode);
  };

  const renderFirstColumn = (index) => {
    if (isEditMode) {
      return <td><input type="checkbox" /></td>;
    } else {
      return <td>{index + 1}</td>;
    }
  };


  
  return (
    <div
      style={{ display: "flex", height: "auto", overflow: "scroll initial" , }}
    >
      <CDBBox display="flex" alignContent="start" >
        <CDBSidebar textColor="#fff" backgroundColor="#800000" style={{height:'100%'}}>
          <SidebarLayout/>
        </CDBSidebar>
      </CDBBox>


      <CDBBox
        
        display="flex"
        justifyContent="start"
        style={{
          width: "100%",
          height: "100%",
        }}
      >

        <FloatingButton/>
        
        <Container fluid>
        <Row className="mt-3 mb-2">
          <Col xs={12} md={6}>
            <Form.Group controlId="search">
              <Form.Control type="text" placeholder="Search User" onChange={handleSearch} />
            </Form.Group>
          </Col>

          
        </Row >
        <Row  className="mt-3 mb-2">
        <Col xs={12} md={10}>
        
        </Col>

        <Col xs={12} md={2}>
  <Button style={{float: "right"}} variant="outline-danger" size="sm" onClick={toggleEditMode}><MdDeleteSweep/></Button>
  <Button style={{marginRight:'5px', marginleft:'10px', float: "right" }} variant="outline-success" size="sm" onClick={toggleEditMode}><MdEditNote/></Button>
</Col>
        </Row>
        <Row>
          <Col>
            <Table className="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th onClick={() => handleSort("submissionId")}>
                    Submission ID{' '}
                    {sortBy === "submissionId" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th onClick={() => handleSort("title")}>
                    Title{' '}
                    {sortBy === "title" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th onClick={() => handleSort("author")}>
                    Author{' '}
                    {sortBy === "author" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th onClick={() => handleSort("submissionDate")}>
                    Submission Date{' '}
                    {sortBy === "submissionDate" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                {sortedBooks.map((book, index) => (
                  <tr key={index}>
                    {renderFirstColumn(index)}
                    <td>{book.submissionId}</td>
                    <td>{book.title}</td>
                    <td>{book.author}</td>
                    <td>{book.submissionDate}</td>
                    <td>{book.status}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
    </Container>


      </CDBBox>
    </div>
  );
}
