import { CDBBox, CDBContainer } from "cdbreact";
import {
  CDBSidebar,
} from "cdbreact";
import SidebarLayout from "./components_admin/SideBarLayout";
import { Container, Row, Col, Form, Table, Button} from "react-bootstrap";
import FloatingButton from "./components_admin/FloatingAdd";
import {BsSortDown, BsSortUp} from 'react-icons/bs'
import { MdEditNote, MdDeleteSweep } from "react-icons/md";
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import SearchResultCount from "../studentside/components_students/ResultCounter";
import UsersNav from "./pages_admin/usernavigation";
import { FaPlus } from "react-icons/fa";

export default function AdminData({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  
  const [students, setStudents] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [books, setBooks] = useState([]);
  const [sortBy, setSortBy] = useState("email");
  const [sortOrder, setSortOrder] = useState("asc");
  const [isEditMode, setIsEditMode] = useState(false);

  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage] = useState(10);
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;

  

  useEffect(() => {
    // Fetch data from your backend API when the component mounts
    axios.get('http://localhost:3000/api/getAllUser') // Adjust the URL according to your server setup
      .then(response => {
        // Filter users with userType of "student"
        const filteredStudents = response.data.data.filter(user => user.userType === "admin");
        setStudents(filteredStudents);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, []);

  useEffect(() => {
    // Map student data to book objects when students state updates
    const mappedBooks = students.map(student => ({
      id: student._id,
      name: student.name,
      username: student.course,
      email: student.email,
      role: student.userType,
      status: "Approved"
    }));
    setBooks(mappedBooks);
  }, [students]);

  const handleSort = (key) => {
    if (sortBy === key) {
      setSortOrder(sortOrder === "asc" ? "desc" : "asc");
    } else {
      setSortBy(key);
      setSortOrder("asc");
    }
  };

  const handleSearch = (e) => {
    setSearchQuery(e.target.value);
  };

  const filteredBooks = books.filter(book =>
    book.username.toLowerCase().includes(searchQuery.toLowerCase()) ||
    book.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const sortedBooks = filteredBooks.sort((a, b) => {
    if (sortOrder === "asc") {
      return a[sortBy] > b[sortBy] ? 1 : -1;
    } else {
      return a[sortBy] < b[sortBy] ? 1 : -1;
    }
  });

  const toggleEditMode = () => {
    setIsEditMode(!isEditMode);
  };

  const renderFirstColumn = (index) => {
    if (isEditMode) {
      return <td><input type="checkbox" /></td>;
    } else {
      return <td>{index + 1}</td>;
    }
  };

  const currentItems = sortedBooks.slice(indexOfFirstItem, indexOfLastItem);

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };



  const [totalCount, setTotalCount] = useState(0);

  useEffect(() => {
    // Fetch total count from backend API
    axios.get('http://localhost:3000/api/countAllUser')
      .then(response => {
        setTotalCount(response.data.totalCount);
      })
      .catch(error => {
        console.error('Error fetching total count:', error);
      });
  }, []);



 

  
  return (
    <div
      style={{ display: "flex", height: "auto", overflow: "scroll initial"  }}
    >
      <CDBBox display="flex" alignContent="start">
        <CDBSidebar textColor="#fff" backgroundColor="#800000" style={{height:'100%'}}>
          <SidebarLayout/>
        </CDBSidebar>
      </CDBBox>


      <CDBBox
        
        display="flex"
        justifyContent="start"
        style={{
          width: "100%",
          height: "100%",
        }}
      >
      <FloatingButton/>
        <Container fluid>
      <Row className="mt-3 mb-3">
        <Col xs={12} md={6}>
         <UsersNav/>
          <Form.Group controlId="search">
            <Form.Control type="text" placeholder="Search"  onChange={handleSearch} />
          </Form.Group>
        </Col>
      </Row>
      <Button style={{float: "right", marginBottom:'25px', fontSize:'12px', marginRight:'25px'}}><FaPlus style={{marginRight:'10px'}}/>Add Admin</Button>

      <Row  className="mt-3 mb-2">
        <Col xs={12} md={10}>
        
        </Col>

        <Col xs={12} md={2}>
  <Button style={{float: "right"}} variant="outline-danger" size="sm" onClick={toggleEditMode}><MdDeleteSweep/></Button>
  <Button style={{marginRight:'5px', marginleft:'10px', float: "right" }} variant="outline-success" size="sm" onClick={toggleEditMode}><MdEditNote/></Button>
</Col>
        </Row>



      <Row>
        <Col>
        <div className="d-flex justify-content-end mb-4">
            <SearchResultCount
              currentPage={currentPage}
              itemsPerPage={itemsPerPage}
              totalItems={sortedBooks.length}
            />
          </div>

        <Table className="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th onClick={() => handleSort("name")}>
                    Name{' '}
                    {sortBy === "name" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th onClick={() => handleSort("username")}>
                    User Name{' '}
                    {sortBy === "username" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th onClick={() => handleSort("email")}>
                    Email{' '}
                    {sortBy === "email" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th onClick={() => handleSort("role")}>
                    Role{' '}
                    {sortBy === "role" && sortOrder === "asc" ? <BsSortUp /> : <BsSortDown />}
                  </th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              {currentItems.map((book, index) => (
                  <tr key={index}>
                    {renderFirstColumn(index)}
                    <td>{book.name}</td>
                    <td>{book.username}</td>
                    <td>{book.email}</td>
                    <td>{book.role}</td>
                    <td>{book.status}</td>
                    
                  </tr>
                ))}
              </tbody>
            </Table>
        </Col>
      </Row>

      <div style={{ display: 'flex', justifyContent: 'center', marginTop: '45px', marginBottom:'20px'}}>
  <nav aria-label="Page navigation example">
    <ul className="pagination">
      <li className={`page-item ${currentPage === 1 && 'disabled'}`}>
        <button className="page-link" onClick={() => paginate(currentPage - 1)} aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span className="sr-only">Previous</span>
        </button>
      </li>
      {Array.from({ length: Math.ceil(sortedBooks.length / itemsPerPage) }, (_, i) => (
        <li key={i} className={`page-item ${currentPage === i + 1 && 'active'}`}>
          <button className="page-link" onClick={() => paginate(i + 1)}>{i + 1}</button>
        </li>
      ))}
      <li className={`page-item ${currentPage === Math.ceil(sortedBooks.length / itemsPerPage) && 'disabled'}`}>
        <button className="page-link" onClick={() => paginate(currentPage + 1)} aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span className="sr-only">Next</span>
        </button>
      </li>
    </ul>
  </nav>
</div>






    </Container>


      </CDBBox>
    </div>
  );
}
