import { Line } from 'react-chartjs-2';
import { VITE_BACKEND_URL } from '../../../../App';
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { FaArrowCircleUp, FaArrowCircleDown } from "react-icons/fa";

const WeeklyApprovedChart = () => {
  const [data, setData] = useState([]); // Define data state
  const [isLoading, setIsLoading] = useState(false);
  const [dataForSevenDays, setDataForSevenDays] = useState(0);
  const [dataForFourteenDays, setDataForFourteenDays] = useState(0);
  const [dataForTwentyOneDays, setDataForTwentyOneDays] = useState(0);
  const [dataForTwentyEightDays, setDataForTwentyEightDays] = useState(0);
  const [dataForThirtyFiveDays, setDataForThirtyFiveDays] = useState(0);
  const [dataForFortyTwoDays, setDataForFortyTwoDays] = useState(0);
  const [dataForFortyNineDays, setDataForFortyNineDays] = useState(0);

  useEffect(() => {
    fetchBorrowedBooks();
  }, []);
  const fetchBorrowedBooks = async () => {
    try {
      setIsLoading(true);
      const response = await axios.get(`${VITE_BACKEND_URL}/api/getSubmissionapproved`);
      setIsLoading(false);
      if (response.data.status === "ok") {
        setData(response.data.data);
  
        const today = new Date();
        let sumForSevenDays = 0;
        let sumForFourteenDays = 0;
        let sumForTwentyOneDays = 0;
        let sumForTwentyEightDays = 0;
        let sumForThirtyFiveDays = 0;
        let sumForFortyTwoDays = 0;
        let sumForFortyNineDays = 0; // Initialize sum for 49 days
        for (let i = 0; i < 49; i++) { // Loop for 49 days
          const currentDate = new Date(today);
          currentDate.setDate(today.getDate() - i);
          const dayData = response.data.data.filter(item => {
            const createdAtDate = new Date(item.createdAt);
            return (
              createdAtDate.getDate() === currentDate.getDate() &&
              createdAtDate.getMonth() === currentDate.getMonth() &&
              createdAtDate.getFullYear() === currentDate.getFullYear()
            );
          });
          sumForFortyNineDays += dayData.length; // Accumulate count for 49 days
          if (i < 42) {
            sumForFortyTwoDays += dayData.length;
          }
          if (i < 35) {
            sumForThirtyFiveDays += dayData.length;
          }
          if (i < 28) {
            sumForTwentyEightDays += dayData.length;
          }
          if (i < 21) {
            sumForTwentyOneDays += dayData.length;
          }
          if (i < 14) {
            sumForFourteenDays += dayData.length;
          }
          if (i < 7) {
            sumForSevenDays += dayData.length;
          }
        }
        setDataForSevenDays(sumForSevenDays);
        setDataForFourteenDays(sumForFourteenDays);
        setDataForTwentyOneDays(sumForTwentyOneDays);
        setDataForTwentyEightDays(sumForTwentyEightDays);
        setDataForThirtyFiveDays(sumForThirtyFiveDays);
        setDataForFortyTwoDays(sumForFortyTwoDays);
        setDataForFortyNineDays(sumForFortyNineDays); // Set the sum for the last 49 days
      } else {
        console.error('Error fetching borrowed books:', response.data);
      }
    } catch (error) {
      setIsLoading(false);
      console.error('Error fetching borrowed books:', error);
    }
  };
  
  const today = new Date();
const labels = [];
const dataForLabels = [];
for (let i = 6; i >= 0; i--) {
  const currentDate = new Date(today);
  currentDate.setDate(today.getDate() - i);
  const formattedDate = currentDate.toLocaleDateString('en-US', { month: '2-digit', day: '2-digit', year: 'numeric' });
  labels.push(formattedDate);

  let dataForLabel = 0;
  switch(i) {
    case 6: // Data for 49 days ago to 42 days ago
      dataForLabel = dataForFortyNineDays - dataForFortyTwoDays;
      break;
    case 5: // Data for 42 days ago to 35 days ago
      dataForLabel = dataForFortyTwoDays - dataForThirtyFiveDays;
      break;
    case 4: // Data for 35 days ago to 28 days ago
      dataForLabel = dataForThirtyFiveDays - dataForTwentyEightDays;
      break;
    case 3: // Data for 28 days ago to 21 days ago
      dataForLabel = dataForTwentyEightDays - dataForTwentyOneDays;
      break;
    case 2: // Data for 21 days ago to 14 days ago
      dataForLabel = dataForTwentyOneDays - dataForFourteenDays;
      break;
    case 1: // Data for 14 days ago to 7 days ago
      dataForLabel = dataForFourteenDays - dataForSevenDays;
      break;
    case 0: // Data for 7 days ago to today
      dataForLabel = dataForSevenDays;
      break;
    default:
      break;
  }
  dataForLabels.push(dataForLabel);
}




const calculatePercentageChange = () => {
  
  const totaldiff = dataForFourteenDays - dataForSevenDays
  // Check if there were no submissions yesterday
  if (totaldiff === 0) {
      // If there were no submissions both today and yesterday, return 0
      if (dataForSevenDays === 0) return 0;
      // If there were submissions today but none yesterday, return 100%
      return 100;
  }
  
  // Calculate percentage change
  if (dataForSevenDays === totaldiff) {
      return 0; // If the counts are equal, return 0%
  }
  else if (dataForSevenDays > totaldiff) {
    const percentageChange = ((totaldiff - dataForSevenDays) / dataForSevenDays) * -100 ;
  
    return Math.round(percentageChange);
  }
  else {
      const percentageChange = ((dataForSevenDays - totaldiff)/totaldiff) * -100 ;
    
      return Math.round(percentageChange);
  }
};




// const arrowIcon = datafortoday > dataforyesterday ? <FaArrowCircleUp /> : <FaArrowCircleDown />;
let arrowIcon;
if (dataForSevenDays > dataForFourteenDays - dataForSevenDays) {
  arrowIcon = <FaArrowCircleUp />;
} else {
  arrowIcon = <FaArrowCircleDown />;
}

let color;
if (dataForSevenDays > dataForFourteenDays - dataForSevenDays) {
 color = 'green';
} else if (dataForSevenDays === dataForFourteenDays - dataForSevenDays) {
  color = '#FFC107'; // Assuming you want a different color for equality
} else {
 color = 'red';
}


const percentageChange = calculatePercentageChange();

const displayPercentageChange = percentageChange + '%';

const Chartdata = {
  labels: labels,
  datasets: [
    {
      data: dataForLabels,
      fill: true,
      borderColor: 'rgba(75, 192, 192, 1)',
      tension: 0,
      borderWidth: 2,
      pointStyle: false,
    },
  ],
};

  
  
  
  
  

  const options = {
    scales: {
      x: {
        display: false, // Hides x-axis labels
      },
      y: {
        display: false, // Hides y-axis labels
      },
    },
    plugins: {
      legend: {
        display: false, // Hides the legend
      },
    },
  };

  return (
   
<div className="m-2 mt-3">
<div style={{ display: "flex", alignItems: "center" }}>
  <div style={{ marginLeft: "auto", justifyItems: "center" }}>
    <h1>{dataForSevenDays}</h1>
    <div className="d-flex align-items-center" style={{ color: color }}>
   
      {arrowIcon}
      <h6 className="m-2">{displayPercentageChange}</h6>
    </div>
  </div>
  <div style={{ height: '90px', width: '190px', marginLeft: 'auto' }}>
    <Line data={Chartdata} options={options} />
  </div>
</div>
</div>
  );
};

export default WeeklyApprovedChart;


// <div className="m-2 mt-3">
// <div style={{ display: "flex", alignItems: "center" }}>
//   <div style={{ marginLeft: "auto", justifyItems: "center" }}>
//     <h1>{datafortoday}</h1>
//     <div className="d-flex align-items-center" style={{ color: color }}>
//       {arrowIcon}
//       <h6 className="m-2">{displayPercentageChange}</h6>
//     </div>
//   </div>
//   <div style={{ height: '90px', width: '190px', marginLeft: 'auto' }}>
//     <Line data={Chartdata} options={options} />
//   </div>
// </div>
// </div>