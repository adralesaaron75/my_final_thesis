import React from "react";
import { Container, Row, Col, Card, CardBody } from "react-bootstrap";
import { FaArrowCircleUp, FaArrowCircleDown } from "react-icons/fa";
import WeeklySubmittedChart from "./WeeklySubChart";
import WeeklyApprovedChart from "./WeeklyApproved";
import WeeklyRejectedChart from "./WeeklyRejectedChart";
import WeeklyWebScrapedChart from "./WeeklyWebScraChart";


const WeeklyDash = () => {
  return (
    <Row className="mt-1">
      <Col>
        <Card>
          <Card.Header   style={{background:'#7DCCCC' ,color:'white', border:'none'}}>Weekly Submit</Card.Header>
          
              <WeeklySubmittedChart />
           
        </Card>
      </Col>

      <Col>
        <Card>
          <Card.Header   style={{background:'#7DCCCC' ,color:'white', border:'none'}}>Weekly Approved</Card.Header>
         
              <WeeklyApprovedChart />
           
        </Card>
      </Col>
      <Col>
        <Card>
          <Card.Header   style={{background:'#7DCCCC' ,color:'white', border:'none'}}>Weekly Rejected</Card.Header>
         
              <WeeklyRejectedChart />
           
        </Card>
      </Col>
      <Col>
        <Card>
          <Card.Header    style={{background:'#7DCCCC' ,color:'white', border:'none'}}>Weekly WebScraped</Card.Header>
          
              <WeeklyWebScrapedChart />
           
        </Card>
      </Col>
    </Row>
  );
};

export default WeeklyDash;
