import { Table } from "react-bootstrap";
import { Container, Card, CardHeader, CardBody } from "react-bootstrap";
import React, { useState, useEffect } from 'react';
import axios from 'axios'; // Import axios for making HTTP requests
import { VITE_BACKEND_URL } from "../../../App";
import { Nav, NavItem, NavLink } from 'react-bootstrap';
import { Link, useLocation } from 'react-router-dom';

const renderCircleWithLetter = (name) => {
  const startingLetter = name.charAt(0).toUpperCase();
  return (
    <div
      style={{
        width: "30px",
        height: "30px",
        borderRadius: "50%",
        backgroundColor: "gray",
        textAlign: "center",
        lineHeight: "30px",
        color: "white",
        marginRight: "5px",
      }}
    >
      {startingLetter}
    </div>
  );
};

const TableRow = ({ user }) => {
  const { name, course } = user;
  return (
    <tr>
      <td>
        <div className="d-flex align-items-center">
          <div className="d-flex align-items-center mr-3">
            {renderCircleWithLetter(name)}
          </div>
          <div>{name}</div>
        </div>
      </td>
      <td>{course}</td>
    </tr>
  );
};
const Widget1 = () => {
  const [students, setStudents] = useState([]);
  const location = useLocation();

  useEffect(() => {
    // Fetch data from your backend API when the component mounts
    axios.get(`${VITE_BACKEND_URL}/api/getAllUser`) // Use VITE_BACKEND_URL instead of hard-coded URL
      .then(response => {
        // Filter only admin users
        const adminStudents = response.data.data.filter(user => user.userType === 'student');
        setStudents(adminStudents);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, []);

  return (
    <Container className="mb-4">
      <Card>
        <CardHeader style={{background:'#7DCCCC' ,color:'white'}}>Recent Student</CardHeader>
        <CardBody>
          <Table className="table table-borderless">
            <tbody>
              {students.slice(0, 8).map((user, index) => ( // Limiting to 10 rows here
                <TableRow key={index} user={user} />
              ))}
            </tbody>
          </Table>
        </CardBody>
        <Card.Footer className="text-center bg-light">
        
        
        <NavLink as={Link} to="/admin/admin/res" className={location.pathname === '/admin/admin/res' ? 'active' : ''} style={{color:'#0D6EFD'}}>View all Admin &gt;</NavLink>
     
     
        </Card.Footer>
      </Card>
    </Container>
  );
};

export default Widget1;

