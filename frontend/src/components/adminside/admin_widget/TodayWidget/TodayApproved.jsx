import { Line } from 'react-chartjs-2';
import { VITE_BACKEND_URL } from '../../../../App';
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { FaArrowCircleUp, FaArrowCircleDown } from "react-icons/fa";

const ApprovedChart = () => {
  const [data, setData] = useState([]); // Define data state
  const [isLoading, setIsLoading] = useState(false);
  const [datafortoday, setdatafortoday] = useState("");
  const [dataforyesterday, setdataforyesterday] = useState("");
  const [dataForPreviousDays, setDataForPreviousDays] = useState([]);

  useEffect(() => {
    fetchBorrowedBooks();
  }, []);

  const fetchBorrowedBooks = async () => {
    try {
      setIsLoading(true);
      const response = await axios.get(`${VITE_BACKEND_URL}/api/getSubmissionapproved`);
      setIsLoading(false);
      if (response.data.status === "ok") {
        setData(response.data.data);

        // Count data for today
        const today = new Date(); // Get today's date
        const todayData = response.data.data.filter(item => {
          const createdAtDate = new Date(item.createdAt); // Convert createdAt to Date object
          // Check if createdAt is today
          return (
            createdAtDate.getDate() === today.getDate() &&
            createdAtDate.getMonth() === today.getMonth() &&
            createdAtDate.getFullYear() === today.getFullYear()
          );
        });
        setdatafortoday(todayData.length); // Set the count for today's data
        
        const yesterday = new Date(today); // Kunin ang petsa ng kahapon
yesterday.setDate(today.getDate() - 1); // I-adjust ang petsa papunta sa kahapon
const yesterdayData = response.data.data.filter(item => {
  const createdAtDate = new Date(item.createdAt); // Convert createdAt to Date object
  // Check if createdAt is yesterday
  return (
    createdAtDate.getDate() === yesterday.getDate() &&
    createdAtDate.getMonth() === yesterday.getMonth() &&
    createdAtDate.getFullYear() === yesterday.getFullYear()
  );
});
setdataforyesterday(yesterdayData.length); 

        // Count data for previous days
        const previousDaysData = [];
        for (let i = 1; i <= 6; i++) {
          const previousDay = new Date(today);
          previousDay.setDate(today.getDate() - i);
          const previousDayData = response.data.data.filter(item => {
            const createdAtDate = new Date(item.createdAt); // Convert createdAt to Date object
            // Check if createdAt is the previous day
            return (
              createdAtDate.getDate() === previousDay.getDate() &&
              createdAtDate.getMonth() === previousDay.getMonth() &&
              createdAtDate.getFullYear() === previousDay.getFullYear()
            );
          });
          previousDaysData.push(previousDayData.length);
        }
        setDataForPreviousDays(previousDaysData.reverse()); // Set the count for previous days
      } else {
        console.error('Error fetching borrowed books:', response.data);
      }
    } catch (error) {
      setIsLoading(false);
      console.error('Error fetching borrowed books:', error);
    }
  };
  



  const calculatePercentageChange = () => {
  

    // Check if there were no submissions yesterday
    if (dataforyesterday === 0) {
        // If there were no submissions both today and yesterday, return 0
        if (datafortoday === 0) return 0;
        // If there were submissions today but none yesterday, return 100%
        return 100;
    }
    
    // Calculate percentage change
    if (datafortoday === dataforyesterday) {
        return 0; // If the counts are equal, return 0%
    }
    else if (datafortoday > dataforyesterday) {
      const percentageChange = ((dataforyesterday - datafortoday) / datafortoday) * -100 ;
    
      return Math.round(percentageChange);
    }
    else {
        const percentageChange = ((datafortoday - dataforyesterday)/dataforyesterday) * -100 ;
      
        return Math.round(percentageChange);
    }
};
  
  const percentageChange = calculatePercentageChange();

   // const arrowIcon = datafortoday > dataforyesterday ? <FaArrowCircleUp /> : <FaArrowCircleDown />;
   let arrowIcon;
   if (datafortoday > dataforyesterday) {
     arrowIcon = <FaArrowCircleUp />;
  
   } else {
     arrowIcon = <FaArrowCircleDown />;
   }
   
   let color;
   if (datafortoday > dataforyesterday) {
     color = 'green';
   } else if (datafortoday === dataforyesterday) {
     color = '#FFC107'; // Assuming you want a different color for equality
   } else {
     color = 'red';
   }
  
  // Corrected display of percentage change
  const displayPercentageChange = percentageChange + '%';

  const formatDate = (date) => {
    // Format the date as "MM/DD/YYYY"
    const month = (date.getMonth() + 1).toString().padStart(2, '0'); // Get month and pad with leading zero if needed
    const day = date.getDate().toString().padStart(2, '0'); // Get day and pad with leading zero if needed
    const year = date.getFullYear(); // Get full year
    return `${month}/${day}/${year}`;
  };

  const todayLabel = formatDate(new Date()); // Get today's date and format it
  const previousDaysLabels = [];
  for (let i = 1; i <= 6; i++) {
    const previousDay = new Date();
    previousDay.setDate(previousDay.getDate() - i);
    previousDaysLabels.unshift(formatDate(previousDay)); // Format and add to the beginning of the array
  }


  const Chartdata = {
    labels: [...previousDaysLabels, todayLabel],
    datasets: [
      {
        data: [...dataForPreviousDays, datafortoday], // Data for today and previous days
        fill: true,
        borderColor: 'rgba(0,67,255,2)',
        tension: 0,
        borderWidth: 2, // Hides the line
        pointStyle: false, // Display only the line, no dots
      },
    ],
  };

  const options = {
    scales: {
      x: {
        display: false, // Hides x-axis labels
      },
      y: {
        display: false, // Hides y-axis labels
      },
    },
    plugins: {
      legend: {
        display: false, // Hides the legend
      },
    },
  };

  return (
    <div className="m-2 mt-3">
      <div style={{ display: "flex", alignItems: "center" }}>
        <div style={{ marginLeft: "auto", justifyItems: "center" }}>
          <h1>{datafortoday}</h1>
          <div className="d-flex align-items-center" style={{ color: color }}>
            {arrowIcon}
            <h6 className="m-2">{displayPercentageChange}</h6>
          </div>
        </div>
        <div style={{ height: '90px', width: '190px', marginLeft: 'auto' }}>
          <Line data={Chartdata} options={options} />
        </div>
      </div>
    </div>
  );
};

export default ApprovedChart;
