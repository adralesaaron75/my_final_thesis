import React from "react";
import { Container, Row, Col, Card, CardBody } from "react-bootstrap";
import SubmittedChart from "./TodaySubChart";
import ApprovedChart from "./TodayApproved";
import { FaArrowCircleUp, FaArrowCircleDown } from "react-icons/fa";
import RejectedChart from "./TodayRejectedChart";
import WebScrapedChart from "./TodayWebScraChart";

const TodayDash = () => {
  return (
    <Row className="mt-4 mb-3">
      <Col>
        <Card>
          <Card.Header   style={{background:'#7DCCCC' ,color:'white', border:'none'}}>Today Submit</Card.Header>
         
              <SubmittedChart />
         
        </Card>
      </Col>

      <Col>
        <Card>
          <Card.Header   style={{background:'#7DCCCC' ,color:'white', border:'none'}}>Today Approved</Card.Header>
          <ApprovedChart/>
        </Card>
      </Col>
      <Col>
      <Card>
  <Card.Header  style={{background:'#7DCCCC' ,color:'white', border:'none'}}>Today Rejected</Card.Header>
 
      <RejectedChart />
    
</Card>

      </Col>
      <Col>
        <Card>
          <Card.Header   style={{background:'#7DCCCC' ,color:'white', border:'none'}} >Today WebScraped</Card.Header>
         
              <WebScrapedChart />
           
        </Card>
      </Col>
    </Row>
  );
};

export default TodayDash;
