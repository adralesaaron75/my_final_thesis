
import React from "react";
import { Container, Row, Col, Card, CardHeader, CardBody } from "react-bootstrap";
import RadarChartWid from "./RadarGraph";


const Widget5 = () => {
  return (
    <Container className="mb-4">
        <Card>
           <CardHeader style={{background:'#7DCCCC' ,color:'white'}}>Line Chart</CardHeader>
           <CardBody>
            <RadarChartWid/>
            

           </CardBody>


        </Card>


        
    </Container>
  );
};

export default Widget5;
