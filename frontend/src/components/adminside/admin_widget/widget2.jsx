import React, { useState, useEffect } from 'react';
import { Container, Card, CardHeader, CardBody, Table } from "react-bootstrap";
import axios from "axios";
import { VITE_BACKEND_URL } from "../../../App";

const Widget2 = () => {
  const [dataRejected, setDataRejected] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${VITE_BACKEND_URL}/api/getAllBorrowedBooks`);
        setDataRejected(response.data.data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  // Function to format date as "mm/dd/yyyy"
  const formatDate = (dateString) => {
    const dateObject = new Date(dateString);
    const month = dateObject.getMonth() + 1;
    const day = dateObject.getDate();
    const year = dateObject.getFullYear();
    return `${month}/${day}/${year}`;
  };

  // Function to check if a date is within the last week
  const isWithinLastWeek = (dateString) => {
    const oneWeekAgo = new Date();
    oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);
    const submissionDate = new Date(dateString);
    return submissionDate >= oneWeekAgo;
  };

  return (
    <Container className="mb-4">
      <Card>
        <CardHeader style={{background:'#7DCCCC' ,color:'white'}}>Recent Title Submitted</CardHeader>
        <CardBody>
          <Table className="table table-borderless">
            <tbody>
              {dataRejected
                .filter(submission => isWithinLastWeek(submission.createdAt))
                .slice(0, 8) // Limit to the first 12 elements
                .map((submission, index) => {
                  const truncatedTitle = submission.SubmitTitleName.length > 45 ? submission.SubmitTitleName.slice(0, 45) + "..." : submission.SubmitTitleName;
                  return (
                    <tr key={index}>
                      <td style={{ width: '80vw' }}>
                        <div className="d-flex align-items-center" title={submission.SubmitTitleName} style={{ cursor: 'pointer' }}>
                          {truncatedTitle}
                        </div>
                      </td>
                      <td>{formatDate(submission.createdAt)}</td>
                    </tr>
                  );
                })}
            </tbody>
          </Table>
        </CardBody>
        <Card.Footer className="text-center bg-light">
          <a href="#" className="text-primary">View all Titles &gt;</a>
        </Card.Footer>
      </Card>
    </Container>
  );
};

export default Widget2;
