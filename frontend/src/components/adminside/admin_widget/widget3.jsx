
import React from "react";
import { Container, Row, Col, Card, CardHeader, CardBody } from "react-bootstrap";
import ChartComponent from "../components_admin/Chart";


const Widget3 = () => {
  return (
    <Container className="mb-4">
        <Card>
           <CardHeader style={{background:'#7DCCCC' ,color:'white'}}>Chart</CardHeader>
           <CardBody>
            <ChartComponent/>
            

           </CardBody>


        </Card>


        
    </Container>
  );
};

export default Widget3;
