import React, { Component } from 'react';
import { Radar } from 'react-chartjs-2';

class RadarChartWid extends Component {
    render() {
        const data = {
            labels: ['Ewan', 'Ewan', 'Ewan', 'Ewan', 'Ewan', 'Ewan', 'Ewan'],
            datasets: [
                {
                    label: 'My Radar Dataset',
                    data: [65, 59, 90, 81, 56, 55, 40],
                    fill: true,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgb(255, 99, 132)',
                    pointBackgroundColor: 'rgb(255, 99, 132)',
                    pointBorderColor: '#fff',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: 'rgb(255, 99, 132)'
                }
            ]
        };

        return (
            <div>
                <h2>Radar Chart Example</h2>
                <Radar data={data} />
            </div>
        );
    }
}


export default RadarChartWid;
