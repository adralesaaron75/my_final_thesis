import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

const CalendarWid = () => {
  // Get current date
  const currentDate = new Date();
  const currentYear = currentDate.getFullYear();
  const currentMonth = currentDate.getMonth();

  // Get the first day of the month and the total number of days in the month
  const firstDayOfMonth = new Date(currentYear, currentMonth, 1);
  const numberOfDaysInMonth = new Date(currentYear, currentMonth + 1, 0).getDate();

  // Get the index of the first day of the month
  const firstDayIndex = firstDayOfMonth.getDay();

  // Array to hold the days of the week
  const daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

  // Array to hold the weeks and days of the month
  let weeks = [];
  let days = [];

  // Fill in the days before the first day of the month
  for (let i = 0; i < firstDayIndex; i++) {
    days.push(<td key={`empty-${i}`} className="text-muted"></td>);
  }

  // Fill in the days of the month
  for (let i = 1; i <= numberOfDaysInMonth; i++) {
    days.push(<td key={i}>{i}</td>);
  }

  // Split the days into weeks
  while (days.length > 0) {
    weeks.push(<tr key={weeks.length}>{days.splice(0, 7)}</tr>);
  }

  return (
    <div className="container">
      <h2 className="text-center">{currentDate.toLocaleString('default', { month: 'long' })} {currentYear}</h2>
      <table className="table table-bordered text-center">
        <thead>
          <tr>
            {daysOfWeek.map(day => (
              <th key={day} className="text-muted">{day}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {weeks.map((week, index) => (
            <React.Fragment key={index}>
              {week}
            </React.Fragment>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default CalendarWid;
