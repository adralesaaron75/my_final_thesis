
import React from "react";
import { Container, Row, Col, Card, CardHeader, CardBody } from "react-bootstrap";
import LineChartWid from "./LineChart";
import CalendarWid from "./CalendarWid.jsx";


const Widget6 = () => {
  return (
    <Container className="mb-4">
        <Card>
           <CardHeader style={{background:'#7DCCCC' ,color:'white'}}>Line Chart</CardHeader>
           <CardBody>
            <LineChartWid/>
           

           </CardBody>


        </Card>


        
    </Container>
  );
};

export default Widget6;
