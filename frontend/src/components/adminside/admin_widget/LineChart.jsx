import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';

class LineChartWid extends Component {
    render() {
        const data = {
            labels: ['0', '1', '2', '3', '4', '5', '6'],
            datasets: [
                {
                    label: 'Submitted',
                    data: [65, 59, 80, 81, 56, 55, 40],
                    fill: false,
                    borderColor: 'rgb(75, 192, 192)',
                    tension: 0.1
                },
                {
                    label: 'Approved',
                    data: [30, 40, 20, 50, 60, 70, 80],
                    fill: false,
                    borderColor: 'rgb(255, 99, 132)',
                    tension: 0.1
                },
                {
                    label: 'Rejected',
                    data: [20, 40, 26, 14, 50, 40, 20],
                    fill: false,
                    borderColor: 'rgb(255, 243, 132)',
                    tension: 0.1
                }
            ]
        };

        const options = {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        };

        return (
            <div>
                <Line data={data} options={options} />
            </div>
        );
    }
}

export default LineChartWid;
