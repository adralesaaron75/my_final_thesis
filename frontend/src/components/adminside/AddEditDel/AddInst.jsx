import Button from "react-bootstrap/Button";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import {Container, Form} from "react-bootstrap";
import { toast } from "react-toastify";
import { VITE_BACKEND_URL } from "../../../App";
import ChipCust from "../components_admin/DropdownCategory";
import ChipDropCourse from "../components_admin/DropdownCourse";
import { FaBars, FaSignInAlt } from "react-icons/fa";
import { FaPlus, FaTimes, FaWindowClose } from 'react-icons/fa';




export default function AddModal({ userType, onChange }) {
  const [data, setData] = useState([]);
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  const [titlename, setTitlename] = useState("");
  const [contactemail, setContactemail] = useState("");
  const [authorjoin, setAuthorjoin] = useState("");
  const [referencejoin, setReferencejoin] = useState("");
  const [abstractname, setAuthorname] = useState("");
  const [category, setCategory] = useState("");
  const [publisheddate, setPublisheddate] = useState("");
  const [yearpublished, setYearpublished] = useState("");
  const [course, setCourse] = useState("");

  const [wordCount, setWordCount] = useState(0); //
  


  const [authors, setAuthors] = useState([{ id: 1, name: "Author 1" }]);

  const addAuthor = () => {
    const newId = authors.length + 1;
    setAuthors([...authors, { id: newId, name: `Author ${newId}` }]);
  };

  const removeAuthor = (id) => {
    setAuthors(authors.filter(author => author.id !== id));
  };

  const [references, setReferences] = useState([{ id: 1, name: "Reference 1 (Optional)" }]);

  const addReference = () => {
    const newId = references.length + 1;
    setReferences([...references, { id: newId, name: `Reference ${newId}` }]);
  };

  const removeReference = (id) => {
    setReferences(references.filter(reference => reference.id !== id));
  };


  

  const [image, setImage] = useState(""); // Excluded based on your request

  // Convert to base64 format
  function covertToBase64(e) {
    console.log(e);
    var reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = () => {
      console.log(reader.result); // Base64encoded string
      setImage(reader.result); // Excluded based on your request
    };
    reader.onerror = (error) => {
      console.log("Error: ", error);
    };
  }
  // End convert to base64 format


  const colorsData = [
    { label: 'Desktop Games', value: 'Desktop Games' },
    { label: 'Desktop Applications', value: 'Desktop Applications' },
    { label: 'Web Games', value: 'Web Games' },
    { label: 'Web Applications', value: 'Web Applications' },
    { label: 'Mobile Games', value: 'Mobile Games' },
    { label: 'Mobile Applications', value: 'Mobile Applications' },
    { label: 'Enterprise and E-commerce', value: 'Enterprise and E-commerce' },
    { label: 'Organization Applications', value: 'Organizational Applications' },
    { label: 'Other', value: 'Other' },
  ];

  const [selectedColors, setSelectedColors] = useState([]);

  const toggleColor = (color) => {
    setSelectedColors((prevSelected) => {
      if (prevSelected.includes(color)) {
        return prevSelected.filter((c) => c !== color);
      } else {
        return [...prevSelected, color];
      }
    });
    onChange(color);
  };

  const removeColor = (color) => {
    setSelectedColors((prevSelected) => prevSelected.filter((c) => c !== color));
  };


  const sidebarStyle = {
    width: '100%',
    backgroundColor: '#f9f9f9',
    border: '1px solid #ddd',
    borderRadius: '5px',
    boxShadow: "0px 2px 5px rgba(0, 0, 0, 0.3)",
    justifyContent: 'center',
    marginTop:'12px',
    marginBottom:'12px',
    padding:'25px'
  };



















  // Add book
  const handleSubmit = async (e) => {
    e.preventDefault();
    const errors = {}; 
    // Validation for contact email
    if (!yearpublished.trim()) {
      // Set error message for contact email
      errors.yearpublished = 'Year email is required';
    }

    if (!contactemail.trim()) {
      // Set error message for contact email
      errors.contactemail = 'Contact email is required';
    }
  
    // Validation for course
    if (!course.trim()) {
      // Set error message for course
      errors.course = 'Course is required';
    }

    if (!publisheddate.trim()) {
      // Set error message for course
      errors.publisheddate = 'Published date is required';
    }

    if (selectedColors.length === 0) {
      errors.category = 'At least one category must be selected';
    }


    setErrors(errors);
  
    // Check if there are any validation errors
    if (Object.keys(errors).length > 0) {
      // Display error messages or handle them as needed
      console.log(errors);
      return; // Prevent further execution if there are errors
    }
    
  
    // If user is not admin, proceed with form submission
    if (userType !== "admin") {
      const joinedAuthors = authors.map(author => author.name).join(" | ");
      setAuthorjoin(joinedAuthors);
      const joinedReferences = references.map(reference => reference.name).join(" | ");
      setReferencejoin(joinedReferences);
  
      try {
        const response = await fetch(`${VITE_BACKEND_URL}/api/add-book`, {
          method: "POST",
          crossDomain: true,
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({
            titlename,
            contactemail,
            authorjoin,
            abstractname,
            category: selectedColors.join(' | '),
            publisheddate,
            yearpublished,
            course,
            referencejoin,
            base64: image,
          }),
        });
  
        const data = await response.json();
  
        console.log(data, "Admin-added");
  
        if (data.status === "ok") {
          setShowFirstModal(false); // Hide the first modal
    setShowSecondModal(false); // Show the second modal
          toast.success("Add Successfully");
          setTimeout(() => {
            window.location.href = "/admin/institution";
          }, 2000);
          getAllBooks();
        } else {
          toast.error("Something went wrong");
        }
      } catch (error) {
        console.error("Error:", error);
        toast.error("Something went wrong");
      }
    } else {
      // Handle admin case (if needed)
      alert("You are not Admin");
    }
  };
  
  // End add book

  // Fetch all books
  useEffect(() => {
    getAllBooks();
  }, []);

  // Fetching all books
  const getAllBooks = async () => {
    await fetch(`${VITE_BACKEND_URL}/api/getAllBooks`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data, "bookData");
        setData(data.data);
      });
  };

  // End fetch all books

  // Add modal
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  // End add modal

  // Edit modal
  const [showedit, setShowedit] = useState(false);
  const handleCloseedit = () => setShowedit(false);
  const handleShowedit = () => setShowedit(true);
  // End edit modal

  // const handleDropdownSelect = (value) => {
  //   setCategory(value);
  // };

  


  const [showFirstModal, setShowFirstModal] = useState(false);
  const [showSecondModal, setShowSecondModal] = useState(false);
  
  const [errors, setErrors] = useState({});

  const handleShowSecondModal = () => {
    // Check if the form in the first modal is valid


   

    try {
      const errors = {};
      
      // Validate the title
      if (titlename.trim() === '') {
        errors.titlename = 'Title is required';
      } else if (titlename.trim().split(/\s+/).length < 5) {
        errors.titlename = 'Title must contain invalid';
      }
      
      // Validate the abstract
      if (abstractname.trim() === '') {
        errors.abstractname = 'Abstract is required';
      } else if (abstractname.trim().split(/\s+/).length < 150) {
        errors.abstractname = 'Abstract must contain at least 150 words';
      }
      
      // Update the errors state
      setErrors(errors);
  
      // Proceed to the next step if there are no errors
      if (Object.keys(errors).length === 0) {
        setShowFirstModal(false);
      setShowSecondModal(true);
      }
    } catch (error) {
      console.error('Validation error:', error);
      // Handle validation errors
    }


  };

  const handlePrevFirstModal = () => {
    setShowFirstModal(true); // Hide the first modal
    setShowSecondModal(false); // Show the second modal
  };

  const handleCloseSecondModal = () => setShowSecondModal(false);



  const countWords = (text) => {
    const words = text.trim().split(/\s+/);
    return words.length;
  };

  // Update word count whenever abstractname changes
  useEffect(() => {
    setWordCount(countWords(abstractname));
  }, [abstractname]);

  

  return (
        <div 
        style={{ }}
      >

      <div style={{ position: 'relative' }}>
        <span
          style={{
            position: 'absolute',
            left: '-120px', // Adjust this value to your preference
            top: '35%',
            transform: 'translateY(-50%)',
            color: '#007bff',
            fontWeight: 'bold',
            fontSize: '1rem',
          }}
        >
          Institutional
        </span>
        <button
          onClick={setShowFirstModal}
          style={{
            backgroundColor: '#007bff',
            color: 'white',
            border: 'none',
            borderRadius: '50%',
            width: '50px',
            height: '50px',
            fontSize: '1.5rem',
            cursor: 'pointer',
            boxShadow: '0px 2px 5px rgba(0, 0, 0, 0.3)',
            outline: 'none',
            marginBottom: '20px'
          }}
        >
          <FaBars />
        </button>
      </div>



          <Modal
            show={showFirstModal}
            onHide={() => setShowFirstModal(false)}
            backdrop="static"
            keyboard={false}
            size="xl"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            style={{ zIndex: 99999 }}
            
               
          >
         <Button
  onClick={() => setShowFirstModal(false)}
  
  style={{
    position: 'absolute',
    top: '-10px',
    right: '-10px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: "white",
    border: "none",
    borderRadius: "50%",
    width: "35px",
    height: "35px",
    fontSize: "1rem", // Increase the font size
    cursor: "pointer",
    boxShadow: "0px 2px 5px rgba(0, 0, 0, 0.3)",
    outline: "none",
    marginBottom: "20px",
    zIndex:'9999',
    padding:'0',
    background:"maroon"
  }}
>
  <FaWindowClose />
</Button>
<Container>
<div style={ sidebarStyle }>

            <Form onSubmit={handleSubmit}>
            <Modal.Body>
           
                <Form.Group controlId="titlename" className="mb-3" >
                  <Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }}>Title Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Title Name"
                    value={titlename}
                    onChange={(e) => setTitlename(e.target.value)}
                    style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
                    isInvalid={!!errors.titlename}
                  />
                  <Form.Control.Feedback type="invalid">{errors.titlename}</Form.Control.Feedback>
                </Form.Group>

                <Form.Group>
  <Form.Label style={{ fontFamily: "Century Schoolbook",  fontSize:'18px',  textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }}>Abstract</Form.Label>
  <Form.Control
   as="textarea"
   rows={15}
    className="form-control"
    id="exampleFormControlTextarea1"
  
    placeholder="Enter Abstract"
    value={abstractname}
    onChange={(e) => setAuthorname(e.target.value)}
   isInvalid={!!errors.abstractname}
    style={{
      fontFamily: "Franklin Gothic Book",
      // border: '1px solid #bbb',
      boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)' // Adding box shadow
    }}
  />
     <Form.Control.Feedback type="invalid">{errors.abstractname}</Form.Control.Feedback>
     <p   style={{
            position: 'absolute',
            top: '120px',
            right: '36px',
            display: 'flex',
            fontSize: '12px'
          }}>{wordCount}/150</p>
</Form.Group>


               

              
              </Modal.Body>
              <Modal.Footer >
               
                
                <Button style={{background:'maroon'}} onClick={handleShowSecondModal}>
                Next
          </Button>
              </Modal.Footer>
            </Form>
            </div>
            </Container>
            
          </Modal>








          




          <Modal show={showSecondModal} onHide={handleCloseSecondModal}  backdrop="static"
            keyboard={false}
            size="xl"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            style={{ zIndex: 99999 }}
            
            >
       
       <Button
  onClick={() => setShowSecondModal(false)}
  
  style={{
    position: 'absolute',
    top: '-10px',
    right: '-10px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: "white",
    border: "none",
    borderRadius: "50%",
    width: "35px",
    height: "35px",
    fontSize: "1rem", // Increase the font size
    cursor: "pointer",
    boxShadow: "0px 2px 5px rgba(0, 0, 0, 0.3)",
    outline: "none",
    marginBottom: "20px",
    zIndex:'9999',
    padding:'0',
    background:"maroon"
  }}
>
  <FaWindowClose />
</Button>

<Container>

<div style={ sidebarStyle }>
        <Form onSubmit={handleSubmit}>
        <Modal.Body >
          {/* Content for the second modal */}
          <div className="d-flex justify-content-between">
            <div style={{ width: "90vw", marginRight: "5px" }}>
              <Form.Group controlId="categoryname" className="mb-3">
              <Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }} >Category</Form.Label>



                <div>
                  
      <select
        className={`form-select ${errors.category ? 'is-invalid' : ''}`}
        value={''}
        onChange={(e) => toggleColor(e.target.value)}
        style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
      >
        <option value="">Select a Category</option>
        {colorsData.map((color) => (
          <option
            key={color.value}
            value={color.value}
            className={selectedColors.includes(color.value) ? 'selected' : ''}
            
          >
            {color.label}
          </option>
        ))}
      </select>

      {selectedColors.length > 0 && (
        <div className="card p-2">
          <div className="selected-colors mb-1">
            {selectedColors.map((colorValue) => {
              const selectedColor = colorsData.find(item => item.value === colorValue);
              return (
                <div
                  key={colorValue}
                  className="selected-color d-inline-block me-2 mb-2 px-2 py-1 rounded"
                  style={{  color: 'black', fontSize: '0.7rem', fontWeight: 'bold' }}
                >
                  {selectedColor.label}
                  <button
                    type="button"
                    className="btn-close ms-2"
                    aria-label="Close"
                    onClick={() => removeColor(colorValue)}
                  ></button>
                </div>
              );
            })}
          </div>
           <Form.Control.Feedback type="invalid">{errors.category}</Form.Control.Feedback>
          {selectedColors.length < 0 && (
  <Form.Group controlId="selectedItems" className="mb-3">
    <Form.Control type="text" value={selectedColors.join(' | ')} readOnly />
  

  </Form.Group>
)}
   
        </div>
      )}
    </div>













                
              </Form.Group>
            </div>
            <div style={{ width: "20vw" }}>
              <Form.Group controlId="yearpublished" className="mb-3">
              <Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }} >Year</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="yyyy"
                  value={yearpublished}
                  onChange={(e) => setYearpublished(e.target.value)}
                  style={{ width: "100%", fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)' }}
                  isInvalid={!!errors.yearpublished}
                
                />
                <Form.Control.Feedback type="invalid">{errors.yearpublished}</Form.Control.Feedback>
              </Form.Group>
            </div>
          </div>

          <div className="d-flex justify-content-between">
            <div style={{ width: "40%", marginRight: "5px" }}>
              <Form.Group controlId="Course" className="mb-3">
              <Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }}>Course</Form.Label>
              
                        <Form.Control
                          as="select" // Change input type to select for dropdown
                          className="form-select"
                          value={course}
                          onChange={(e) => setCourse(e.target.value)}
                          style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
                          isInvalid={!!errors.course}
                        >
                             <option value="">Select a Course</option>
                          <option value="BSCS">BSCS</option>
                          <option value="BSIT">
                          BSIT
                          </option>
                          <option value="BSIS">
                          BSIS
                          </option>
                        </Form.Control>

                        <Form.Control.Feedback type="invalid">{errors.course}</Form.Control.Feedback>


                
              </Form.Group>
            </div>
            <div style={{ width: "60%" }}>
              <Form.Group controlId="contactemail" className="mb-3">
              <Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }}>Contact Email</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Contact Email"
                  value={contactemail}
                  onChange={(e) => setContactemail(e.target.value)}
                  isInvalid={!!errors.contactemail}
                  
                  style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
                />
                   <Form.Control.Feedback type="invalid">{errors.contactemail}</Form.Control.Feedback>
              </Form.Group>
            </div>
          </div>

          <div className="d-flex justify-content-between">
    


          <div style={{ width: "70%",  marginRight: "5px" }}>

          <Form.Group controlId="publisheddate" className="mb-3">
          <Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }}>Upload Image</Form.Label>
            <Form.Control
              type="file"
              accept="image/*"
              
              onChange={covertToBase64}
              placeholder="No file Chosen"
              style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
            />
          </Form.Group>

            </div>

            <div style={{ width: "30%"}}>

         
<Form.Group controlId="publisheddate" className="mb-3">
<Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }}>Published Date</Form.Label>
  <Form.Control
    type="date"
    value={publisheddate}
    onChange={(e) => setPublisheddate(e.target.value)}
    isInvalid={!!errors.publisheddate}
    style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
  />
       <Form.Control.Feedback type="invalid">{errors.publisheddate}</Form.Control.Feedback>
</Form.Group>
</div>
          </div>


          {references.map((reference, index) => (
              <div key={reference.id} style={{ display: "flex", marginBottom: "10px" }}>
                <Form.Group style={{ flexGrow: 1, marginRight: "10px", marginBottom: 0}}>
                <Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }}>{reference.name}</Form.Label>
                 <Form.Control
  type="text"
  name={`reference-${reference.id}`}
  placeholder={reference.name}
  style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
  value={reference.referencejoin || ""} // Use author-specific authorjoin
  onChange={(e) => {
    const { value } = e.target;
  
    // Update the authorjoin for the specific author
    const updatedReferences = references.map(prevReference =>
      prevReference.id === reference.id ? { ...prevReference, referencejoin: value } : prevReference
    );
    setReferences(updatedReferences);
  
    // Join authorjoin of all authors with " | " separator
    const joinedReferencejoin = updatedReferences.map(reference => reference.referencejoin || "").join(" | ");
    setReferencejoin(joinedReferencejoin);
  }}
/>


                </Form.Group>
                {index === 0 ? (
                <Button className="mt-4" variant="success" style={{ borderRadius: "50%", width: "35px", height: "35px", display: "flex", justifyContent: 'center', alignItems: 'center' }} onClick={addReference}><FaPlus /></Button>

                ) : (
                  <Button className="mt-4" variant="outline-danger" style={{ borderRadius: "50%", width: "35px", height: "35px", display: "flex", justifyContent: 'center', alignItems: 'center' }} onClick={() => removeReference(reference.id)}><FaTimes /></Button>
                )}
              </div>
            ))}

            









{authors.map((author, index) => (
              <div key={author.id} style={{ display: "flex", marginBottom: "10px" }}>
                <Form.Group style={{ flexGrow: 1, marginRight: "10px", marginBottom: 0}}>
                <Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }}>{author.name}</Form.Label>
                 <Form.Control
  type="text"
  name={`author-${author.id}`}
  placeholder={author.name}
  style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
  value={author.authorjoin || ""} // Use author-specific authorjoin
  onChange={(e) => {
    const { value } = e.target;
  
    // Update the authorjoin for the specific author
    const updatedAuthors = authors.map(prevAuthor =>
      prevAuthor.id === author.id ? { ...prevAuthor, authorjoin: value } : prevAuthor
    );
    setAuthors(updatedAuthors);
  
    // Join authorjoin of all authors with " | " separator
    const joinedAuthorjoin = updatedAuthors.map(author => author.authorjoin || "").join(" | ");
    setAuthorjoin(joinedAuthorjoin);
  }}
  

  
/>


                </Form.Group>
                {index === 0 ? (
                  <Button className="mt-4" variant="success" style={{ borderRadius: "50%", width: "35px", height: "35px", display: "flex", justifyContent: 'center', alignItems: 'center' }} onClick={addAuthor}><FaPlus /></Button>
                ) : (
                  <Button className="mt-4" variant="outline-danger" style={{ borderRadius: "50%", width: "35px", height: "35px", display: "flex", justifyContent: 'center', alignItems: 'center' }} onClick={() => removeAuthor(author.id)}><FaTimes /></Button>
                )}
              </div>
            ))}



        </Modal.Body>
        <Modal.Footer >
          <Button style={{background:'maroon'}} onClick={handlePrevFirstModal}>
            Prev
          </Button>
          <Button style={{background:'maroon'}} type="submit">
                  Save
                </Button>
          {/* You can add a save button or any other action button here */}
        </Modal.Footer>
        </Form>
        </div>
       </Container>
      </Modal>
        </div>

  );
}
