import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import {Container,Row,Col,Card,Form} from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import { VITE_BACKEND_URL } from "../../../App";

import { FaBars, FaDoorClosed, FaWindowClose } from "react-icons/fa";
import Loading from "../components_admin/Loading";
import CarouselWithForm from "../components_admin/Carousek";
import axios from 'axios';
import datascraped from "../../../../../search_results.json"
import { FaEdit, FaSave, FaGreaterThan, FaLessThan } from "react-icons/fa";
import { toast } from "react-toastify";

export default function WebAdd({ userType }) {
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState(false);
  const [showCarousel, setShowCarousel] = useState(false); // State for showing Carousel
  const [searchTerm, setSearchTerm] = useState('');
  const [modal2Data, setModal2Data] = useState([]); // State to hold data for Modal 2

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setShowCarousel(true); // Show Carousel after loading
      // Load data into "Modal 2" here
      setModal2Data(datascraped);
    }, 80000);
    // 80000
  };

  const handleChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleSaveAndRun = () => {
    // Save the form value to a JSON file on the backend
    axios.post('http://localhost:3000/saveFormToJson', { searchTerm })
      .then(() => {
        // After saving, run the Node.js script
        return axios.post('http://localhost:3000/runcode', { searchTerm });
      })
      .then(response => {
        console.log(response.data);
        // Handle success of running the script, if needed
      })
      .catch(error => {
        console.error(error);
        // Handle error, if needed
      });
  };


  const scrapedDataList = datascraped;

  const [currentIndex, setCurrentIndex] = useState(0);
  const [isEditing, setIsEditing] = useState(false);
  const [editedTitle, setEditedTitle] = useState(scrapedDataList[currentIndex].title);
  const [editedLink, setEditedLink] = useState(scrapedDataList[currentIndex].link);
  const [editedAuthor, setEditedAuthor] = useState(scrapedDataList[currentIndex].author);
  
  const [editedAbstract, setEditedAbstract] = useState(scrapedDataList[currentIndex].abstract);

  const [editedYear, setEditedYear] = useState("");



  const handlePrevious = () => {
    setCurrentIndex(prevIndex => (prevIndex === 0 ? scrapedDataList.length - 1 : prevIndex - 1));
    updateFormData();
  };
  
  const handleNext = () => {
    setCurrentIndex(prevIndex => (prevIndex === scrapedDataList.length - 1 ? 0 : prevIndex + 1));
    updateFormData();
  };

  const updateFormData = () => {
    setEditedTitle(scrapedDataList[currentIndex].title);
    setEditedLink(scrapedDataList[currentIndex].link);
    setEditedAuthor(scrapedDataList[currentIndex].author);
    setEditedAbstract(scrapedDataList[currentIndex].abstract);
  };

  const handleEditClick = () => {
    setIsEditing(true);
    // Set initial values for editing
    setEditedTitle(scrapedDataList[currentIndex].title);
    setEditedLink(scrapedDataList[currentIndex].link);
    setEditedAuthor(scrapedDataList[currentIndex].author);
    setEditedAbstract(scrapedDataList[currentIndex].abstract);
  };

  useEffect(() => {
    // Update the state variables based on the current index whenever the currentIndex changes
    setEditedTitle(scrapedDataList[currentIndex]?.title || '');
    setEditedLink(scrapedDataList[currentIndex]?.link || '');
    setEditedAuthor(scrapedDataList[currentIndex]?.author || '');
    setEditedAbstract(scrapedDataList[currentIndex]?.abstract || '');
  }, [currentIndex, scrapedDataList]);
  
  

  const handleTitleChange = (e) => {
    setEditedTitle(e.target.value);
  };

  const handleLinkChange = (e) => {
    setEditedLink(e.target.value);
  };

  const handleAuthorChange = (e) => {
    setEditedAuthor(e.target.value);
  };

  const handleAbstractChange = (e) => {
    setEditedAbstract(e.target.value);
  };

  // const handleSave = () => {
  //   // Prepare the data to be sent to the backend
  //   const formData = {
  //     ScrapedTitle: editedTitle,
  //     ScrapedAuthor: editedAuthor,
  //     ScrapedLink: editedLink,
  //     ScrapedAbstract: editedAbstract
  //   };
  
  //   // Send a POST request to your backend API
  //   fetch(`${VITE_BACKEND_URL}/api/add-webscraped`, {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json"
  //     },
  //     body: JSON.stringify(formData)
  //   })
  //   .then(response => response.json())
  //   .then(data => {
  //     console.log(data);
  //     // Handle response from the backend, if needed
  //     // For example, show a success message to the user
  //     toast.success("Form data saved successfully");
  //   })
  //   .catch(error => {
  //     console.error(error);
  //     // Handle any errors that occur during the request
  //     // For example, show an error message to the user
  //     toast.error("Failed to save form data");
  //   });
  // };


  const handleSave = () => {
    // Map each item in scrapedDataList to a Promise that sends a POST request to save the data
    const savePromises = scrapedDataList.map(item => {
      // Prepare the data to be sent to the backend
      const formData = {
        ScrapedTitle: item.title,
        ScrapedAuthor: item.author,
        ScrapedLink: item.link,
        ScrapedAbstract: item.abstract,
        ScrapedYear: item.year // Include the year if available
      };
  
      // Send a POST request to save the item to the backend
      return fetch(`${VITE_BACKEND_URL}/api/add-webscraped`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(formData)
      });
    });
  
    // Execute all savePromises in parallel
    Promise.all(savePromises)
      .then(responses => {
        // Check if all requests were successful
        const allRequestsSuccessful = responses.every(response => response.ok);
        if (allRequestsSuccessful) {
          setShowCarousel(false)
          toast.success("All items saved successfully");
        } else {
          toast.error("Some items failed to save");
        }
      })
      .catch(error => {
        console.error(error);
        toast.error("Failed to save items");
      });
  };
  

  useEffect(() => {
    // Regular expression pattern to match the year (4 digits) before the author
    const yearPattern = /\b\d{4}\b(?=\s*-\s*\w+)/;
    // Extract the year from the author using the regular expression pattern
    const match = scrapedDataList[currentIndex].author.match(yearPattern);
    // If a match is found, set the extracted year to editedYear
    if (match) {
      setEditedYear(match[0]);
    } else {
      // If no match is found, set editedYear to an empty string or some default value
      setEditedYear("");
    }
  }, [currentIndex, scrapedDataList]);
  

  useEffect(() => {
    // Regular expression pattern to match the year and text following it, including ellipsis and subsequent text
    const yearAndTextPattern = /\s-\s.*$|….*$|\b\d{4}\b\s.*/; // '…' represents the ellipsis character

    // Extract the author without the year, following text, and ellipsis using the regular expression pattern
    const authorWithoutYearAndText = scrapedDataList[currentIndex]?.author.replace(yearAndTextPattern, '').trim();
    
    // Replace commas with vertical bars
    const editedAuthor = authorWithoutYearAndText.replace(/,/g, ' |');

    // Set the editedAuthor state to the author without the year, following text, and ellipsis
    setEditedAuthor(editedAuthor);
}, [currentIndex, scrapedDataList]);


  




  const sidebarStyle = {
    width: '100%',
    backgroundColor: '#f9f9f9',
    border: '1px solid #ddd',
    borderRadius: '5px',
    boxShadow: "0px 2px 5px rgba(0, 0, 0, 0.3)",
    justifyContent: 'center',
    marginTop:'12px',
    marginBottom:'12px',
    padding:'25px'
  };

  
  const sidebarStyle1 = {
    width: '100%',
    backgroundColor: '#f9f9f9',
    border: '1px solid #ddd',
    borderRadius: '5px',
    boxShadow: "0px 2px 5px rgba(0, 0, 0, 0.3)",
    justifyContent: 'center',
    marginTop:'1px',
    marginBottom:'10px',
    padding:'10px'
  };
  
  
 

  return (
    <div style={{}}>
      <div style={{ position: "relative" }}>
        <span
          style={{
            position: "absolute",
            left: "-120px",
            top: "35%",
            transform: "translateY(-50%)",
            color: "#007bff",
            fontWeight: "bold",
            fontSize: "1rem",
          }}
        >
          Web Scraped
        </span>
        <button
          onClick={handleShow}
          style={{
            backgroundColor: "#007bff",
            color: "white",
            border: "none",
            borderRadius: "50%",
            width: "50px",
            height: "50px",
            fontSize: "1.5rem",
            cursor: "pointer",
            boxShadow: "0px 2px 5px rgba(0, 0, 0, 0.3)",
            outline: "none",
            marginBottom: "20px",
          }}
        >
          <FaBars />
        </button>
      </div>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        style={{ zIndex: 99999 }}
      >
       <Container>

<div style={ sidebarStyle }>

<Button
  onClick={() => setShow(false)}
  
  style={{
    position: 'absolute',
    top: '-10px',
    right: '-10px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: "white",
    border: "none",
    borderRadius: "50%",
    width: "35px",
    height: "35px",
    fontSize: "1rem", // Increase the font size
    cursor: "pointer",
    boxShadow: "0px 2px 5px rgba(0, 0, 0, 0.3)",
    outline: "none",
    marginBottom: "20px",
    zIndex:'9999',
    padding:'0',
    background:"maroon"
  }}
>
  <FaWindowClose />
</Button>

        <Form onSubmit={handleSubmit}>
          <Modal.Body>

            
            <Form.Group controlId="titlename">
            <Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }} >Enter Web Scraped</Form.Label>
              <Form.Control
                 type="text"
                 placeholder="Enter search term"
                 value={searchTerm}
                 onChange={handleChange}
                    style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={handleSaveAndRun} type="submit" style={{background:'maroon'}}>
              Next
            </Button>
          </Modal.Footer>
        </Form>
        </div>
        </Container>
        {loading && (
          <div
            style={{
              position: "fixed",
              top: 0,
              left: 0,
              width: "100%",
              height: "100%",
              background: "rgba(255, 255, 255, 0.5)",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              zIndex: 9999,
            }}
          >
            <Loading />
          </div>
        )}
      </Modal>








      

      <Modal size="xl" show={showCarousel} onHide={() => setShowCarousel(false)}  style={{  zIndex: 99999}}>

        
        <Modal.Body  >

<Button
  onClick={() => setShowCarousel(false)}
  
  style={{
    position: 'absolute',
    top: '-10px',
    right: '-10px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: "white",
    border: "none",
    borderRadius: "50%",
    width: "35px",
    height: "35px",
    fontSize: "1rem", // Increase the font size
    cursor: "pointer",
    boxShadow: "0px 2px 5px rgba(0, 0, 0, 0.3)",
    outline: "none",
    marginBottom: "20px",
    zIndex:'9999',
    padding:'0',
    background:"maroon"
  }}
>
  <FaWindowClose />
</Button>

      
      {/* <Container className="justify-content-center align-items-center"> */}
     
      <div style={ sidebarStyle1 }>
          <Card className="shadow-lg" >
          
            <Card.Body style={{ backgroundColor: '#f9f9f9',  boxShadow: "0px 0px 5px rgba(0, 0, 0, 0.5)"}}>
            
           
            
              <Form className='mt-2'>
              <Form.Group controlId="formTitle" className='mb-3'>
              <Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }} >Title</Form.Label>
  <Form.Control
    type="text"
    name="author"
    placeholder="Enter Title"
    value={editedTitle}
    onChange={handleTitleChange}
    style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
  />
</Form.Group>

<Form.Group controlId="formLink" className='mb-3'>
<Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }} >Link</Form.Label>
  <Form.Control
    type="text"
    name="link"
    placeholder="Enter link"
    value={editedLink}
    onChange={handleLinkChange}
    style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
  />
</Form.Group>

<Form.Group controlId="formAuthor" className='mb-3'>
<Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }} >Author</Form.Label>
  <Form.Control
    type="text"
    name="author"
    placeholder="Enter author"
    value={editedAuthor}
    onChange={handleAuthorChange}
    style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
  />
</Form.Group>

<Form.Group controlId="formYear" className='mb-3'>
<Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }} >Year</Form.Label>
  <Form.Control
    type="text"
    name="year"
    placeholder="Enter Year"
    value={editedYear}
    readOnly 
    style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
    
  />
</Form.Group>


<Form.Group controlId="formAbstract">
<Form.Label  style={{fontFamily:"Century Schoolbook", fontSize:'18px', textShadow: '-6px 3px 8px rgba(0, 0, 0, 0.3)' }} >Abstract</Form.Label>
  <Form.Control
    as="textarea"
    rows={15}
    value={editedAbstract}
    onChange={handleAbstractChange}
    style={{fontFamily:"Franklin Gothic Book", boxShadow: '0 2px 10px rgba(0, 0, 0, 0.2)'}}
  />
</Form.Group>

              </Form>   
              <div className="text-end mt-4">
                <Button variant="danger" >Reject</Button>{' '}
                <Button variant="success" onClick={handleSave}>Save</Button>
              </div>

           
            </Card.Body>
          </Card>
          </div>
       
      {/* </Container> */}



      <Container className="justify-content-center mt-4">
  <Col className="text-center">
    <Button variant="link" onClick={handlePrevious} style={{ marginRight: '10px' }}>
      <FaLessThan />
    </Button>
    {scrapedDataList.map((data, index) => (
      <div
        key={index}
        className="d-inline-block me-3"
        onClick={() => setCurrentIndex(index)}
        style={{ cursor: 'pointer' }}
      >
        <div
          style={{
            width: '8px',
            height: '8px',
            backgroundColor: index === currentIndex ? 'maroon' : 'gray',
            borderRadius: '50%',
            display: 'inline-block',
          }}
        />
      </div>
    ))}
    <Button variant="link" onClick={handleNext} style={{ marginLeft: '10px' }}>
      <FaGreaterThan />
    </Button>
  </Col>
</Container>


    
        </Modal.Body>
      </Modal>
    </div>
  );
}
