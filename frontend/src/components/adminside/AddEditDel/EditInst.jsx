import Button from "react-bootstrap/Button";
import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import { toast } from "react-toastify";
import axios from "axios";
import { VITE_BACKEND_URL } from "../../../App";
import { FaEdit, FaSignInAlt } from "react-icons/fa";
import { CDBBtn } from "cdbreact";

export default function EditModal({ userType }) {
  const [data, setData] = useState([]);
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  const [titlename, setTitlename] = useState("");
  const [contactemail, setContactemail] = useState("");
  const [authorjoin, setAuthorjoin] = useState("");
  const [abstractname, setAuthorname] = useState("");
  const [category, setCategory] = useState("");
  const [publisheddate, setPublisheddate] = useState("");
  const [yearpublished, setYearpublished] = useState("");

  useEffect(() => {
    getAllBooks();
  }, []);

  // Fetching all books
  const getAllBooks = async () => {
    await fetch(`${VITE_BACKEND_URL}/api/getAllBooks`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data, "bookData");
        setData(data.data);
      });
  };
  

  // Edit modal
  const [showedit, setShowedit] = useState(false);
  const handleCloseedit = () => setShowedit(false);
  const handleShowedit = () => setShowedit(true);
  // End edit modal

  const [isLoading, setIsLoading] = useState(false);
  const [book, setBook] = useState({
    titlename,
    contactemail,
    authorjoin,
    abstractname,
    category,
    publisheddate,
    yearpublished,
    id: "",
  });

  const getSinglBook = async (
    id,
    titlename,
    contactemail,
    authorjoin,
    abstractname,
    category,
    publisheddate,
    yearpublished
  ) => {
    setIsLoading(true);
    try {
      await axios.get(
        `${VITE_BACKEND_URL}/api/get-singlebook/${
          (id,
          titlename,
          contactemail,
          authorjoin,
          abstractname,
          category,
          publisheddate,
          yearpublished)
        }`
      );
      setBook({
        titlename: titlename,
        contactemail: contactemail,
        authorjoin: authorjoin,
        abstractname: abstractname,
        category: category,
        publisheddate: publisheddate,
        yearpublished: yearpublished,
        id: id,
        // image: response.data.image,
      });
      getSinglBook();
      setIsLoading(false);

      if (data.status == "ok") {
        toast.success("get book successfully");
        setTimeout(() => {
          window.location.href = "/admin/institutional";
        }, 2000);
      }
    } catch (error) {
      toast.error(error.message);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getSinglBook();
  }, []);

  const updateBook = async (e) => {
    if (userType == "admin") {
      e.preventDefault();
      alert("You are not Admin");
    } else {
      e.preventDefault();
      const id = e.target[0].value;
      const titlename = e.target[1].value;
      const contactemail = e.target[2].value;
      const authorjoin = e.target[3].value;
      const abstractname = e.target[4].value;
      const category = e.target[5].value;
      const publisheddate = e.target[6].value;
      const yearpublished = e.target[7].value;

      await fetch(`${VITE_BACKEND_URL}/api/updatebook/${id}`, {
        method: "PUT",
        crossDomain: true,
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          titlename,
          contactemail,
          authorjoin,
          abstractname,
          category,
          publisheddate,
          yearpublished,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data, "Update-book");
          if (data.status == "ok") {
            toast.success("Update book successfully");
            setTimeout(() => {
              window.location.href = "/admin/institutional";
            }, 2000);
            // getAllBooks();
          } else {
            toast.error("Something went wrong");
          }
        });
    }
  };

  return (
    <>
      <Button
        onClick={() => {
          getSinglBook(
            i._id,
            i.titlename,
            i.contactemail,
            i.authorjoin,
            i.abstractname,
            i.category,
            i.publisheddate,
            i.yearpublished
          );
          handleShowedit();
        }}
        variant="primary"
        size="sm"
        style={{ marginRight: "5px" ,fontSize: "0.7rem" }}
      >
        <FaEdit style={{ fontSize: "0.7rem" }} />
      </Button>


      <Modal
        show={showedit}
        onHide={handleCloseedit}
        backdrop="static"
        keyboard={false}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header
          closeButton
          style={{
            backgroundColor: "maroon",
            backgroundImage:
              "linear-gradient(to bottom, rgba(25,25,25,0.6), rgba(0,0,0,0))",
          }}
        >
          <Modal.Title style={{ color: "white" }}> Edit</Modal.Title>
        </Modal.Header>
        <Form onSubmit={updateBook}>
          <Modal.Body style={{ backgroundColor: "#800000" }}>
            <Form.Group controlId="titlename">
              <Form.Label style={{ color: "white" }}>Title Name</Form.Label>
              <input
                type="text"
                style={{ display: "none" }}
                name="id"
                onChange={(e) => setBook({ ...book, name: e.target.value })}
                value={book.id}
              />
              <Form.Control
                type="text"
                placeholder="Enter Title Name"
                value={book.titlename}
                onChange={(e) => setBook({ ...book, name: e.target.value })}
                required
              />
            </Form.Group>
            <Form.Group
                      className="mb-3"
                      controlId="exampleForm.ControlInput1"
                    >
                      <Form.Label>Book Number/Code:</Form.Label>
                      <Form.Control
                        type="text"
                        value={book.contactemail}
                        onChange={(e) =>
                          setBook({ ...book, name: e.target.value })
                        }
                        placeholder="Enter Number or Code"
                      />
                    </Form.Group>

          </Modal.Body>
          <Modal.Footer
            style={{
              backgroundColor: "maroon",
              backgroundImage:
                "linear-gradient(to Top, rgba(25,25,25,0.6), rgba(0,0,0,0))",
            }}
          >
            <Button variant="secondary" onClick={handleCloseedit}>
              Close
            </Button>
            <Button variant="primary" type="submit">
              Update
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}
