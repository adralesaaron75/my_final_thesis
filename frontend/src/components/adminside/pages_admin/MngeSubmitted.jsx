
import {CDBBox, CDBCard, CDBCardBody, CDBContainer } from "cdbreact";
import { Link } from "react-router-dom";
import {
  CDBSidebar
} from "cdbreact";

import SidebarLayout from "../components_admin/SideBarLayout";
import { Container, Form, Row, Col, Card, Table, Modal, Button, Nav } from "react-bootstrap";
import React, { useState,useEffect } from 'react';
import { toast } from "react-toastify";
import { VITE_BACKEND_URL } from "../../../App";
import axios from "axios";
import Badge from "react-bootstrap/esm/Badge";
import ManageNav from "./MngeNav";
import { FaSearch,FaFilter  } from 'react-icons/fa'; // Import the search icon component
import { GrSort } from "react-icons/gr";





export default function ManageSubmitted({ userType  }) {
  // export default function Students({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  const categoryoptions= [
    { label: 'Mobile Game, Game Development and Virtual Reality', value: 'Mobile Game, Game Development and Virtual Reality' },
    { label: 'Information Retrieval and Search Engines', value: 'Information Retrieval and Search Engines' },
    { label: 'Artificial Intelligence and Machine Learning', value: 'Artificial Intelligence and Machine Learning' },
    { label: 'E-commerce and Digital Marketing', value: 'E-commerce and Digital Marketing' },
    { label: 'Social Network Analysis and Mining ', value: 'Social Network Analysis and Mining' },
    { label: 'Health Informatics', value: 'Health Informatics' },
    { label: 'Natural Language Processing (NLP)', value: 'Natural Language Processing (NLP)' },
    { label: 'Geographic Information Systems (GIS)', value: 'Geographic Information Systems (GIS)' },
    { label: 'Educational Technology', value: 'Educational Technology' },
    { label: 'Robotic Systems and Automation', value: 'Robotic Systems and Automation' },
    { label: 'Desktop Games and Applications', value: 'Desktop Games and Applications' },
    { label: 'Web Games and Applications', value: 'Web Games and Applications' },
    { label: 'Mobile Games and Applications', value: 'Mobile Games and Applications' },
    { label: 'Enterprise and E-commerce', value: 'Enterprise and E-commerce' },
    { label: 'Organization Applications', value: 'Organizational Applications' },
  ];




  const [showModal, setShowModal] = useState(false);
  const [showModalRecommend, setShowModalRecommend] = useState(false);
  const [showSearch, setShowSearch] = useState(false); 

  const [selectedCategory, setSelectedCategory] = useState(""); // State variable for selected category
  const [selectedCourse, setSelectedCourse] = useState(""); // State variable for selected course
  const [selectedFromDate, setSelectedFromDate] = useState(""); // State variable for selected from date
  const [selectedToDate, setSelectedToDate] = useState(""); 

  const [showFilter, setShowFilter] = useState(false); // State variable to manage filter visibility


  const [sortByTitle, setSortByTitle] = useState(false);
  const [sortByCategory, setSortByCategory] = useState(false);
  const [sortByCourse, setSortByCourse] = useState(false);

  const toggleSearch = () => {
    setShowSearch(!showSearch);
  };



  const handleCloseModal = () => {
    setShowModal(false);
  };




    const [data, setData] = useState([]); // Define data state
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        fetchUserData();
        fetchBorrowedBooks();
    }, []);

    const fetchUserData = () => {
        // Your fetch userData logic
    }

    const fetchBorrowedBooks = async () => {
        try {
            setIsLoading(true);
            const id = getId();
            const response = await axios.get(`${VITE_BACKEND_URL}/api/borrowedbooklists/${id}`);
            setIsLoading(false);
            if (response.data.success) {
                setData(response.data.data);
            }
        } catch (error) {
            setIsLoading(false);
            console.error('Error fetching borrowed books:', error);
        }
    }

    const getId = () => {
      if (fetchBorrowedBooks.length > 0) {
        return fetchBorrowedBooks[0]._id;
      } else {
        // Handle the case when the array is empty
        return null; // or return some default value
      }
    };
    




    const [selectedSubmitTitleName, setSelectedSubmitTitleName] = useState('');
    const [selectedSubmitAbsract, setSelectedSubmitAbsract] = useState('');
    const [selectedSubmitAuthor, setSelectedSubmitAuthor] = useState('');
    const [selectedSubmitCategory, setSelectedSubmitCategory] = useState('');
    const [selectedSubmitYear, setSelectedSubmitYear] = useState('');
    const [selectedSubmitCourse, setSelectedSubmitCourse] = useState('');
    const [selectedSubmitSubmittedby, setSelectedSubmitSubmittedby] = useState('');
    
    const [selectedSubmitId, setSelectedSubmitId] = useState('');
    const [selectedStatus, setselectedStatus] = useState('');
    const [selectedrecommendation, setselectedrecommendation] = useState('');
    const [selecteddatecreated, setSelecteddatecreated] = useState('');

    // Inside the handleDivClick function:
    const handleDivClick = (submitTitleName, submitAbstract, id, status, recommendation ,createdAt , course, Category, Year, Submittedby, Author) => {
        setSelectedSubmitTitleName(submitTitleName); // Set the selected SubmitTitleName
        setSelectedSubmitAbsract(submitAbstract); 
        setSelectedSubmitCourse(course);
        setSelectedSubmitAuthor(Author);
        setSelectedSubmitCategory(Category);
        setSelectedSubmitYear(Year);
        setSelectedSubmitSubmittedby(Submittedby);
        setSelectedSubmitId(id); // Set the selected _id
        setselectedStatus(status);
        setselectedrecommendation(recommendation);
        setSelecteddatecreated(createdAt);
        setShowModal(true); // Show the modal
        
    };



    const handleSubmit = async (e) => {
      if (userType === "admin") {
          e.preventDefault();
          alert("You are not Admin");
      } else {
          e.preventDefault();
          // Default course to "n/a" if selectedSubmitAuthor is null
          const course = selectedSubmitCourse || "n/a";
          const category = selectedSubmitCategory || "n/a";
          const yearpublished = selectedSubmitYear || "n/a";
          const authorjoin = selectedSubmitAuthor || "n/a";
          

          
  
          await fetch(`${VITE_BACKEND_URL}/api/add-book`, {
              method: "POST",
              crossDomain: true,
              headers: {
                  "Content-Type": "application/json",
                  Accept: "application/json",
                  "Access-Control-Allow-Origin": "*",
              },
              body: JSON.stringify({
                  titlename: selectedSubmitTitleName,
                  abstractname: selectedSubmitAbsract,
                  course: course,
                  category: category,
                  contactemail: "n/a",
                  authorjoin: authorjoin,
                  publisheddate: "n/a",
                  yearpublished: yearpublished,
                  // base64: image, // Excluded based on your request
              }),
          })
          .then((res) => res.json())
          .then((data) => {
              handleUpdate();
              handleSaveApproved();
              console.log(data, "Admin-added");
              if (data.status === "ok") {
                  toast.success("Add Successfully");
                  setTimeout(() => {
                      // window.location.href = "/admin/institution";
                  }, 2000);
                  getAllBooks();
              } else {
                  toast.error("Something went wrong");
              }
          });
      }
  };
  

    const handleSaveRejected = () => {
      // Prepare the data to be sent to the backend
      const course = selectedSubmitCourse || "n/a";
      const category = selectedSubmitCategory || "n/a";
      const yearpublished = selectedSubmitYear || "n/a";
      const formData = {
        SubmitTitleName: selectedSubmitTitleName,
        SubmitAbstract: selectedSubmitAbsract,
        recommendation: selectedrecommendation,
        course: course,
        category: category,
        contactemail: "n/a",
        authorjoin: "n/a",
        publisheddate: "n/a",
        yearpublished: yearpublished,
        // base64: image, // Excluded based on your request

      };
    
      // Send a POST request to your backend API
      fetch(`${VITE_BACKEND_URL}/api/add-submissionrejected`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(formData)
      })
      .then(response => response.json())
      .then(data => {
        handleUpdateRejected();
        console.log(data);
        // Handle response from the backend, if needed
        // For example, show a success message to the user
        toast.success("Rejected saved successfully");
      })
      .catch(error => {
        console.error(error);
        // Handle any errors that occur during the request
        // For example, show an error message to the user
        toast.error("Failed to save form data");
      });
    };

    const handleSaveApproved = () => {
      // Prepare the data to be sent to the backend
      const course = selectedSubmitCourse || "n/a";
      const category = selectedSubmitCategory || "n/a";
      const yearpublished = selectedSubmitYear || "n/a";
      const authorjoin = selectedSubmitAuthor || "n/a";

      const formData = {
        SubmitTitleName: selectedSubmitTitleName,
        SubmitAbstract: selectedSubmitAbsract,
          course: course,
                  category: category,
                  contactemail: "n/a",
                  authorjoin: authorjoin,
                  publisheddate: "n/a",
                  yearpublished: yearpublished,
                  // base64: image, // Excluded based on your request
        

      };
    
      // Send a POST request to your backend API
      fetch(`${VITE_BACKEND_URL}/api/add-submissionapproved`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(formData)
      })
      .then(response => response.json())
      .then(data => {
        console.log(data);
        // Handle response from the backend, if needed
        // For example, show a success message to the user
        // toast.success("Approved saved successfully");
      })
      .catch(error => {
        console.error(error);
        // Handle any errors that occur during the request
        // For example, show an error message to the user
        toast.error("Failed to save form data");
      });
    };

   


    const handleOppened = async () => {
      try {
    
        const formData = {
          OpennedorNot:"Oppened"
        };
    
        // Send a PUT request to update the submission
        const response = await axios.put(`${VITE_BACKEND_URL}/api/updateborrowedbook/${selectedSubmitId}`, formData);
        
        // Check if the update was successful
        if (response.data.status === 'ok') {
         
         
        }
      } catch (error) {
        console.error('Error updating submission:', error);
        
      }
    };



    const handleUpdate = async () => {
      try {
        // Check if both title name and abstract are not empty
        if (selectedSubmitTitleName.trim() === '' || selectedSubmitAbsract.trim() === '') {
          toast.error('Title and abstract cannot be empty');
          return;
        }
    
        // Prepare the data to be sent to the backend
        const formData = {
          SubmitTitleName: selectedSubmitTitleName,
          SubmitAbstract: selectedSubmitAbsract,
          status: "approved",
          OpennedorNot:"Oppened"
          
         
        };
    
        // Send a PUT request to update the submission
        const response = await axios.put(`${VITE_BACKEND_URL}/api/updateborrowedbook/${selectedSubmitId}`, formData);
        
        // Check if the update was successful
        if (response.data.status === 'ok') {
          // toast.success('Submission updated successfully');
          // Optionally, you can refresh the data after update
          fetchBorrowedBooks();
          // Close the modal after successful update
          handleCloseModal();
        } else {
          toast.error('Failed to update submission');
        }
      } catch (error) {
        console.error('Error updating submission:', error);
        toast.error('Failed to update submission');
      }
    };

    const handleUpdateRejected = async () => {
      try {
        // Check if both title name and abstract are not empty
        if (selectedSubmitTitleName.trim() === '' || selectedSubmitAbsract.trim() === '') {
          toast.error('Title and abstract cannot be empty');
          return;
        }
    
        // Prepare the data to be sent to the backend
        const formData = {
          SubmitTitleName: selectedSubmitTitleName,
          SubmitAbstract: selectedSubmitAbsract,
          status: "rejected",
          recommendation: selectedrecommendation,
        };
    
        // Send a PUT request to update the submission
        const response = await axios.put(`${VITE_BACKEND_URL}/api/updateborrowedbook/${selectedSubmitId}`, formData);
        
        // Check if the update was successful
        if (response.data.status === 'ok') {
          // toast.success('Submission updated successfully');
          // Optionally, you can refresh the data after update
          fetchBorrowedBooks();
          // Close the modal after successful update
          handleCloseModal();
        } else {
          toast.error('Failed to update submission');
        }
      } catch (error) {
        console.error('Error updating submission:', error);
        toast.error('Failed to update submission');
      }
    };

    const [isBackgroundDark, setIsBackgroundDark] = useState(false); // New state for background color

    const handleBackgroundClick = () => {
      setIsBackgroundDark(false); // Close background when clicked
      // setIsClicked(false); // Close button when clicked outside
    };


    const calculateDaysDifference = (dateString) => {
      const createdAtDate = new Date(dateString);
      const currentDate = new Date();
      const differenceInTime = currentDate.getTime() - createdAtDate.getTime();
      const differenceInDays = Math.ceil(differenceInTime / (1000 * 3600 * 24));
      return differenceInDays;
  };


  
  const [showDrawer, setShowDrawer] = useState(false);

    useEffect(() => {
    const handleScroll = () => {
      setShowDrawer(false);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const removeCategory = () => {
    setSelectedCategory("");
  };

  // Function to clear selected course
  const removeCourse = () => {
    setSelectedCourse("");
  };

  // Function to clear selected date range
  const clearDate = () => {
    setSelectedFromDate("");
    setSelectedToDate("");
  };

    

    
    
    
    
  const author = selectedSubmitAuthor.split("|").map(part => (
    <span style={{ color: "black", fontSize: "12px" }}>{part}</span>
  ));
  
  const authorWithLineBreaks = [];
  author.forEach((part, index) => {
    authorWithLineBreaks.push(part);
    if (index < author.length - 1) {
      authorWithLineBreaks.push(<br key={index} />);
    }
  });



 

  return (
    <div
      style={{ display: "flex", height: "auto", overflow: "scroll initial" }}
    >
      <CDBBox display="flex" alignContent="start">
        <CDBSidebar textColor="#fff" backgroundColor="#800000" style={{height:'100%'}}>
          <SidebarLayout />
        </CDBSidebar>
      </CDBBox>

      <CDBBox
        // display="flex"
        justifyContent="center"
        style={{
          width: "100%",
          height: "100%",
        }}
      >
       {/* Name: {userData.name} */}





<div style={{ backgroundColor: '#f8f9fa', padding: '20px', borderRadius: '8px', boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)' }}>
<div style={{ marginBottom: '20px' }}>
        {/* Account page navigation */}
       <ManageNav/>

       <div style={{ display: 'flex', marginTop: '5px', justifyContent: 'end', alignItems: 'center' }}> {/* Added alignItems */}
  <FaSearch className="my-3" style={{ cursor: 'pointer', marginRight: '5px' }} onClick={toggleSearch} />  {showSearch ? ( null ) : <text>Search</text>}
  
  {showSearch && (
    <Form.Group controlId="search" className="my-3" style={{ margin: '0' }}> {/* Added margin: 0 to remove default margin */}
      <Form.Control type="text" placeholder="Search" />
    </Form.Group>
  )}

<div className="d-flex align-items-end">
              <div className="mr-3" style={{marginRight:'25px', marginLeft:'25px'}}>
                <FaFilter onClick={() => setShowFilter(!showFilter)} /> Filter {/* Toggle filter visibility */}
              </div>
              <div  onClick={() => setShowDrawer(!showDrawer)}>
        <GrSort /> Sort
      </div>

      
            </div>

</div>

{showFilter && ( // Show filter card if showFilter is true
            <CDBCard className="mt-2">
              <CDBCardBody>
                {selectedCategory || selectedCourse || selectedFromDate || selectedToDate ? (
                  <div className="mt-3" style={{marginLeft:'25px'}}>
                    Keyword: 
                    {selectedCategory ?(
                      <div
                        className="selected-color d-inline-block me-2 mb-2 px-2 py-1 rounded"
                        style={{ color: 'black', fontSize: '0.7rem', fontWeight: 'bold' }}
                      >
                        {selectedCategory}
                        <button
                          type="button"
                          className="btn-close ms-2"
                          aria-label="Close"
                          onClick={() => removeCategory()}
                        ></button>
                      </div>
                    ):null}
                    {selectedCourse ?(
                      <div
                        className="selected-color d-inline-block me-2 mb-2 px-2 py-1 rounded"
                        style={{ color: 'black', fontSize: '0.7rem', fontWeight: 'bold' }}
                      >
                        {selectedCourse}
                        <button
                          type="button"
                          className="btn-close ms-2"
                          aria-label="Close"
                          onClick={() => removeCourse()}
                        ></button>
                      </div>
                    ):null}
                    {(selectedFromDate && selectedToDate) ? (
                      <div
                        className="selected-color d-inline-block me-2 mb-2 px-2 py-1 rounded"
                        style={{ color: 'black', fontSize: '0.7rem', fontWeight: 'bold' }}
                      >
                        {selectedFromDate} To {selectedToDate}
                        <button
                          type="button"
                          className="btn-close ms-2"
                          aria-label="Close"
                          onClick={() => clearDate()}
                        ></button>
                      </div>
                    ) : null}

                    <hr
                      className="d-flex justify-content-center"
                      style={{
                        width: "90%",
                        borderTop: "1px solid #000000",
                        margin: "auto",
                        marginTop: "10px",
                        marginBottom: "10px",
                      }}
                    />
                  </div>
                ) : null}

                <Container>
                  <Row className="justify-content-center mt-3 mb-3"> {/* Added justify-content-center class */}
                    <Col md={3}>
                      <Row>
                        <Col>
                          <Form.Group className="mb-3">
                            <Form.Label>From:</Form.Label>
                            <Form.Control type="date"   onChange={(e) => setSelectedFromDate(e.target.value)}/>
                          </Form.Group>
                        </Col>
                        <Col>
                          <Form.Group className="mb-3">
                            <Form.Label>To:</Form.Label>
                            <Form.Control type="date"  onChange={(e) => setSelectedToDate(e.target.value)}/>
                          </Form.Group>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={3}>
                      <Form.Group className="mb-3">
                        <Form.Label>Category:</Form.Label>
                        <Form.Control as="select" className="form-select" value={selectedCategory} onChange={(e) => setSelectedCategory(e.target.value)}>
                          {categoryoptions.map((option) => (
                            <option key={option.value} value={option.value}>{option.label}</option>
                          ))}
                        </Form.Control>
                      </Form.Group>
                    </Col>
                    <Col md={3}>
                      <Form.Group className="mb-3">
                        <Form.Label>Course:</Form.Label>
                        <Form.Control as="select" className="form-select" value={selectedCourse} onChange={(e) => setSelectedCourse(e.target.value)}>
                          <option value="BSCS">BSCS</option>
                          <option value="BSIT">BSIT</option>
                          <option value="BSIS">BSIS</option>
                        </Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>    
                  <Row className="justify-content-end mb-4">
                    <Col md={3}>
                      <Button variant="primary" >Apply Filters</Button> 
                    </Col>
                  </Row>
                </Container>
              </CDBCardBody>
            </CDBCard>
          )}

{showDrawer && (
                <div
                  style={{
                    position: "fixed",
                    top: 185,
                    right: 25,
                    zIndex: 999,
                    filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.99))'
                  }}
                >
                   <div
                    style={{
                      position: "relative",
                      background: "#f8f9fa",
                      boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
                      borderRadius: "5px",
                      width: "15vw",
                    }}
                  >
                    <Container className="p-3">
                    <Form.Check
      type="radio"
      name="sortOption"
      label="Sort by Title"
      checked={sortByTitle}
      onChange={() => {
        setSortByTitle(true);
        setSortByCategory(false);
        setSortByCourse(false);
      }}
    />
    {/* Radio button for category sorting */}
    <Form.Check
      type="radio"
      name="sortOption"
      label="Sort by Category"
      checked={sortByCategory}
      onChange={() => {
        setSortByTitle(false);
        setSortByCategory(true);
        setSortByCourse(false);
      }}
    />
    <Form.Check
      type="radio"
      name="sortOption"
      label="Sort by Course"
      checked={sortByCourse}
      onChange={() => {
        setSortByTitle(false);
        setSortByCategory(false);
        setSortByCourse(true);
      }}
    />
    </Container>
                 
                  </div>
                </div>
)}

          






      </div>
      <hr className="my-4" />


    
      {data
  .sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt)) // Sort by createdAt from newest to oldest
  .map((i, index) => (
    <div 
      key={index} 
      onClick={() => {
        handleOppened() // Call handleOppened
        handleDivClick(
          i.SubmitTitleName, 
          i.SubmitAbstract, 
          i._id, 
          i.status, 
          i.recommendation, 
          i.createdAt,  
          i.course, 
          i.category, 
          i.YearPublished, 
          i.Submittedby, 
          i.SubmitAuthor, 
          i.OpennedorNot
        ); // Call handleDivClick
      }} 
      style={{ marginBottom: '10px' }}
    >

      <Container fluid>
        <Card className="mt-2" style={{ filter: "drop-shadow(0 9px 8px rgba(0, 0, 0, 0.15))" }}>
          {i.OpennedorNot === "New" && (
            <div style={{ position: 'absolute', top: 0, left: 0 }}>
              <Badge bg="warning" style={{ minWidth: 'auto', fontSize: '10px' }}>{i.OpennedorNot}</Badge>
            </div>
          )}
          <Card.Body style={{ paddingTop: '7px', paddingBottom: '7px' }}>
            <Row>
              <Col md={1} className="d-flex justify-content-center align-items-center">
                <Form.Group controlId="formCheckbox" className="m-0">
                  <Form.Check
                    type="checkbox"
                    style={{ fontSize: '17px', filter: 'drop-shadow(3px 2px 1px rgba(0, 0, 0, 0.3))' }}
                  />
                </Form.Group>
              </Col>

              <Col md={2}>
                <div style={{ width: '100%', height: '3rem', overflow: 'hidden', textOverflow: 'ellipsis', fontSize: '12px', paddingBottom: 'auto', paddingTop: '5px', display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
                  <div style={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>
                    <h6 style={{ display: 'inline-block', marginBottom: 0 }}>{i.SubmittedName}</h6>
                  </div>
                  <div style={{ color: 'gray', overflow: 'hidden', textOverflow: 'ellipsis' }}>
                    {i.Submittedby}
                  </div>
                </div>
              </Col>

              <Col md={9}>
                <div
                  style={{ width: '100%', height: '2.7rem', overflow: 'hidden', textOverflow: 'ellipsis', fontSize: '12px', paddingBottom: 'auto', paddingTop: '5px' }}
                >
                  <h6 style={{ display: 'inline-block', marginBottom: 0, fontWeight: 'bold' }}>{i.SubmitTitleName}</h6>
                  <span style={{ color: 'gray', overflow: 'hidden', textOverflow: 'ellipsis' }}> - {i.SubmitAbstract}</span>

                </div>
              </Col>
            </Row>
          </Card.Body>
        </Card>
      </Container>
    </div>
  ))}






    </div>







       

       {/* <Modal show={showModal} size="xl" onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title></Modal.Title>
        </Modal.Header>
        <Modal.Body>

              <Form className='mt-2'>
                <Form.Group controlId="formTitle">
                  <Form.Label>Title</Form.Label>
                
                    <Form.Control
                      type="text"
                      name="author"
                      placeholder="Enter Title"
                      value={selectedSubmitTitleName}
                      onChange={(e) => setSelectedSubmitTitleName(e.target.value)}

                    />
                
                </Form.Group>


                <Form.Group controlId="formAbstract">
                  <Form.Label>Abstract</Form.Label>
               
                    <Form.Control
                      as="textarea"
                      rows={15}
                      value={selectedSubmitAbsract}
                      onChange={(e) => setSelectedSubmitAbsract(e.target.value)}
                    />
                
                   
                 
                </Form.Group>
              </Form>   
              <div className="text-end mt-4">
                <Button variant="danger" onClick={() => setShowModalRecommend(true)} >Reject</Button>{' '}
               <Button variant="success" onClick={handleSubmit}>Approved</Button>
              </div>


        </Modal.Body>
      </Modal> */}


      {showModalRecommend && (
      <div
          onClick={handleBackgroundClick}
         style={{
           position: "fixed",
           top: 0,
           left: 0,
           width: "100%",
           height: "100%",
           background: "rgba(1, 2, 4, 0.5)",
           display: "flex",
           alignItems: "center",
           justifyContent: "center",
           zIndex: 9999,
         }}
       >


      <Modal show={showModalRecommend} size="lg" onHide={() => setShowModalRecommend(false)} style={{zIndex:99999}}>
        <Modal.Header closeButton>
          <Modal.Title></Modal.Title>
        </Modal.Header>
        <Modal.Body>

              <Form className='mt-2'>
         



                <Form.Group controlId="formAbstract">
                  <Form.Label>Comments and Suggestions</Form.Label>
               
                    <Form.Control
                      as="textarea"
                      rows={12}
                      value={selectedrecommendation}
                      onChange={(e) => setselectedrecommendation(e.target.value)}
                    />
                
                   
                 
                </Form.Group>
             
              </Form>   
              <div className="text-end mt-4">
               
               <Button variant="success" onClick={handleSaveRejected}>Submit</Button>
              </div>


        </Modal.Body>
      </Modal>


      </div>
      )}


<Modal size="xl" show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title style={{width:'92%'}}></Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* <p>{content}</p> */}
          <Container>
  <Row>
  <Col md={2}>
  <div style={{    width: '9vw', 
      height: '25vh', 
      backgroundColor: '#ddd', borderRadius: '4px' }}>
    {/* Placeholder content */}
    
  </div>
</Col>

    <Col style={{ background: 'white' }}>
    <Row >
                  <Col md={1}>
                    <p
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                        fontSize:'12px',
                      }}
                    >
                      Title:
                    </p>
                  </Col>
                  <Col>
                    <p>
                      <span style={{ color: "black", fontSize: "17px" }}>
                        {selectedSubmitTitleName}
                        
                      </span>
                    </p>
                  </Col>
                </Row>
               
                <Row>
                  <Col md={1} >
                    <p
                      style={{
                        color: "black",
                        fontSize:'12px',
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Author:
                    </p>
                  </Col>
                  <Col xs={3} md={11}>
                    <p>
                      <span style={{ color: "black", fontSize: "12px" }}>
                        {authorWithLineBreaks}
                      
                    
                     
                       
                      </span>
                    </p>
                  </Col>
                </Row>

                <Row>
                  <Col md={1} >
                    <p
                      style={{
                        color: "black",
                        fontSize:'12px',
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Course:
                    </p>
                  </Col>
                  <Col xs={3} md={11}>
                    <p>
                      <span style={{ color: "black", fontSize: "12px" }}>
                        {selectedSubmitCourse}
                    
                      </span>
                    </p>
                  </Col>
                </Row>

                <Row>
                  <Col md={1} >
                    <p
                      style={{
                        color: "black",
                        fontSize:'12px',
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Category:
                    </p>
                  </Col>
                  <Col xs={3} md={11}>
                  <p>
  <span style={{ color: "black", fontSize: "12px" }}>
    {selectedSubmitCategory.split('|').map((course, index) => (
      <React.Fragment key={index}>
        {course}
        <br />
      </React.Fragment>
    ))}
  </span>
</p>

                  </Col>
                </Row>



               
                <Row>
                  <Col md={1} >
                    <p
                      style={{
                        color: "black",
                        fontSize:'12px',
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Year:
                    </p>
                  </Col>
                  <Col xs={3} md={11}>
                    <p>
                      <span style={{ color: "black", fontSize: "12px" }}>
                      {selectedSubmitYear}
                      </span>
                    </p>
                  </Col>
                </Row>


                
    </Col>
    
  </Row>

  


  
</Container>
<Container className="mt-5">
  <div>
  
    <div style={{marginLeft:'30px'}}>Abstract:</div>
    <Container className="mt-3" style={{ width:'90%', textIndent: '70px' }}>{selectedSubmitAbsract}</Container>
    </div>
    <div className="mt-5">

    {/* <div class="p-2" style={{ width: "25%" }}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                  onClick={toggleTopicList}
                 
                >
                  
                  <div
                    style={{ marginLeft: "30px" }}
                    
                  >
                    <h5>...</h5>
                  </div>
                  {showTopicList ? <IoIosArrowDown /> : <IoIosArrowForward />}
                </div>
              </div> */}
              {/* {showTopicList && (
          <>
            <hr
              class="d-flex justify-content-start"
              style={{
                width: "auto", // Set width to auto to allow it to start from the left
                borderTop: "1px solid #000000",
                margin: "0",
                marginTop: "10px",
                marginBottom: "10px",
              }}
            />
            
          </>
        )} */}

    </div>
    
  
</Container>



        </Modal.Body>
        <Modal.Footer>
        <Button variant="danger" onClick={() => setShowModalRecommend(true)} >Reject</Button>{' '}
               <Button variant="success" onClick={handleSubmit}>Approved</Button>
        </Modal.Footer>
      </Modal>



      

     
      
      
       

       
        
      </CDBBox>
    </div>
  );
}
