import React from 'react';
import { Nav, NavItem, NavLink } from 'react-bootstrap';
import { Link, useLocation } from 'react-router-dom';

function UsersNav() {
  const location = useLocation();

  let content;
  if (location.pathname === '/admin/managesubmission/submitted') {
    content = "Content for Submitted";
  } else if (location.pathname === '/admin/managesubmission/approved') {
    content = "Content for Approved";
  } else if (location.pathname === '/admin/managesubmission/rejected') {
    content = "Content for Rejected";
  } else {
    content = "Default Content";
  }

  return (
    <Nav className="nav-borders mb-5">
      <NavItem>
        <NavLink as={Link} to="/admin/admin/res" style={{ color: location.pathname === '/admin/admin/res' ? '#ffffff' : '#495057', backgroundColor: location.pathname === '/admin/admin/res' ? '#4B7EEB' : '', border: '2px solid transparent', borderRadius: '8px', fontSize: '13px' }} className={location.pathname === '/admin/admin/res' ? 'active' : ''}>Admin</NavLink>
      </NavItem>
      <NavItem>
        <NavLink as={Link} to="/admin/users" style={{ color: location.pathname === '/admin/users' ? '#ffffff' : '#495057', backgroundColor: location.pathname === '/admin/users' ? '#4B7EEB' : '' ,border: '2px solid transparent', borderRadius: '8px', fontSize: '13px' }} className={location.pathname === '/admin/users' ? 'active' : ''}>Users</NavLink>
      </NavItem>
      <NavItem>
      </NavItem>
    </Nav>
  );
}

export default UsersNav;
