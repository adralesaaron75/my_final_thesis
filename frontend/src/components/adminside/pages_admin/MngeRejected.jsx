
import {CDBBox, CDBCard, CDBCardBody, CDBContainer } from "cdbreact";
import { Link } from "react-router-dom";
import {
  CDBSidebar
} from "cdbreact";
import { NavLink } from "react-router-dom";
import SidebarLayout from "../components_admin/SideBarLayout";
import { Container, Form, Row, Col, Card, Table, Modal, Button, Nav } from "react-bootstrap";
import React, { useState,useEffect } from 'react';
import { FaPlus, FaTimes, FaArrowLeft, FaArrowRight } from "react-icons/fa";
import { FaEdit, FaSave, FaGreaterThan, FaLessThan } from "react-icons/fa";
import { VITE_BACKEND_URL } from "../../../App";
import axios from "axios";
import Badge from "react-bootstrap/esm/Badge";
import ManageNav from "./MngeNav";
import { FaSearch,FaFilter  } from 'react-icons/fa'; // Import the search icon component
import { GrSort } from "react-icons/gr";
import {
  IoIosArrowDown,
  IoIosArrowUp,
  IoIosArrowForward,
} from "react-icons/io";





export default function ManageRejected({  }) {
  // export default function Students({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };

  const [showTopicList, setShowTopicList] = useState(false);


  const toggleTopicList = () => {
    setShowTopicList(!showTopicList);
  };




  const [showModal, setShowModal] = useState(false);


  const [showSearch, setShowSearch] = useState(false); 

  const toggleSearch = () => {
    setShowSearch(!showSearch);
  };



  const handleCloseModal = () => {
    setShowModal(false);
  };

  const [isEditing, setIsEditing] = useState(false);


  
  const handleEditClick = () => {
    setIsEditing(true);
    // Populate edited fields with selected data
    const selectedData = data.find(item => item.SubmitTitleName === selectedSubmitTitleName && item.SubmitAbstract === selectedSubmitAbsract);
    if (selectedData) {
        setEditedTitle(selectedData.title);
        setEditedLink(selectedData.link);
        setEditedAuthor(selectedData.author);
        setEditedAbstract(selectedData.abstract);
    }
};

  const handleTitleChange = (e) => {
    setEditedTitle(e.target.value);
  };

  const handleLinkChange = (e) => {
    setEditedLink(e.target.value);
  };

  const handleAuthorChange = (e) => {
    setEditedAuthor(e.target.value);
  };

  const handleAbstractChange = (e) => {
    setEditedAbstract(e.target.value);
  };

  const handleSave = () => {
    // Handle save functionality here
    setIsEditing(false);
    // Perform any save action if needed
  };




  const [userData, setUserData] = useState(null);
    const [data, setData] = useState([]); // Define data state
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        fetchUserData();
        fetchBorrowedBooks();
    }, []);

    const fetchUserData = () => {
        // Your fetch userData logic
    }

    const fetchBorrowedBooks = async () => {
        try {
            setIsLoading(true);
            const id = getId();
            const response = await axios.get(`${VITE_BACKEND_URL}/api/borrowedbooklists/${id}`);
            setIsLoading(false);
            if (response.data.success) {
                setData(response.data.data);
            }
        } catch (error) {
            setIsLoading(false);
            console.error('Error fetching borrowed books:', error);
        }
    }

    const getId = () => {
        // Your getId logic
    }




    const [selectedSubmitTitleName, setSelectedSubmitTitleName] = useState('');
    const [selectedSubmitAbsract, setSelectedSubmitAbsract] = useState('');
    const [selectedSubmitAuthor, setSelectedSubmitAuthor] = useState('');
    const [selectedSubmitCategory, setSelectedSubmitCategory] = useState('');
    const [selectedSubmitYear, setSelectedSubmitYear] = useState('');
    const [selectedSubmitCourse, setSelectedSubmitCourse] = useState('');
    const [selectedSubmitSubmittedby, setSelectedSubmitSubmittedby] = useState('');
    const [selectedrecommendation, setselectedrecommendation] = useState('');

    // Function to handle clicking on a submission item
    const handleDivClick = (submitTitleName, submitAbstract,  course, Category, Year, Submittedby, recommendation, Author) => {
        setSelectedSubmitTitleName(submitTitleName); // Set the selected SubmitTitleName
        setSelectedSubmitAbsract(submitAbstract); 
        setSelectedSubmitCourse(course);
        setSelectedSubmitAuthor(Author);
        setSelectedSubmitCategory(Category);
        setSelectedSubmitYear(Year);
        setSelectedSubmitSubmittedby(Submittedby);
        setselectedrecommendation(recommendation);
        setShowModal(true); // Show the modal
    };

    const [datarejected, setDatarejected] = useState([]);


    useEffect(() => {
      fetchrejected();
  }, []);


  const fetchrejected = async () => {
    try {
      const response = await fetch(`${VITE_BACKEND_URL}/api/getSubmissionrejected`);
      const jsonData = await response.json();
      setDatarejected(jsonData.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };


  const [showDrawer, setShowDrawer] = useState(false);

  useEffect(() => {
  const handleScroll = () => {
    setShowDrawer(false);
  };

  window.addEventListener("scroll", handleScroll);

  return () => {
    window.removeEventListener("scroll", handleScroll);
  };
}, []);


const [showFilter, setShowFilter] = useState(false); // State variable to manage filter visibility


const [sortByTitle, setSortByTitle] = useState(false);
const [sortByCategory, setSortByCategory] = useState(false);
const [sortByCourse, setSortByCourse] = useState(false);

const categoryoptions= [
  { label: 'Mobile Game, Game Development and Virtual Reality', value: 'Mobile Game, Game Development and Virtual Reality' },
  { label: 'Information Retrieval and Search Engines', value: 'Information Retrieval and Search Engines' },
  { label: 'Artificial Intelligence and Machine Learning', value: 'Artificial Intelligence and Machine Learning' },
  { label: 'E-commerce and Digital Marketing', value: 'E-commerce and Digital Marketing' },
  { label: 'Social Network Analysis and Mining ', value: 'Social Network Analysis and Mining' },
  { label: 'Health Informatics', value: 'Health Informatics' },
  { label: 'Natural Language Processing (NLP)', value: 'Natural Language Processing (NLP)' },
  { label: 'Geographic Information Systems (GIS)', value: 'Geographic Information Systems (GIS)' },
  { label: 'Educational Technology', value: 'Educational Technology' },
  { label: 'Robotic Systems and Automation', value: 'Robotic Systems and Automation' },
  { label: 'Desktop Games and Applications', value: 'Desktop Games and Applications' },
  { label: 'Web Games and Applications', value: 'Web Games and Applications' },
  { label: 'Mobile Games and Applications', value: 'Mobile Games and Applications' },
  { label: 'Enterprise and E-commerce', value: 'Enterprise and E-commerce' },
  { label: 'Organization Applications', value: 'Organizational Applications' },
];

const [selectedCategory, setSelectedCategory] = useState(""); // State variable for selected category
const [selectedCourse, setSelectedCourse] = useState(""); // State variable for selected course
const [selectedFromDate, setSelectedFromDate] = useState(""); // State variable for selected from date
const [selectedToDate, setSelectedToDate] = useState(""); 


const removeCategory = () => {
  setSelectedCategory("");
};

// Function to clear selected course
const removeCourse = () => {
  setSelectedCourse("");
};

// Function to clear selected date range
const clearDate = () => {
  setSelectedFromDate("");
  setSelectedToDate("");
};




  
 



    




 

  return (
    <div
      style={{ display: "flex", height: "auto", overflow: "scroll initial" }}
    >
      <CDBBox display="flex" alignContent="start">
        <CDBSidebar textColor="#fff" backgroundColor="#800000" style={{height:'100%'}}>
          <SidebarLayout />
        </CDBSidebar>
      </CDBBox>

      <CDBBox
        // display="flex"
        justifyContent="center"
        style={{
          width: "100%",
          height: "100%",
        }}
      >
       {/* Name: {userData.name} */}





<div style={{ backgroundColor: '#f8f9fa', padding: '20px', borderRadius: '8px', boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)' }}>
<div style={{ marginBottom: '20px' }}>
        {/* Account page navigation */}
      <ManageNav/>

      <div style={{ display: 'flex', marginTop: '5px', justifyContent: 'end', alignItems: 'center' }}> {/* Added alignItems */}
  <FaSearch className="my-3" style={{ cursor: 'pointer', marginRight: '5px' }} onClick={toggleSearch} />  {showSearch ? ( null ) : <text>Search</text>}
  
  {showSearch && (
    <Form.Group controlId="search" className="my-3" style={{ margin: '0' }}> {/* Added margin: 0 to remove default margin */}
      <Form.Control type="text" placeholder="Search" />
    </Form.Group>
  )}

<div className="d-flex align-items-end">
              <div className="mr-3" style={{marginRight:'25px', marginLeft:'25px'}}>
                <FaFilter onClick={() => setShowFilter(!showFilter)} /> Filter {/* Toggle filter visibility */}
              </div>
              <div  onClick={() => setShowDrawer(!showDrawer)}>
        <GrSort /> Sort
      </div>

      
            </div>

</div>

{showFilter && ( // Show filter card if showFilter is true
            <CDBCard className="mt-2">
              <CDBCardBody>
                {selectedCategory || selectedCourse || selectedFromDate || selectedToDate ? (
                  <div className="mt-3" style={{marginLeft:'25px'}}>
                    Keyword: 
                    {selectedCategory ?(
                      <div
                        className="selected-color d-inline-block me-2 mb-2 px-2 py-1 rounded"
                        style={{ color: 'black', fontSize: '0.7rem', fontWeight: 'bold' }}
                      >
                        {selectedCategory}
                        <button
                          type="button"
                          className="btn-close ms-2"
                          aria-label="Close"
                          onClick={() => removeCategory()}
                        ></button>
                      </div>
                    ):null}
                    {selectedCourse ?(
                      <div
                        className="selected-color d-inline-block me-2 mb-2 px-2 py-1 rounded"
                        style={{ color: 'black', fontSize: '0.7rem', fontWeight: 'bold' }}
                      >
                        {selectedCourse}
                        <button
                          type="button"
                          className="btn-close ms-2"
                          aria-label="Close"
                          onClick={() => removeCourse()}
                        ></button>
                      </div>
                    ):null}
                    {(selectedFromDate && selectedToDate) ? (
                      <div
                        className="selected-color d-inline-block me-2 mb-2 px-2 py-1 rounded"
                        style={{ color: 'black', fontSize: '0.7rem', fontWeight: 'bold' }}
                      >
                        {selectedFromDate} To {selectedToDate}
                        <button
                          type="button"
                          className="btn-close ms-2"
                          aria-label="Close"
                          onClick={() => clearDate()}
                        ></button>
                      </div>
                    ) : null}

                    <hr
                      className="d-flex justify-content-center"
                      style={{
                        width: "90%",
                        borderTop: "1px solid #000000",
                        margin: "auto",
                        marginTop: "10px",
                        marginBottom: "10px",
                      }}
                    />
                  </div>
                ) : null}

                <Container>
                  <Row className="justify-content-center mt-3 mb-3"> {/* Added justify-content-center class */}
                    <Col md={3}>
                      <Row>
                        <Col>
                          <Form.Group className="mb-3">
                            <Form.Label>From:</Form.Label>
                            <Form.Control type="date"   onChange={(e) => setSelectedFromDate(e.target.value)}/>
                          </Form.Group>
                        </Col>
                        <Col>
                          <Form.Group className="mb-3">
                            <Form.Label>To:</Form.Label>
                            <Form.Control type="date"  onChange={(e) => setSelectedToDate(e.target.value)}/>
                          </Form.Group>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={3}>
                      <Form.Group className="mb-3">
                        <Form.Label>Category:</Form.Label>
                        <Form.Control as="select" className="form-select" value={selectedCategory} onChange={(e) => setSelectedCategory(e.target.value)}>
                          {categoryoptions.map((option) => (
                            <option key={option.value} value={option.value}>{option.label}</option>
                          ))}
                        </Form.Control>
                      </Form.Group>
                    </Col>
                    <Col md={3}>
                      <Form.Group className="mb-3">
                        <Form.Label>Course:</Form.Label>
                        <Form.Control as="select" className="form-select" value={selectedCourse} onChange={(e) => setSelectedCourse(e.target.value)}>
                          <option value="BSCS">BSCS</option>
                          <option value="BSIT">BSIT</option>
                          <option value="BSIS">BSIS</option>
                        </Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>    
                  <Row className="justify-content-end mb-4">
                    <Col md={3}>
                      <Button variant="primary" >Apply Filters</Button> 
                    </Col>
                  </Row>
                </Container>
              </CDBCardBody>
            </CDBCard>
          )}

{showDrawer && (
                <div
                  style={{
                    position: "fixed",
                    top: 185,
                    right: 25,
                    zIndex: 999,
                    filter: 'drop-shadow(0 4px 8px rgba(0, 0, 0, 0.99))'
                  }}
                >
                   <div
                    style={{
                      position: "relative",
                      background: "#f8f9fa",
                      boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
                      borderRadius: "5px",
                      width: "15vw",
                    }}
                  >
                    <Container className="p-3">
                    <Form.Check
      type="radio"
      name="sortOption"
      label="Sort by Title"
      checked={sortByTitle}
      onChange={() => {
        setSortByTitle(true);
        setSortByCategory(false);
        setSortByCourse(false);
      }}
    />
    {/* Radio button for category sorting */}
    <Form.Check
      type="radio"
      name="sortOption"
      label="Sort by Category"
      checked={sortByCategory}
      onChange={() => {
        setSortByTitle(false);
        setSortByCategory(true);
        setSortByCourse(false);
      }}
    />
    <Form.Check
      type="radio"
      name="sortOption"
      label="Sort by Course"
      checked={sortByCourse}
      onChange={() => {
        setSortByTitle(false);
        setSortByCategory(false);
        setSortByCourse(true);
      }}
    />
    </Container>
                 
                  </div>
                </div>
)}








      </div>
      <hr className="my-4" />


    



      {datarejected.map((i, index) => (
  <div key={index} onClick={() => handleDivClick(i.SubmitTitleName, i.SubmitAbstract, i.course, i.category, i.YearPublished, i.Submittedby, i.recommendation)}  style={{ marginBottom: '10px' }}>
    <Container fluid>
      <Card className="mt-2" style={{ filter: "drop-shadow(0 9px 8px rgba(0, 0, 0, 0.15))" }}>
      <div style={{ position: 'absolute', top: 0, left: 0 }}>
          <Badge bg="warning" style={{ minWidth: 'auto', fontSize:'10px'}}>new</Badge>
        </div>
        <Card.Body style={{ paddingTop: '7px', paddingBottom: '7px' }}>
          <Row>
          <Col md={1} className="d-flex justify-content-center align-items-center">
              <Form.Group controlId="formCheckbox" className="m-0">
                <Form.Check
                  type="checkbox"
                  style={{fontSize:'17px',filter: 'drop-shadow(3px 2px 1px rgba(0, 0, 0, 0.3))'}}
                />
              </Form.Group>
            </Col>

            <Col md={2}>
  <div style={{ width: '100%', height: '3rem', overflow: 'hidden', textOverflow: 'ellipsis', fontSize: '12px', paddingBottom: 'auto', paddingTop: '5px', display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
    <div style={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>
      <h6 style={{ display: 'inline-block', marginBottom: 0 }}>{i.SubmittedName} a</h6>
    </div>
    <div style={{ color: 'gray', overflow: 'hidden', textOverflow: 'ellipsis' }}>
      {i.Submittedby}
    </div>
  </div>
</Col>

            <Col md={9}>
              <div
                style={{ width: '100%', height: '2.7rem', overflow: 'hidden', textOverflow: 'ellipsis', fontSize: '12px', paddingBottom: 'auto', paddingTop: '5px' }}
              >
                <h6 style={{ display: 'inline-block', marginBottom: 0, fontWeight: 'bold' }}>{i.SubmitTitleName}</h6>
                <span style={{ color: 'gray', overflow: 'hidden', textOverflow: 'ellipsis' }}> - {i.SubmitAbstract}</span>
              </div>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </Container>
  </div>
))}





    </div>



























      
<Modal size="xl" show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title style={{width:'92%'}}></Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* <p>{content}</p> */}
          <Container>
  <Row>
  <Col md={2}>
  <div style={{    width: '9vw', 
      height: '25vh', 
      backgroundColor: '#ddd', borderRadius: '4px' }}>
    {/* Placeholder content */}
    
  </div>
</Col>

    <Col style={{ background: 'white' }}>
    <Row >
                  <Col md={1}>
                    <p
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                        fontSize:'12px',
                      }}
                    >
                      Title:
                    </p>
                  </Col>
                  <Col>
                    <p>
                      <span style={{ color: "black", fontSize: "17px" }}>
                        {selectedSubmitTitleName}
                        
                      </span>
                    </p>
                  </Col>
                </Row>
               
                <Row>
                  <Col md={1} >
                    <p
                      style={{
                        color: "black",
                        fontSize:'12px',
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Author:
                    </p>
                  </Col>
                  <Col xs={3} md={11}>
                    <p>
                      <span style={{ color: "black", fontSize: "12px" }}>
                        {selectedSubmitAuthor}
                      
                    
                     
                       
                      </span>
                    </p>
                  </Col>
                </Row>

                <Row>
                  <Col md={1} >
                    <p
                      style={{
                        color: "black",
                        fontSize:'12px',
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Course:
                    </p>
                  </Col>
                  <Col xs={3} md={11}>
                    <p>
                      <span style={{ color: "black", fontSize: "12px" }}>
                        {selectedSubmitCourse}
                    
                      </span>
                    </p>
                  </Col>
                </Row>

                <Row>
                  <Col md={1} >
                    <p
                      style={{
                        color: "black",
                        fontSize:'12px',
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Category:
                    </p>
                  </Col>
                  <Col xs={3} md={11}>
                  {/* <p>
  <span style={{ color: "black", fontSize: "12px" }}>
    {selectedSubmitCategory.split('|').map((course, index) => (
      <React.Fragment key={index}>
        {course}
        <br />
      </React.Fragment>
    ))}
  </span>
</p> */}

<p>
                      <span style={{ color: "black", fontSize: "12px" }}>
                        {selectedSubmitCategory}
                    
                      </span>
                    </p>


                  </Col>
                </Row>



               
                <Row>
                  <Col md={1} >
                    <p
                      style={{
                        color: "black",
                        fontSize:'12px',
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      Year:
                    </p>
                  </Col>
                  <Col xs={3} md={11}>
                    <p>
                      <span style={{ color: "black", fontSize: "12px" }}>
                      {selectedSubmitYear}
                      </span>
                    </p>
                  </Col>
                </Row>


                
    </Col>
    
  </Row>

  


  
</Container>
<Container className="mt-5">
  <div>
  
    <div style={{marginLeft:'30px'}}>Abstract:</div>
    <Container className="mt-3" style={{ width:'90%', textIndent: '70px' }}>{selectedSubmitAbsract}</Container>
    </div>
    <div className="mt-5">

    <div class="p-2" style={{ width: "35%" }}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                  onClick={toggleTopicList}
                 
                >
                  
                  <div
                    style={{ marginLeft: "30px" }}
                    
                  >
                    <h6>Comments and Suggestions</h6>
                  </div>
                  {showTopicList ? <IoIosArrowDown /> : <IoIosArrowForward />}
                </div>
              </div>
              {showTopicList && (
          <>
            <hr
              class="d-flex justify-content-start"
              style={{
                width: "auto", // Set width to auto to allow it to start from the left
                borderTop: "1px solid #000000",
                margin: "0",
                marginTop: "10px",
                marginBottom: "10px",
              }}
            />
            <Container className="mt-3" style={{ width:'90%', textIndent: '70px' }}>{selectedrecommendation}</Container>
            
          </>
        )}

    </div>
    
  
</Container>



        </Modal.Body>
        <Modal.Footer>
        <Button variant="danger" >Delete</Button>{' '}
        </Modal.Footer>
      </Modal>


      

     
      
      
       

       
        
      </CDBBox>
    </div>
  );
}
