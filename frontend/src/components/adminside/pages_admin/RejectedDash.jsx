import { CDBBox, CDBContainer } from "cdbreact";
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from "cdbreact";
import React, { useState } from 'react';
import SidebarLayout from "../components_admin/SideBarLayout";
import AdminDashboard from "../components_admin/AdminDashboard";

export default function RejectedDash({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };


  
  return (
    <div
      style={{ display: "flex", height: "auto", overflow: "scroll initial" }}
    >
      <CDBBox display="flex" alignContent="start">
        <CDBSidebar textColor="#fff" backgroundColor="#800000" style={{height:'100%'}}>
          <SidebarLayout/>
        </CDBSidebar>
      </CDBBox>


      <CDBBox
        
        display="flex"
        justifyContent="start"
        style={{
          width: "100%",
          height: "100%",
        }}
      >

        <AdminDashboard/>
        
      


      </CDBBox>
    </div>
  );
}
