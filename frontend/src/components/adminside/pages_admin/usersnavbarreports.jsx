import React from 'react';
import { Container, Navbar, Nav, NavLink } from 'react-bootstrap';

import { Link, useLocation } from 'react-router-dom';

function UserReportsnav() {
  const location = useLocation();



  let content;
  if (location.pathname === '/admin/reports/submitted') {
    content = "Content for Submitted";
  } else if (location.pathname === '/admin/reports/approved') {
    content = "Content for Approved";
  } else if (location.pathname === '/admin/reports/disapproved') {
    content = "Content for Disapproved";
  } else if (location.pathname === '/admin/reports/webscraped') {
    content = "Content for Web Scraped";
  } else {
    content = "Default Content";
  }

  return (
   
      <Navbar>
        <Container>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Item>
                <Link to="/admin/reports/admin" className={`nav-link ${location.pathname === '/admin/reports/admin' ? 'active' : ''}`}>
                  <div className="box" style={{ width: "100px", height: "75px", border: "2px solid gray ", borderRadius: "7px", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", backgroundColor: location.pathname === '/admin/reports/admin' ? '#4B7EEB' : '#F2EDF9'}}>
                    <div className="box" style={{ width: "96%", height: "36px", marginTop: '0', borderTopLeftRadius: '3px', borderTopRightRadius: '3px', display: "flex", justifyContent: "center", alignItems: "center", background: "white" }}>
                      <p style={{ fontSize: '20px', margin: '0', marginRight: '45px' }}>26</p>
                    </div>
                    <p style={{ margin: "0", marginTop: "15px", color: location.pathname === '/admin/reports/admin' ? 'white' : 'black', fontSize: '12px', marginRight: 'auto', marginLeft: '5px' }}>Admin</p>
                  </div>
                </Link>
              </Nav.Item>
              <Nav.Item>
                <Link to="/admin/reports/users" className={`nav-link ${location.pathname === '/admin/reports/users' ? 'active' : ''}`}>
                  <div className="box" style={{ width: "100px", height: "75px", border: "2px solid gray ", borderRadius: "7px", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", backgroundColor: location.pathname === '/admin/reports/users' ? '#4B7EEB' : '#F2EDF9', }}>
                    <div className="box" style={{ width: "96%", height: "36px", marginTop: '0', borderTopLeftRadius: '3px', borderTopRightRadius: '3px', display: "flex", justifyContent: "center", alignItems: "center", background: "white" }}>
                      <p style={{ fontSize: '20px', margin: '0', marginRight: '45px' }}>21</p>
                    </div>
                    <p style={{ margin: "0", marginTop: "15px", color: location.pathname === '/admin/reports/users' ? 'white' : 'black', fontSize: '12px', marginRight: 'auto', marginLeft: '5px' }}>Users</p>
                  </div>
                </Link>
              </Nav.Item>
              
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
     
  );
}

export default UserReportsnav;
