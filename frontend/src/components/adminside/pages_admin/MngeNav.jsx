import React from 'react';
import { Nav, NavItem, NavLink } from 'react-bootstrap';
import { Link, useLocation } from 'react-router-dom';

function ManageNav() {
  const location = useLocation();

  let content;
  if (location.pathname === '/admin/managesubmission/submitted') {
    content = "Content for Submitted";
  } else if (location.pathname === '/admin/managesubmission/approved') {
    content = "Content for Approved";
  } else if (location.pathname === '/admin/managesubmission/rejected') {
    content = "Content for Rejected";
  } else {
    content = "Default Content";
  }

  return (
    <Nav className="nav-borders">
      <NavItem>
        <NavLink as={Link} to="/admin/managesubmission/submitted" style={{ color: location.pathname === '/admin/managesubmission/submitted' ? '#ffffff' : '#495057', backgroundColor: location.pathname === '/admin/managesubmission/submitted' ? '#4B7EEB' : '', border: '2px solid transparent', borderRadius: '8px', fontSize: '13px' }} className={location.pathname === '/admin/managesubmission/submitted' ? 'active' : ''}>Submitted</NavLink>
      </NavItem>
      <NavItem>
        <NavLink as={Link} to="/admin/managesubmission/approved" style={{ color: location.pathname === '/admin/managesubmission/approved' ? '#ffffff' : '#495057', backgroundColor: location.pathname === '/admin/managesubmission/approved' ? '#4B7EEB' : '' ,border: '2px solid transparent', borderRadius: '8px', fontSize: '13px' }} className={location.pathname === '/admin/managesubmission/approved' ? 'active' : ''}>Approved</NavLink>
      </NavItem>
      <NavItem>
        <NavLink as={Link} to="/admin/managesubmission/rejected" style={{ color: location.pathname === '/admin/managesubmission/rejected' ? '#ffffff' : '#495057', backgroundColor: location.pathname === '/admin/managesubmission/rejected' ? '#4B7EEB' : '' ,border: '2px solid transparent', borderRadius: '8px', fontSize: '13px' }} className={location.pathname === '/admin/managesubmission/rejected' ? 'active' : ''}>Rejected</NavLink>
      </NavItem>
      <NavItem>
      </NavItem>
    </Nav>
  );
}

export default ManageNav;
