// import React, { useState, useEffect } from 'react';
// import axios from 'axios';

// const StudentList = () => {
//   const [students, setStudents] = useState([]);
//   const [sortColumn, setSortColumn] = useState(null);
//   const [sortOrder, setSortOrder] = useState('asc');

//   useEffect(() => {
//     fetchData();
//   }, []); // Empty dependency array ensures useEffect runs only once after initial render

//   const fetchData = async () => {
//     try {
//       const response = await axios.get('http://localhost:3000/api/getAllUser'); // Adjust the URL according to your server setup
//       setStudents(response.data.data);
//     } catch (error) {
//       console.error('Error fetching data:', error);
//     }
//   };

//   const handleSort = (column) => {
//     if (sortColumn === column) {
//       // If already sorted by this column, reverse the order
//       setSortOrder(sortOrder === 'asc' ? 'desc' : 'asc');
//     } else {
//       // If sorting by a new column, set it as the sorting column and default to ascending order
//       setSortColumn(column);
//       setSortOrder('asc');
//     }
//   };

//   const sortedStudents = students.slice().sort((a, b) => {
//     const columnA = a[sortColumn];
//     const columnB = b[sortColumn];

//     if (sortOrder === 'asc') {
//       return columnA < columnB ? -1 : columnA > columnB ? 1 : 0;
//     } else {
//       return columnA > columnB ? -1 : columnA < columnB ? 1 : 0;
//     }
//   });

//   return (
//     <div>
//       <h1>Student List</h1>
//       <table>
//         <thead>
//           <tr>
//             <th onClick={() => handleSort('name')}>Name</th>
//             <th onClick={() => handleSort('course')}>Course</th>
//             <th onClick={() => handleSort('yearandsection')}>Year & Section</th>
//             <th onClick={() => handleSort('email')}>Email</th>
//           </tr>
//         </thead>
//         <tbody>
//           {sortedStudents.map(student => (
//             <tr key={student._id}>
//               <td>{student.name}</td>
//               <td>{student.course}</td>
//               <td>{student.yearandsection}</td>
//               <td>{student.email}</td>
//             </tr>
//           ))}
//         </tbody>
//       </table>
//     </div>
//   );
// };

// export default StudentList;
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { VITE_BACKEND_URL } from '../../../App';

function StudentList({ userData }) {
 
 
  return (
    <div>
    {/* <h2>User Data:</h2>
    <p>Name: {userData.name}</p>
    <p>Email: {userData.email}</p> */}
    {/* Render other user data as needed */}
  </div>
  );
}

export default StudentList;
