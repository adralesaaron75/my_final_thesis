import { CDBBox, CDBContainer } from "cdbreact";
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from "cdbreact";
import React, { useState, useEffect } from 'react';
import SidebarLayout from "../components_admin/SideBarLayout";
import AdminDashboard from "../components_admin/AdminDashboard";
import Reportsnav from "./navbarreports";
import { Container, Navbar, Nav, NavLink, Row, Col, Card, Table } from 'react-bootstrap';
import Filterdate from "../components_admin/Filterdate";
import { VITE_BACKEND_URL } from "../../../App";
import axios from "axios";

export default function Reportsapproved({ userData }) {
  const logOut = () => {
    window.localStorage.clear();
    window.location.href = "../../";
  };


  
  const [data, setData] = useState([]); // Define data state
  const [isLoading, setIsLoading] = useState(false);
  const [dateRange, setDateRange] = useState("");

  useEffect(() => {
    fetchUserData();
    fetchBorrowedBooks();
}, []);

const fetchUserData = () => {
  // Your fetch userData logic
}

const fetchBorrowedBooks = async () => {
  try {
    setIsLoading(true);
    const response = await axios.get(`${VITE_BACKEND_URL}/api/getSubmissionapproved`);
    setIsLoading(false);
    if (response.data.status === "ok") {
      setData(response.data.data);
    } else {
      console.error('Error fetching borrowed books:', response.data);
    }
  } catch (error) {
    setIsLoading(false);
    console.error('Error fetching borrowed books:', error);
  }
}




  const renderCircleWithLetter = (name) => {
    const startingLetter = name.charAt(0).toUpperCase();
    return (
      <div
        style={{
          width: "30px",
          height: "30px",
          borderRadius: "50%",
          backgroundColor: "#FFCC00",
          textAlign: "center",
          lineHeight: "30px",
          color: "white",
          marginRight: "-5px",
          boxShadow: "0 2px 4px rgba(0, 0, 0, 0.2)", // Add shadow effect
        
        }}
      >
        {startingLetter}
      </div>
    );
  };

  const lname = [
    { name: "Farrel Hoggin", course:"BSCS"},
    { name: "Irma Olech" , course:"BSIS"},
    { name: "Emmit Gallacher" , course:"BSIT"},
    { name: "Dunn Astlet" , course:"BSIT"},
  ];


  const groupItemsByDate = (items) => {
    const groups = {};
    items.forEach((item) => {
      const date = new Date(item.createdAt).toLocaleDateString("en-US", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit",
      });
      if (!groups[date]) {
        groups[date] = [];
      }
      groups[date].push(item);
    });
    return groups;
  };
  
  // Now, let's group the data by their createdAt date
  const groupedData = groupItemsByDate(data);

  

  return (
    <div
      style={{ display: "flex", height: "auto", overflow: "scroll initial" }}
    >
      <CDBBox display="flex" alignContent="start">
        <CDBSidebar textColor="#fff" backgroundColor="#800000" style={{ height: '100%' }}>
          <SidebarLayout />
        </CDBSidebar>
      </CDBBox>


      <CDBBox

        display="flex"
        justifyContent="start"
        style={{
          width: "100%",
          height: "100%",
        }}
      >

        <Container className="mt-5" fluid>
          <Reportsnav />
          <Filterdate setDateRange={setDateRange} />
          {/* <p>Date Range: {dateRange}</p> */}


          <Container fluid>
          <Table className="table">
              <thead>
                <tr>
                  <th width="390">
                    Title
                  </th>
                  <th width="200">
                    Author
                  </th>
                  <th width="160">Course</th>
                  <th width="200">
                    Category
                  </th>
                  <th width="180">Submission Date</th>
                </tr>
              </thead>
            </Table>

            {Object.keys(groupedData).map((date, index) => {
              // Check if dateRange is set
              if (dateRange) {
                const [startDate, endDate] = dateRange.split(" to ");
                const currentDate = new Date(date);
                const startDateObj = new Date(startDate);
                const endDateObj = new Date(endDate);
                const startDateAdjusted = new Date(startDate);
startDateAdjusted.setDate(startDateAdjusted.getDate() - 1);

                // Check if the current date falls within the date range
              
                  if (currentDate >= startDateAdjusted && currentDate <= endDateObj) {
                  return (
                    <Card key={index} className="mb-4">
                      <Card.Header>{date}</Card.Header>
                      <Card.Body>
                        <Table className="table">
                          <tbody style={{ fontSize: '14px' }}>
                            {groupedData[date].map((item, idx) => (
                              <tr key={idx}>
                                <td width="390" style={{ fontSize: '14px', textTransform: "uppercase", verticalAlign: 'middle' }}>{item.SubmitTitleName}</td>
                                <td width="200" style={{ verticalAlign: 'middle' }}>
                                  {item.authorjoin && item.authorjoin !== "n/a" ? (
                                    item.authorjoin.split('|').map((author, index) => (
                                      <text key={index} style={{ display: 'inline-flex', cursor: 'pointer' }} title={author.trim().split('(')[0].trim()}>
                                        {renderCircleWithLetter(author.trim().split('(')[0].trim())}
                                      </text>
                                    ))
                                  ) : (
                                      <span>&nbsp;</span>
                                    )}
                                </td>
                                <td width="160">{item.course}</td>
                                <td width="200">{item.category}</td>
                                <td width="180">
                                  {new Date(item.createdAt).toLocaleDateString("en-US", {
                                    year: "numeric",
                                    month: "2-digit",
                                    day: "2-digit",
                                  })}
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </Table>
                      </Card.Body>
                    </Card>
                  );
                } else {
                  return null; // Skip rendering if date is outside the range
                }
              } else {
                // If dateRange is not set, render all groupedData
                return (
                  <Card key={index} className="mb-4">
                    <Card.Header>{date}</Card.Header>
                    <Card.Body>
                      <Table className="table">
                        <tbody style={{ fontSize: '14px' }}>
                          {groupedData[date].map((item, idx) => (
                            <tr key={idx}>
                              <td width="390" style={{ fontSize: '14px', textTransform: "uppercase", verticalAlign: 'middle' }}>{item.SubmitTitleName}</td>
                              <td width="200" style={{ verticalAlign: 'middle' }}>
                                {item.authorjoin && item.authorjoin !== "n/a" ? (
                                  item.authorjoin.split('|').map((author, index) => (
                                    <text key={index} style={{ display: 'inline-flex', cursor: 'pointer' }} title={author.trim().split('(')[0].trim()}>
                                      {renderCircleWithLetter(author.trim().split('(')[0].trim())}
                                    </text>
                                  ))
                                ) : (
                                    <span>&nbsp;</span>
                                  )}
                              </td>
                              <td width="160">{item.course}</td>
                              <td width="200">{item.category}</td>
                              <td width="180">
                                {new Date(item.createdAt).toLocaleDateString("en-US", {
                                  year: "numeric",
                                  month: "2-digit",
                                  day: "2-digit",
                                })}
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </Table>
                    </Card.Body>
                  </Card>
                );
              }
            })}

          </Container>

        </Container>


      </CDBBox>
    </div>
  );
}
