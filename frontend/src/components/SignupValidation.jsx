import * as Yup from "yup";

export const signupValidation = Yup.object({
    name: Yup.string().matches(/^[a-zA-Z ]+$/, "Name should contain letters only").min(3).required("Please enter Name"),
    email: Yup.string().email("Please enter a valid email").required("Please enter Email").matches(/@sorsu\.gmail$/, "Emails should only be from sorsu domain"),
    password: Yup.string().matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*])[a-zA-Z\d!@#$%^&*]{6,}$/,
        "Password must contain at least one uppercase letter, one special character, one number, and be at least 6 characters long"
    ).required("Please enter Password"),
    confirmPassword: Yup.string().oneOf([Yup.ref("password")], "Passwords do not match").required("Please confirm your password"),
})
